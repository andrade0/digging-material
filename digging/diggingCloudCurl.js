import jQuery from 'jquery';

const diggingCloudCurl = function (url, cb) {
  jQuery.ajax({
    type: 'GET',
    url: 'https://us-central1-digging-903.cloudfunctions.net/curl?url='+url,
    success: (response) => {
      cb(null, response);
    },
    error: (err) => {
      cb(err, null);
    }
  });
};

export default diggingCloudCurl;
