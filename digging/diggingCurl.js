import jQuery from 'jquery';
import md5 from 'md5';

const diggingCurl = function (url, cb) {
  const key = md5(url);
  jQuery.ajax({
    type: 'GET',
    url,
    success: (response) => {
      const curlResult = { html: response, url: key };
      cb(null, curlResult);
    },
    error: (err) => {
      cb(err, null);
    }
  });
};


export default diggingCurl;
