import Juno from './sources/juno';
import Ra from './sources/ra';
import Deejay from './sources/deejay';
import Itunes from './sources/itunes';
import Beatport from './sources/beatport';
import Discogs from './sources/discogs';
import Mixesdb from './sources/mixesdb';
import Youtube from './sources/youtube';
import Yoyaku from './sources/yoyaku';

const sourcesObjects = {};
sourcesObjects.juno = new Juno();
sourcesObjects.ra = new Ra();
sourcesObjects.deejay = new Deejay();
sourcesObjects.itunes = new Itunes();
sourcesObjects.beatport = new Beatport();
sourcesObjects.discogs = new Discogs();
sourcesObjects.mixesdb = new Mixesdb();
sourcesObjects.youtube = new Youtube();
sourcesObjects.yoyaku = new Yoyaku();

export default sourcesObjects;
