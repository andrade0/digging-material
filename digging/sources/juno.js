import _ from 'underscore';
import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import diggingCurl from '../diggingCurl';
import moment from 'moment';
import diggingHelpers from '../diggingHelpers';
import diggingCloudCurl from '../diggingCloudCurl';

export default class juno {

  constructor() {
    this.name = 'Juno';
    this.value = 'juno';
    this.logo = 'public/img/sources/juno.png';
    this.color = '#2797C7';
  }

  artist(query, searchArtistCallback) {
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[artist][0]=${encodeURI(query)}`;
        diggingCurl(url, waterfallCallback);
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease(this, r);
            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }

  label(query, searchArtistCallback) {
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[label][0]=${encodeURI(query)}`;
        diggingCurl(url, waterfallCallback);
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease(this, r);
            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }

  all(query, searchArtistCallback) {
    const $this = this;
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `http://www.juno.co.uk/search/?solrorder=relevancy&facet[stocked][0]=3&show_out_of_stock=1&q[all][0]=${encodeURI(query)}`;
        diggingCurl(url, waterfallCallback);
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease($this, r);
            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }

  parseList(html) {
    const $ = cheerio.load(html);
    const releases = [];
    if ($('.dv-item').length > 0) {
      $('.dv-item').each(function () {
        let releaseTitle = null;
        let releaseYear = null;
        let releaseDate = null;
        let releaseCover = null;
        let releaseLabel = null;
        let releaseUrl = null;
        let releaseGenre = null;
        let releaseArtist = null;

        const src = $(this).find('div.pl-img').find('img').attr('src');
        if (src) {
          if (src.match('data:image')) {
            const rel = $(this).find('div.pl-img').find('img').attr('rel');
            if (rel) {
              releaseCover = rel;
            }
          } else {
            releaseCover = src;
          }
        }

        const artistList = [];

        $(this).find('.pl-info').find('.vi-text').each(function () {
          $(this).find('a').each((k, v) => {
            if ($(v).attr('href').match('/artists/')) {
              artistList.push(S($(v).text()).capitalize().s.trim());
            }

            if ($(v).attr('href').match('/labels/')) {
              releaseLabel = S($(v).text()).capitalize().s.trim();
            }

            if ($(v).attr('href').match('/products/')) {
              releaseTitle = S($(v).text()).capitalize().s.trim();
              releaseUrl = `http://www.juno.co.uk${$(v).attr('href')}`;
            }
          });
        });


        $(this).find('.pl-info').find('.vi-text').each((k, v) => {
          if (k === 4) {
            releaseGenre = $(v).text().trim().replace('/', ', ');
          }

          if (k === 3) {
            const str = $(v).text().trim();
            if (str.match(/Cat:/, 'i') && str.match(/Rel:/, 'i')) {
              const ex = str.split('Rel:');
              if (ex.length > 0) {
                const date = ex[1].trim();
                const ex1 = date.split(' ');
                if (ex1.length > 2) {
                  if (parseInt(ex1[2]) > 60) {
                    releaseYear = `19${ex1[2]}`;
                  } else {
                    releaseYear = `20${ex1[2]}`;
                  }

                  const month = ex1[1];
                  const day = ex1[0];

                  const months = [];
                  months.push('Jan');
                  months.push('Feb');
                  months.push('Mar');
                  months.push('Apr');
                  months.push('May');
                  months.push('Jun');
                  months.push('Jul');
                  months.push('Aug');
                  months.push('Sep');
                  months.push('Oct');
                  months.push('Nov');
                  months.push('Dec');

                  let m = _.indexOf(months, month);

                  if (m > 0) {
                    if (m < 10) {
                      m = `0${m}`;
                    }
                    releaseDate = `${releaseYear}-${m}-${day}`;
                  }
                }
              }
            }
          }
        });

        if (artistList.length < 1) {
          releaseArtist = null;
        } else if (artistList.length < 2) {
          releaseArtist = artistList[0];
        } else {
          releaseArtist = artistList.join(', ');
        }

        const tracks = [];

        $(this).find('.vi-tracklist').find('li').each(function () {
          const t = {};
          $(this).find('.vi-text').each(function (k, v) {
            let str = $(this).text().trim();
            if (str) {
              const reg = new RegExp(/[(][0-9][:][0-9]{2}[)]/i);
              let matches = str.match(reg);
              if (matches && matches[0]) {
                t.duration = matches[0];
                t.duration = S(t.duration).replaceAll('(', '').s;
                t.duration = S(t.duration).replaceAll(')', '').s;
                const expl = str.split(' ');
                const str2 = [];
                expl.forEach((s) => {
                  if (!s.trim().match(reg)) {
                    str2.push(s);
                  }
                });
                str = str2.join(' ');
              }

              if (str.match(reg)) {

              }

              t.title = str;

              const explode = t.title.split(' - "');

              matches = t.title.match(new RegExp(/[ ]["].+["]/));

              if (explode.length > 1) {
                t.artist = explode[0];
                t.title = explode[1];
                t.title = S(t.title).replaceAll('"', '').s;
              } else if (matches) {
                t.title = S(matches[0]).replaceAll('"', '').s;
                t.title = S(t.title).replaceAll(' ', '').s;
                t.artist = matches.input.substr(0, matches.index);
              } else {
                t.artist = releaseArtist;
              }
            }
          });
          $(this).find('.vi-icon a.jrplayer').each((k, v) => {
            t.sample = $(v).attr('href');
          });
          tracks.push(t);
        });

        if (tracks.length > 0) {
          releases.push({
            kind: 'release',
            artist: releaseArtist,
            tracks,
            label: releaseLabel,
            cover: releaseCover,
            source: 'juno',
            url: releaseUrl,
            title: releaseTitle,
            playedBy: releaseArtist,
            year: releaseYear,
            releaseDate,
            genre: releaseGenre
          });
        }
      });
    }

    return releases;
  }

  news(query, getNewsCallback) {
    const $this = this;
    const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
    const feeds = [
      `http://www.juno.co.uk/all/this-week/?items_per_page=100&show_out_of_stock=1&media_type=vinyl&ddk=${today}`,
      `http://www.juno.co.uk/deep-house/this-week/?items_per_page=100&show_out_of_stock=1&media_type=vinyl&ddk=${today}`,
      `http://www.juno.co.uk/deep-house/this-week/?items_per_page=100show_out_of_stock=1&media_type=vinyl&ddk=${today}`
    ];
    const all_juno = [];
    async.each(feeds, (url, cb) => {
      diggingCloudCurl(url, (error, result) => {
        if (result.html) {
          const releases = $this.parseList(result.html);

          if (releases && releases.length && releases.length > 0) {
            releases.forEach((r) => {
              const release = diggingHelpers.prepareRelease($this, r);

              if (release) {
                all_juno.push(release);
              }
            });
          }
          cb(null);
        } else {
          cb(null);
        }
      });
    }, (e, r) => {
      getNewsCallback(all_juno);
    });
  }
}
