import _ from 'underscore';
import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import moment from 'moment';
import diggingHelpers from '../diggingHelpers';
import diggingCurl from '../diggingCurl';
import diggingCloudCurl from '../diggingCloudCurl';

export default class deejay {
  constructor() {
    this.name = 'Deejay.de';
    this.value = 'deejay';
    this.logo = 'public/img/sources/deejay.png';
    this.color = '#271F22';
  }
  artist(query, searchArtistCallback) {
    const $this = this;
    const releases = [];
    async.waterfall([
      function (waterfallCallback) {
        const url = `http://www.deejay.de/content.php?param=/${encodeURI(query)}/perpage_500`;
        diggingCurl(url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = $this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  label(query, searchArtistCallback) {
    const $this = this;
    $this.artist(query, searchArtistCallback);
  }
  all(query, searchArtistCallback) {
    const $this = this;
    $this.artist(query, searchArtistCallback);
  }
  parseList(html) {
    const $ = cheerio.load(html);

    const str = $('div#wrapper div#wrapcontent.column div#content h1.inline').first().text().trim();

    if (str) {
      const split = str.split(' : ');
      if (split && split.length && split.length === 2) {
        const query = split[1];
      }
    }

    const releases = [];

    if ($('div#theList article.product').length > 0) {
      $('div#theList article.product').each(function () {
        let releaseTitle = null;
        let releaseYear = null;
        let releaseDate = null;
        const releaseCover = null;
        let releaseLabel = null;
        let releaseUrl = null;
        let releaseGenre = null;
        let releaseArtist = null;
        let releaseSourceId = null;

        const src = $(this).find('div.main_container div.cover div.img.img1 a.zoom').attr('href');
        if (src) {
          // releaseCover = 'http://www.deejay.de'+src;
        }

        const artistList = [];

        const a = $(this).find('div.main_container div.inner_container div.inner_a div.artikel h2.artist interprets a').text().trim();

        if (a && a !== 'Various Artists') {
          artistList.push(a);
        }

        const l = $(this).find('div.main_container div.inner_container div.inner_b div.label b').text().trim();
        if (l) {
          releaseLabel = l;
        }

        const sid = $(this).find('div.main_container div.inner_container div.inner_b div.label strong').text().trim();
        if (sid) {
          releaseSourceId = sid;
        }

        const t = $(this).find('div.main_container div.inner_container div.inner_a div.artikel h3.title a').first();
        if (t) {
          releaseTitle = S(t.text()).capitalize().s.trim();
          releaseUrl = `http://www.deejay.de${t.attr('href')}`;
        }

        const gs = [];
        $(this).find('div.product_footer div.product_footer_a div.style a.main').each((k, v) => {
          const a = $(v);
          gs.push(a.text());
        });

        if (gs.length > 0) {
          releaseGenre = gs.join(', ');
        }

        const rd = $(this).find('div.product_footer div.product_footer_a div.date').text().trim().split('.');

        if (rd && rd.length && rd.length === 3) {
          releaseYear = rd[2];
          const month = rd[1];
          const day = rd[0];
          releaseDate = `${releaseYear}-${month}-${day}`;
        }

        if (artistList.length < 1) {
          releaseArtist = null;
        } else if (artistList.length < 2) {
          releaseArtist = artistList[0];
        } else {
          releaseArtist = artistList.join(', ');
        }

        const tracks = [];

        $(this).find('div.main_container div.inner_container div.inner_a div.tracks ul.playtrack li a.track').each(function (k, v) {
          const t = {};
          const elt = $(v);
          const href = elt.attr('href');
          const id = elt.attr('id');
          if (href && id) {
            const sp = id.split('_');

            if (sp.length && sp.length === 3) {
              const domid = sp[1];

              if (`play/__${domid}_${sp[2]}` === href) {
                t.sample = `http://www.deejay.de/streamit/${domid[domid.length - 2]}/${domid[domid.length - 1]}/${domid}${sp[2]}.mp3`;
              }
            }
          }

          const str = $(this).find('em').first().text().trim();

          if (str) {
            t.title = str;

            const explode = t.title.split(' - ');
            const matches = t.title.match(new RegExp(/[ ]['].+[']/));

            if (matches) {
              t.title = S(matches[0]).replaceAll("'", '').s;
              t.title = S(t.title).replaceAll(' ', '').s;
              t.artist = matches.input.substr(0, matches.index);
            } else if (explode.length > 1) {
              t.artist = explode[0];
              t.title = explode[1];
            } else {
              t.artist = releaseArtist;
            }

            tracks.push(t);
          }
        });

        if (tracks.length > 0) {
          releases.push({
            kind: 'release',
            artist: releaseArtist,
            tracks,
            label: releaseLabel,
            cover: releaseCover,
            source: 'deejay.de',
            url: releaseUrl,
            title: releaseTitle,
            playedBy: releaseArtist,
            year: releaseYear,
            releaseDate,
            genre: releaseGenre
          });
        }
      });
    }
    return releases;
  }
  news(query, getNewsCallback) {
    const all = [];
    const $this = this;
    const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
    const url = `http://www.deejay.de/content.php?param=/m_All/sm_News?ddk=${today}`;
    diggingCloudCurl(url, (error, result) => {
      if (result.html) {
        const releases = $this.parseList(result.html);
        if (releases && releases.length && releases.length > 0) {
          releases.forEach((r) => {
            const release = diggingHelpers.prepareRelease($this, r);
            if (release) {
              all.push(release);
            }
          });
          getNewsCallback(all);
        } else {
          getNewsCallback([]);
        }
      } else {
        getNewsCallback([]);
      }
    });
  }
};
