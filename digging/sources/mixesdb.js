import _ from 'underscore';
import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import diggingCurl from '../diggingCurl';
import diggingHelpers from '../diggingHelpers';
import moment from 'moment';
import diggingCloudCurl from '../diggingCloudCurl';

export default class mixesdb {
  constructor() {
    this.name = 'MixesDb';
    this.value = 'mixesdb';
    this.logo = 'public/img/sources/mixesdb.png';
    this.color = '#FFA600';
  }
  artist(query, href, searchArtistCallback) {
    const $this = this;
    const pages = [];

    async.waterfall([
      function (waterfallCallback) {
        const url = `http://www.mixesdb.com/db/index.php?title=Special:Search&limit=1000&profile=all&search=${encodeURI(query)}`;
        diggingCurl(url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.html) {
          const $ = cheerio.load(result.html);

          $('.mw-search-results .mw-search-result-heading a').each((k, v) => {
            const href = `http://www.mixesdb.com${$(v).attr('href')}`;
            const title = `http://www.mixesdb.com${$(v).attr('title')}`;

            if (href) {
              pages.push({url: href, title});
            }
          });

          $('ul#catMixesList li a').each((k, v) => {
            const href = `http://www.mixesdb.com${$(v).attr('href')}`;
            const title = `http://www.mixesdb.com${$(v).attr('title')}`;

            if (href) {
              pages.push({url: href, title});
            }
          });
          waterfallCallback(null);
        }
      }
    ], (error, result) => {
      searchArtistCallback(pages);
    });
  }
  all(query, href, searchArtistCallback) {
    const $this = this;
    const pages = [];

    async.waterfall([
      function (waterfallCallback) {
        const url = `http://www.mixesdb.com/db/index.php?title=Special:Search&limit=1000&profile=all&search=${encodeURI(query)}`;
        diggingCurl(url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.html) {
          const $ = cheerio.load(result.html);

          $('.mw-search-results .mw-search-result-heading a').each((k, v) => {
            const href = `http://www.mixesdb.com${$(v).attr('href')}`;
            const title = `http://www.mixesdb.com${$(v).attr('title')}`;

            if (href) {
              pages.push({url: href, title});
            }
          });

          $('ul#catMixesList li a').each((k, v) => {
            const href = `http://www.mixesdb.com${$(v).attr('href')}`;
            const title = `http://www.mixesdb.com${$(v).attr('title')}`;

            if (href) {
              pages.push({url: href, title});
            }
          });
          waterfallCallback(null);
        }
      }
    ], (error, result) => {
      searchArtistCallback(pages);
    });
  }
  artist1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        diggingCurl(page.url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.release) {
          let release = diggingHelpers.prepareRelease($this, result.release);
          waterfallCallback(release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);

          if (r) {
            let release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  all1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        diggingCurl(page.url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.release) {
          waterfallCallback(result.release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);

          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  parseDetail(html, url) {
    const $ = cheerio.load(html);
    const cover = null;
    const label = null;
    let playedBy = null;
    const source = 'mixesdb';
    const tracks = [];
    let year = null;
    let releaseDate = null;

    let ep = $('head title').text();

    let matches = ep.match(new RegExp(/[0-9]{4}[-][0-9]{2}[-][0-9]{2}/i));
    if (matches) {
      ep = ep.replace(new RegExp(/[0-9]{4}[-][0-9]{2}[-][0-9]{2}/i), '');
      releaseDate = matches[0].trim();
    }

    matches = ep.match(new RegExp(/[0-9]{2}[:][0-9]{2}/i));
    if (matches) {
      ep = ep.replace(new RegExp(/[0-9]{2}[:][0-9]{2}/i), '');
    }

    matches = ep.match(new RegExp(/[\[][?]+[\]]/i));
    if (matches) {
      ep = ep.replace(new RegExp(/[\[][?]+[\]]/i), '');
    }

    ep = S(ep).replaceAll('-', ' ').s;
    ep = S(ep).replaceAll('−', ' ').s;
    ep = ep.replace('MixesDB', '');
    ep = S(ep).replaceAll('  ', ' ').s;

    $('#catlinks #mw-normal-catlinks ul li a').each((k, v) => {
      if (k === 0) {
        year = v.children[0].data;
      }

      if (k === 1) {
        playedBy = v.children[0].data;
      }
    });

    $('ol li').each(function (k, v) {
      let str = $(this).text().trim();
      str = str.replace(/(\[)\d+(:)\d+(\])/, '');
      str = str.replace(/(\[)\d+(\])/, '');
      str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/, '');
      str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/, '');
      str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/, '');

      if (str === '?' || str.length < 1) {

      } else {
        const ex = str.split(' - ');
        if (ex.length === 2) {
          const artist = ex[0];
          const title = ex[1];
          if (_.where(tracks, {artist, title}) <= 0) {
            tracks.push({artist, title});
          }
        }
      }
    });

    if (tracks.length < 1) {
      $('#Tracklist + div.list .list-track').each((k, v) => {
        let str = $(v).text().trim();

        str = str.replace(/(\[)\d+(:)\d+(\])/, '');
        str = str.replace(/(\[)\d+(\])/, '');
        str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/, '');
        str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/, '');
        str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/, '');
        str = str.trim();

        if (str === '?' || str.length < 1) {

        } else {
          const ex = str.split(' - ');
          if (ex.length === 2) {
            const artist = ex[0];
            const title = ex[1];
            if (_.where(tracks, {artist, title}) <= 0) {
              tracks.push({artist, title});
            }
          }
        }
      });
    }


    if (tracks.length < 1) {
      $('div.list .list-track').each((k, v) => {
        let str = $(v).text().trim();
        str = str.replace(/(\[)\d+(:)\d+(\])/, '');
        str = str.replace(/(\[)\d+(\])/, '');
        str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/, '');
        str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/, '');
        str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/, '');
        str = str.trim();

        if (str === '?' || str.length < 1) {

        } else {
          const ex = str.split(' - ');
          if (ex.length === 2) {
            const artist = ex[0];
            const title = ex[1];
            if (_.where(tracks, {artist, title}) <= 0) {
              tracks.push({artist, title});
            }
          }
        }
      });
    }

    if (tracks.length < 1) {
      $('div#bodyContent.mw-body-content div#mw-content-text.linkPreviewWrapperList ol li').each((k, v) => {
        let str = $(v).text().trim();

        str = str.replace(/(\[)\d+(:)\d+(\])/, '');
        str = str.replace(/(\[)\d+(\])/, '');
        str = str.replace(/\[[0-9]\:[[0-9]{2}\:[[0-9]{2}\]/, '');
        str = str.replace(/[0-9]{4}\-[[0-9]{2}\-[[0-9]{2}/, '');
        str = str.replace(/[0-9]{2}\:[[0-9]{2}\:[[0-9]{2}/, '');
        str = str.trim();

        if (str === '?' || str.length < 1) {

        } else {
          const ex = str.split(' - ');
          if (ex.length === 2) {
            const artist = ex[0];
            const title = ex[1];

            if (_.where(tracks, {artist, title}) <= 0) {
              tracks.push({artist, title});
            }
          }
        }
      });
    }

    if (tracks.length > 0) {
      return {
        kind: 'mix',
        releaseDate,
        artist: null,
        tracks,
        label,
        cover,
        source,
        url,
        title: ep,
        playedBy,
        year,
        genre: 'Electronic music'
      };
    }

    return null;
  }
  parseList(html) {
    const $ = cheerio.load(html);
    const data = [];


    $('ul.linkPreviewWrapperList a.cat-tlC').each((k, v) => {
      if (v.attribs.href.length > 0) {
        const link = `http://www.mixesdb.com${v.attribs.href}`;
        let ep = null;
        if (v.attribs.title.length > 0) {
          ep = v.attribs.title;
        }
        data.push({url: link, title: ep});
      }
    });

    return data;
  }
  news(query, getNewsCallback) {
    const $this = this;
    const all = [];
    const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
    diggingCloudCurl('http://www.mixesdb.com/db/index.php?title=Main_Page&show=mixesfresh&ddk='+today, (error, result) => {
      if (result && result.html) {
        const pages = $this.parseList(result.html);

        async.each(pages, (page, eachSeriesCb) => {
          diggingCurl(page.url, () => {
            if (result.release) {
              all.push(result.release);
            } else if (result.html) {
              const r = $this.parseDetail(result.html, page.url);
              if (r) {
                const release = diggingHelpers.prepareRelease($this, r);
                if (release) {
                  all.push(release);
                }
              }
            }
            eachSeriesCb(null);
          });
        }, () => {
          getNewsCallback(all);
        });
      } else {
        getNewsCallback([]);
      }
    });
  }
};
