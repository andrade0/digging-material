import _ from 'underscore';
import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import md5 from 'md5';
import moment from 'moment';
import Fuse from 'fuse.js';
import diggingCurl from '../diggingCurl';
import diggingHelpers from '../diggingHelpers';
import diggingCloudCurl from '../diggingCloudCurl';

export default class yoyaku {
  constructor() {
    this.name = 'Yoyaku';
    this.value = 'yoyaku';
    this.logo = 'public/img/sources/yoyaku.png';
    this.color = '#271F22';
  }
  artist(query, searchArtistCallback) {
    const $this = this;

    diggingCurl(`https://www.yoyaku.io/?s=${encodeURI(query)}&post_type=product&tags=1&ixwps=1`, (error, result) => {
      if (result.html) {
        const $ = cheerio.load(result.html);

        const releases = [];

        $('ul.products li.product').each(function () {
          const r = {
            kind: 'release',
            item_type: 'release',
            playedBy: null,
            year: null,
            releaseDate: null,
            genre: 'house'
          };
          r.url = $(this).find('a.woocommerce-LoopProduct-link').attr('href');
          r.cover = `http:${$(this).find('.image-and-action-button img.wp-post-image').attr('src')}`;
          r.title = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link h2').text();
          r.artist = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.artist span').text();
          r.label = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.label span').text();
          r.tracks = [];

          if (r.artist.trim().length <= 0) {
            r.artist = 'unknown';
          }

          $(this).find('div.yoyaku-desc div.action_button div.player div.fap-in-page-playlist-hidden span.fap-single-track').each(function () {
            const title_str = $(this).attr('data-title');
            const expl = title_str.split('&ndash;');
            if (expl.length > 1) {
              r.tracks.push({
                artist: expl[0].trim().substr(4),
                title: expl[1].trim(),
                sample: $(this).attr('data-href')
              });
            } else {
              r.tracks.push({
                artist: r.artist,
                title: title_str.trim().substr(4),
                sample: $(this).attr('data-href')
              });
            }
          });

          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              releases.push(release);
            }
          }
        });


        searchArtistCallback(releases);
      } else {
        searchArtistCallback([]);
      }
    });
  }
  parseDetail(html, page_url) {
    const $ = cheerio.load(html);
    let release = null;
    const artists_list = [];
    const releaseCover = $('.attachment-shop_single.size-shop_single.wp-post-image').attr('src');
    const releaseTitle = $('.summary.entry-summary .title.table span.results').text();
    const releaseLabel = $('.summary.entry-summary .label.table span.results a').text();
    const releaseArtist = [];
    $('.summary.entry-summary .artist.table span.results a').each(function () {
      releaseArtist.push($(this).text());
    });
    const releaseYear = null;
    const releaseDate = null;
    const releaseGenre = [];
    $('.summary.entry-summary .style.table span.results a').each(function () {
      releaseGenre.push($(this).text());
    });

    const tracks = [];
    $('.yoyaku-single-product-player .yoyaku-player-single-product span.fap-single-track').each(function () {
      const title_str = $(this).attr('data-title');//

      const expl = title_str.split('&ndash;');

      if (expl.length > 1) {
        tracks.push({
          artist: expl[0].trim().substr(4),
          title: expl[1].trim(),
          sample: $(this).attr('data-href')
        });
      } else {
        tracks.push({
          artist: releaseArtist.join(''),
          title: title_str.trim().substr(4),
          sample: $(this).attr('data-href')
        });
      }
    });

    release = {
      artist: releaseArtist.join(', '),
      title: releaseTitle,
      label: releaseLabel,
      genre: releaseGenre.join(', '),
      tracks,
      url: page_url,
      kind: 'release',
      source: 'yoyaku',
      cover: releaseCover,
      playedBy: null,
      releaseDate: null,
      year: null
    };

    return release;
  }
  label(query, searchArtistCallback) {
    this.artist(query, searchArtistCallback);
  }
  all(query, searchArtistCallback) {
    this.artist(query, searchArtistCallback);
  }
  release(url, callback) {
    const $this = this;
    diggingCurl(url, (error, result) => {
      if (result.html) {
        const r = $this.parseDetail(result.html, url);
        if (r) {
          const release = diggingHelpers.prepareRelease($this, r);
          callback(release);
        } else {
          callback(null);
        }
      } else {
        callback(null);
      }
    });
  }
  news(query, getNewsCallback) {
    const $this = this;
    const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
    diggingCloudCurl('https://www.yoyaku.io/all-stock/?ddk='+today, (error, result) => {
      if (result.html) {
        const $ = cheerio.load(result.html);

        const releases = [];

        $('ul.products li.product').each(function () {
          const r = {
            kind: 'release',
            item_type: 'release',
            playedBy: null,
            year: null,
            releaseDate: null,
            genre: 'house'
          };
          r.url = $(this).find('a.woocommerce-LoopProduct-link').attr('href');
          r.cover = `http:${$(this).find('.image-and-action-button img.wp-post-image').attr('src')}`;
          r.title = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link h2').text();
          r.artist = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.artist span').text();
          r.label = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.label span').text();
          r.tracks = [];
          $(this).find('div.yoyaku-desc div.action_button div.player div.fap-in-page-playlist-hidden span.fap-single-track').each(function () {
            const title_str = $(this).attr('data-title');
            const expl = title_str.split('&ndash;');
            if (expl.length > 1) {
              r.tracks.push({
                artist: expl[0].trim().substr(4),
                title: expl[1].trim(),
                sample: $(this).attr('data-href')
              });
            } else {
              r.tracks.push({
                artist: r.artist,
                title: title_str.trim().substr(4),
                sample: $(this).attr('data-href')
              });
            }
          });
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              releases.push(release);
            }
          }
        });

        getNewsCallback(releases);
      } else {
        getNewsCallback([]);
      }
    });
  }
};
