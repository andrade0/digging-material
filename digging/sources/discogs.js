import _ from 'underscore';
import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import moment from 'moment';
import Fuse from 'fuse.js';
import diggingCurl from '../diggingCurl';
import diggingHelpers from '../diggingHelpers';
import diggingCloudCurl from '../diggingCloudCurl';

export default class discogs {
  constructor() {
    this.name = 'Discogs';
    this.value = 'discogs';
    this.logo = 'public/img/sources/discogs.png';
    this.color = '#333333';
  }
  artist(query, href, searchArtistCallback) {
    if (href) {
      href = `${href}?limit=500`;
    }
    const $this = this;
    const pages = [];

    async.waterfall([
      function (waterfallCallback) {
        if (href) {
          waterfallCallback(null, null);
        } else {
          const url = `http://www.discogs.com/search/?q=${encodeURI(query)}&type=artist`;
          diggingCurl(url, waterfallCallback);
        }
      },
      function (result, waterfallCallback) {
        if (href) {
          waterfallCallback(null, href);
        } else {
          let url = null;
          if (result.html) {
            const discogsResults = [];
            const $ = cheerio.load(result.html);
            $('a.search_result_title').each(function () {
              discogsResults.push({
                title: $(this).text().toLowerCase().trim(),
                href: `https://www.discogs.com${$(this).attr('href')}`
              });
            });
            const options = {
              keys: ['title'],
              id: 'href'
            };

            const fuse = new Fuse(discogsResults, options);
            const fuseResult = fuse.search(query);

            if (fuseResult.length > 0 && fuseResult[0]) {
              url = fuseResult[0];
            }

            waterfallCallback(null, url);
          } else {
            waterfallCallback(true);
          }
        }
      },
      function (url, waterfallCallback) {
        if (url) {
          diggingCurl(url, waterfallCallback);
        } else {
          waterfallCallback(true);
        }
      },
      function (result, waterfallCallback) {
        if (result.html) {
          const $ = cheerio.load(result.html);

          $('#artist tr.card td.title > a').each((k, v) => {
            const url = `https://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url});
          });

          $('#label tr.card td.title > a').each((k, v) => {
            const url = `https://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url});
          });

          $('#search_results .card h4 .search_result_title').each((k, v) => {
            const href = `http://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url: href});
          });
          waterfallCallback(null);
        } else {
          waterfallCallback(true);
        }
      }
    ], (err) => {
      searchArtistCallback(pages);
    });
  }
  all(query, href, searchArtistCallback) {
    if (href) {
      href = `${href}?limit=500`;
    }
    const $this = this;
    const pages = [];

    async.waterfall([
      function (waterfallCallback) {
        if (href) {
          waterfallCallback(null, null);
        } else {
          const url = `http://www.discogs.com/search/?q=${encodeURI(query)}`;
          diggingCurl(url, waterfallCallback);
        }
      },
      function (result, waterfallCallback) {
        if (href) {
          waterfallCallback(null, href);
        } else {
          let url = null;
          if (result.html) {
            const discogsResults = [];
            const $ = cheerio.load(result.html);
            $('a.search_result_title').each(function () {
              discogsResults.push({
                title: $(this).text().toLowerCase().trim(),
                href: `https://www.discogs.com${$(this).attr('href')}`
              });
            });
            const options = {
              keys: ['title'],
              id: 'href'
            };



            const fuse = new Fuse(discogsResults, options);
            const fuseResult = fuse.search(query);

            if (fuseResult.length > 0 && fuseResult[0]) {
              url = fuseResult[0];
            }

            waterfallCallback(null, url);
          } else {
            waterfallCallback(true);
          }
        }
      },
      function (url, waterfallCallback) {
        if (url) {
          pages.push({url: url});
        }
        waterfallCallback(true);
      }
    ], (err) => {
      searchArtistCallback(pages);
    });
  }
  artist1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        diggingCurl(page.url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.release) {
          const release = diggingHelpers.prepareRelease($this, result.release);
          waterfallCallback(release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  all1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        diggingCurl(page.url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.release) {
          const release = diggingHelpers.prepareRelease($this, result.release);
          waterfallCallback(release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  parseDetail(html, page_url) {
    const $ = cheerio.load(html);
    let release = null;
    const artists_list = [];
    $('#profile_title > span:nth-child(1) > span').each(function(){
      artists_list.push($(this).find('a').text());
    });
    const genres_list = [];
    $('#page_content > div.body > div.profile a').each(function(){
      if($(this).attr('href').match(new RegExp('style', 'i'))){
        genres_list.push($(this).text().trim());
      }
    });
    let discogsDateField = null;
    $('#page_content > div.body > div.profile a').each(function(){
      if($(this).attr('href').match(new RegExp('&year', 'i'))){
        discogsDateField = $(this).text().trim();
      }
    });

    let releaseYear = null;
    let releaseDate = null;

    if(discogsDateField){

      const months = [];
      months.push('');
      months.push('Jan');
      months.push('Feb');
      months.push('Mar');
      months.push('Apr');
      months.push('May');
      months.push('Jun');
      months.push('Jul');
      months.push('Aug');
      months.push('Sep');
      months.push('Oct');
      months.push('Nov');
      months.push('Dec');

      const expDate = discogsDateField.split(' ');
      switch(expDate.length){
        case 1:
          releaseYear = expDate[0];
          releaseDate = expDate[0]+'-01-01';
          break;
        case 2:
          releaseYear = expDate[1];
          let monthIndex = months.indexOf(expDate[0]);
          let month = null;
          if(monthIndex.toString().length === 1) {
            month = '0'+monthIndex;
          } else {
            month = monthIndex.toString();
          }
          if(month){
            releaseDate = expDate[1]+'-'+month+'-01';
          }
          break;
        case 3:
          releaseYear = expDate[2];
          let monthIndex2 = months.indexOf(expDate[1]);
          let month2 = null;
          if(monthIndex2.toString().length === 1) {
            month2 = '0'+monthIndex2;
          } else {
            month2 = monthIndex2.toString();
          }
          if(month2){
            releaseDate = expDate[2]+'-'+month2+'-'+expDate[0];
          }
          break;
        default:
          break;
      }
    }

    const releaseArtist = artists_list.join(', ');
    const releaseCover = $('#page_content > div.body > div.image_gallery.image_gallery_large > a > span.thumbnail_center > img').attr('src');
    const releaseTitle = $('#profile_title > span:nth-child(2)').text();
    const releaseLabel = $('#page_content > div.body > div.profile > div:nth-child(3) > a').text();
    const releaseGenre = genres_list.join(', ');


    const tracks = [];
    $('#tracklist').find('table.playlist').find('tr.track').each(function(){
      let trackArtist = $(this).find('td.tracklist_track_artists a').text().trim();
      const tracktitle = $(this).find('td.tracklist_track_title span.tracklist_track_title').text().trim();
      if(trackArtist.length === 0) {
        trackArtist = releaseArtist;
      }
      tracks.push({
        artist: trackArtist,
        title: tracktitle,
        sample: null
      });
    });

    release = {
      kind: 'release',
      artist: releaseArtist,
      tracks,
      label: releaseLabel,
      cover: releaseCover,
      source: 'discogs',
      url: page_url,
      title: releaseTitle,
      playedBy: releaseArtist,
      year: releaseYear,
      genre: releaseGenre,
      releaseDate
    };

    return release;
  }
  label(query, href, searchArtistCallback) {
    if (href) {
      href = `${href}?limit=500`;
    }
    const $this = this;
    const pages = [];
    async.waterfall([
      function (waterfallCallback) {
        if (href) {
          waterfallCallback(null, null);
        } else {
          const url = `http://www.discogs.com/search/?q=${encodeURI(query)}&type=label`;
          diggingCurl(url, waterfallCallback);
        }
      },
      function (result, waterfallCallback) {
        if (href) {
          waterfallCallback(null, href);
        } else {
          let url = null;
          if (result.html) {
            const discogsResults = [];
            const $ = cheerio.load(result.html);
            $('a.search_result_title').each(function () {
              discogsResults.push({
                title: $(this).text().toLowerCase().trim(),
                href: `https://www.discogs.com${$(this).attr('href')}`
              });
            });
            const options = {
              keys: ['title'],
              id: 'href'
            };
            const fuse = new Fuse(discogsResults, options);
            const fuseResult = fuse.search(query);

            if (fuseResult.length > 0 && fuseResult[0]) {
              url = fuseResult[0];
            }

            waterfallCallback(null, url);
          } else {
            waterfallCallback(true);
          }
        }
      },
      function (url, waterfallCallback) {
        if (url) {
          diggingCurl(url, waterfallCallback);
        } else {
          waterfallCallback(true);
        }
      },
      function (result, waterfallCallback) {
        if (result.html) {
          const $ = cheerio.load(result.html);

          $('#artist tr.card td.title > a').each((k, v) => {
            const url = `https://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url});
          });

          $('#label tr.card td.title > a').each((k, v) => {
            const url = `https://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url});
          });

          $('#search_results .card h4 .search_result_title').each((k, v) => {
            const href = `http://www.discogs.com${$(v).attr('href')}`;
            pages.push({discogs_id: null, url: href});
          });
          waterfallCallback(null);
        } else {
          waterfallCallback(true);
        }
      }
    ], (err) => {
      searchArtistCallback(pages);
    });
  }
  label1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        diggingCurl(page.url, waterfallCallback);
      },
      function (result, waterfallCallback) {
        if (result.release) {
          const release = diggingHelpers.prepareRelease($this, result.release);
          waterfallCallback(release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  release(url, callback) {
    const $this = this;
    diggingCurl(url, (error, result) => {
      if (result.html) {
        const r = $this.parseDetail(result.html, url);
        if (r) {
          const release = diggingHelpers.prepareRelease($this, r);
          callback(release);
        } else {
          callback(null);
        }
      } else {
        callback(null);
      }
    });
  }
  suggestion(track, callback) {
    const $this = this;
    diggingCurl(`https://www.discogs.com/fr/search/?q=${encodeURI(`${track.artist} ${track.title}`)}&type=release`, (error, result) => {
      if (result.html) {
        const $ = cheerio.load(result.html);

        const href = $('a.search_result_title').first().attr('href');

        if (href) {
          const expl = $('a.search_result_title').first().attr('href').split('/');

          const id = expl[expl.length - 1];

          diggingCurl(`https://www.discogs.com/release/recs/${id}?type=release&page=1`, (error, result) => {
            const r = $this.parse_suggestions(result.html, (releases) => {
              if (releases) {
                callback(releases);
              } else {
                callback([]);
              }
            });
          });
        } else {
          callback([]);
        }
      } else {
        callback([]);
      }
    });
  }
  parse_suggestions(html, callback) {
    const $this = this;
    const $ = cheerio.load(html);
    const releaseUrl = $('div.card').first().find('a.thumbnail_link').first().attr('href');

    const releases = [];
    async.each($('div.card'), (element, cb) => {
      const releaseUrl = $(element).find('a.thumbnail_link').first().attr('href');
      const suggestion_url = `https://www.discogs.com${releaseUrl}`;
      diggingCurl(suggestion_url, (error, result) => {
        if (result.release) {
          releases.push(release);
          cb(null);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, suggestion_url);
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);
            releases.push(release);
            cb(null);
          } else {
            cb(null);
          }
        } else {
          cb(null);
        }
      });
    }, (e, r) => {
      callback(releases);
    });
  } /* ,
   getNews: function(query, getNewsCallback){
   const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
   var $this = this;
   var pages = [];
   async.waterfall([
   function(waterfallCallback)
   {
   var url = "https://www.discogs.com/?limit=50&page=1&ddk="+today;
   diggingCloudCurl(url, waterfallCallback);
   },
   function(result ,waterfallCallback)
   {
   if(result.html)
   {
   var $ = cheerio.load(result.html);

   $('#search_results div.card > a').each(function(k, v){
   var url = 'https://www.discogs.com'+$(v).attr('href');
   pages.push({discogs_id: null, url: url});
   });

   waterfallCallback(null);
   }
   else
   {
   waterfallCallback(true);
   }
   },
   function(waterfallCallback)
   {
   diggingCloudCurl(page.url,  waterfallCallback);
   },
   function(result, waterfallCallback)
   {
   if(result.html)
   {
   var r = $this.parseDetail(result.html, page.url);
   if(r)
   {
   var release = diggingHelpers.prepareRelease($this, r);

   if(release)
   {
   waterfallCallback(release)
   }
   else
   {
   waterfallCallback(null);
   }
   }
   else
   {
   waterfallCallback(null);
   }
   }
   else
   {
   waterfallCallback(null);
   }
   }
   ], function(err){
   console.log(pages);
   //getNewsCallback(pages);
   });
   }*/
};
