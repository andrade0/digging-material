import async from 'async';
import S from 'string';
import cheerio from 'cheerio';
import md5 from 'md5';
import moment from 'moment';
import Fuse from 'fuse.js';
import diggingCurl from './diggingCurl';

/*
 var params = {};

 params.index = 'digging';
 params.type = 'release';
 params.body = {"release" : {
 "properties" : {
 "title" : {"type" : "string", "store" : true },
 "url" : {"type" : "string", "store" : true },
 "cover" : {"type" : "string", "store" : true },
 "label" : {"type" : "string", "store" : true },
 "artist" : {"type" : "string", "store" : true },
 "source" : {"type" : "string", "store" : true },
 "playedBy" : {"type" : "string", "store" : true },
 "kind" : {"type" : "string", "store" : true },
 "source" : {"type" : "string", "store" : true },
 "year" : {"type" : "string", "store" : true },
 "alias" : {"type" : "string", "store" : true },
 "releaseDate" : {"type" : "string", "store" : true },
 "query" : {"type" : "string", "store" : true },
 "tms" : {"type" : "integer", "store" : true },
 "date" : {"type" : "date", "store" : true },
 "tracks" : {"type" : "nested", "include_in_parent" : true, "properties" : {
 "sample": {"type" : "string", "store" : true },
 "title": {"type" : "string", "store" : true },
 "artist": {"type" : "string", "store" : true },
 "tms": {"type" : "string", "store" : true },
 "label": {"type" : "string", "store" : true },
 "duration": {"type" : "string", "store" : true },
 "releaseDate": {"type" : "string", "store" : true },
 "genre": {"type" : "string", "store" : true },
 "playedBy": {"type" : "string", "store" : true },
 "year": {"type" : "string", "store" : true },
 "kind": {"type" : "string", "store" : true },
 "cover": {"type" : "string", "store" : true },
 "source": {"type" : "string", "store" : true },
 "logo": {"type" : "string", "store" : true },
 "youtube": {"type" : "string", "store" : true },
 "query": {"type" : "string", "store" : true },
 "alias": {"type" : "string", "store" : true },
 "_id": {"type" : "string", "store" : true }
 }}
 }
 }};

 ElastiClient.indices.putMapping(params, function(error, result){
 console.log(error);
 console.log(result);
 });


 */


class DiggingHelpers {
  defineSearchType(query, getSearchTypeCallback) {
    query = S(query).replaceAll('(', '').s;
    query = S(query).replaceAll(')', '').s;
    query = S(query).humanize().s;
    query = query.replace(/[^\w\s]/gi, '');
    query = query.toLowerCase();

    const discogsResults = [];

    async.waterfall([
      function (parallelCb) {
        const url = `https://www.discogs.com/search/?q=${encodeURI(query)}&type=all`;
        diggingCurl(url, (error, result) => {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let countArtists = 0;
            let countLabel = 0;
            let countRelease = 0;
            $('#search_results .card h4 a.search_result_title').each((k, v) => {
              const elt = $(v).first();
              const text = $(elt).text().trim().toLowerCase();
              text.replace(new RegExp('\(.*\)', 'i'), '');
              const href = $(elt).attr('href');
              const img = $(elt).parent().prev().find('img').attr('data-src');

              if (href.match('/artist/') && countArtists < 3) {
                discogsResults.push({
                  title: text,
                  href: `https://www.discogs.com${href}`,
                  img,
                  type: 'artist'
                });
                countArtists += 1;
              } else if (href.match('/label/') && countLabel < 3) {
                discogsResults.push({
                  title: text,
                  href: `https://www.discogs.com${href}`,
                  img,
                  type: 'label'
                });
                countLabel += 1;
              } else if (href.match('/release/') && countRelease < 3) {
                discogsResults.push({
                  title: text,
                  href: `https://www.discogs.com${href}`,
                  img,
                  type: 'release'
                });
                countRelease += 1;
              }
            });
            parallelCb(null);
          }
        });
      },
      function (parallelCb) {
        const url = `https://www.residentadvisor.net/search.aspx?searchstr=${encodeURI(query)}&section=events&titles=1`;
        diggingCurl(url, (error, result) => {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let countEvents = 0;
            $('main ul.content-list.search li section.content div.generic.events div ul.list li a').each((k, v) => {
              const elt = $(v).first();
              const text = $(elt).text().trim().toLowerCase();
              const href = $(elt).attr('href');
              const img = null;

              if (countEvents < 3) {
                discogsResults.push({
                  title: text,
                  href: `https://www.discogs.com${href}`,
                  img,
                  type: 'event'
                });
                countEvents += 1;
              }
            });
          }
          parallelCb(null);
        });
      }
    ], (result) => {
      getSearchTypeCallback(discogsResults);
    });
  }

  getSearchType(query, getSearchTypeCallback) {
    query = S(query).replaceAll('(', '').s;
    query = S(query).replaceAll(')', '').s;
    query = S(query).humanize().s;
    query = query.replace(/[^\w\s]/gi, '');
    query = query.toLowerCase();

    async.waterfall([
      function (callback) {
        const url = `https://www.discogs.com/search/?q=${query}&type=all`;
        diggingCurl(url, callback);
      },
      function (result, callback) {
        if (result && result.html) {
          const $ = cheerio.load(result.html);
          const discogsResults = [];
          $('#search_results .card h4 a.search_result_title').each((k, v) => {
            const elt = $(v).first();
            const text = $(elt).text().trim().toLowerCase();
            const href = $(elt).attr('href');

            if (href.match('/artist/') || href.match('/label/')) {
              discogsResults.push({title: text, href});
            }
          });

          const options = {
            keys: ['title'],
            id: 'href'
          };
          const fuse = new Fuse(discogsResults, options);
          const fuseResult = fuse.search(query);

          if (fuseResult.length > 0) {
            const fuseUrlBestMatch = fuseResult[0];

            if (fuseUrlBestMatch.match('/artist/')) {
              callback({searchType: 'artist'});
            } else if (fuseUrlBestMatch.match('/label/')) {
              callback({searchType: 'label'});
            } else {
              callback(null);
            }
          } else {
            callback(null);
          }
        } else {
          callback(null);
        }
      }
    ], (error, result) => {
      let searchType = 'all';
      if (error && error.searchType) {
        searchType = error.searchType;
      } else if (result && result.searchType) {
        searchType = result.searchType;
      }
      getSearchTypeCallback(searchType);
    });
  }
  prepareRelease(source, doc) {
    const $this = this;

    if (!doc.tms) {
      const tms = new Date().getTime();
      doc.tms = tms;
    }

    if (!doc.logo && source.logo) {
      doc.logo = source.logo;
    }

    if (!doc.item_type) {
      doc.item_type = 'release';
    }

    if (!doc.cover || (doc.cover && doc.cover.length <= 0)) {
      doc.cover = '/public/img/vinyl-digging-transparent.png';
    }

    if (doc.ep && !doc.title) {
      doc.title = doc.ep;
    }

    if (doc.artist) {
      doc.artist = doc.artist.replace(new RegExp('\[.*\]', 'i'), '');
      doc.artist = doc.artist.trim().toLowerCase();
      doc.artist = S(doc.artist).capitalize().s;
    }

    if (doc.playedBy) {
      doc.playedBy = doc.playedBy.trim().toLowerCase();
      doc.playedBy = S(doc.playedBy).capitalize().s;
    }

    if (doc.title) {
      doc.title = doc.title.trim().toLowerCase();
      doc.title = S(doc.title).capitalize().s;
    }

    if (doc.genre) {
      doc.genre = doc.genre.trim().toLowerCase();
      doc.genre = S(doc.genre).capitalize().s;
    }

    if (doc.label) {
      doc.label = doc.label.trim().toLowerCase();
      doc.label.replace('germany', '');
      doc.label.replace('france', '');
      doc.label.replace('japan', '');
      doc.label.replace('greece', '');
      doc.label.replace('spain', '');
      doc.label.replace('uk', '');
      doc.label.replace('italy', '');
      doc.label.replace('us', '');
      doc.label = S(doc.label).capitalize().s;
    }

    doc.source = source.value.trim().toLowerCase();

    const keywords = [doc.title, doc.artist, doc.label, doc.genre, doc.playedBy, doc.kind, doc.source];

    if (doc.url && doc.url.length && doc.url.length > 0 && doc.tracks && doc.tracks.length && doc.tracks.length > 0) {
      doc.tracks.forEach((v, k) => {
        if (v.title && v.artist && v.artist.trim().length > 0 && v.title.trim().length > 0) {
          v.title = v.title.replace(new RegExp('\[.*\]', 'i'), '');
          v.artist = v.artist.replace(new RegExp('\[.*\]', 'i'), '');

          v.artist = S(v.artist.trim()).capitalize().s;
          v.title = S(v.title.trim()).capitalize().s;

          if (!v.item_type) {
            v.item_type = 'track';
          }

          if (!v.logo && doc.logo) {
            v.logo = doc.logo;
          }

          v.releaseTitle = doc.title;

          v.tms = doc.tms;

          if (doc.label && !v.label) {
            v.label = doc.label;
          } else if (v.label) {
            v.label = S(v.label.trim()).capitalize().s;
          }

          if (v.label) {
            v.label = v.label.trim().toLowerCase();
            v.label.replace('germany', '');
            v.label.replace('france', '');
            v.label.replace('japan', '');
            v.label.replace('greece', '');
            v.label.replace('spain', '');
            v.label.replace('uk', '');
            v.label.replace('italy', '');
            v.label.replace('us', '');
            v.label = S(v.label).capitalize().s;
          }

          if (!v.duration) {
            v.duration = null;
          }

          if (doc.releaseDate) {
            v.releaseDate = moment(doc.releaseDate).unix();
          } else {
            v.releaseDate = moment().unix();
          }

          if (doc.genre && !v.genre) {
            v.genre = doc.genre;
          } else if (v.genre) {
            v.genre = S(v.genre.trim()).capitalize().s;
          }

          if (doc.playedBy && !v.playedBy) {
            v.playedBy = doc.playedBy;
          } else if (v.playedBy) {
            v.playedBy = S(v.playedBy.trim()).capitalize().s;
          }

          v.url = doc.url;

          v.releaseDate = doc.releaseDate;
          v.year = doc.year;
          v.kind = doc.kind;
          v.cover = doc.cover;
          v.source = doc.source;
          v.logo = source.logo;


          if (!v.youtube) {
            v.youtube = '';
          }

          keywords.push(v.title);

          if ((doc.artist && v.artist) && doc.artist.toLowerCase().trim() !== v.artist.toLowerCase().trim()) {
            keywords.push(v.artist);
          }

          const keywords_track = [doc.title, doc.artist, doc.label, doc.genre, doc.playedBy, doc.kind, doc.source, v.title, v.artist];

          if (!v.query) {
            v.query = keywords_track.join(' ');
          }

          let alias = S(`${v.artist}-${doc.title}-${v.title}`).latinise().s;
          alias = S(alias).strip("'", '"', '!', '`', '*', '£', 'ù', '%', '$', '¨', '°', '(', ')', '§', '[', ']', '{', '}', ',').s;
          alias = S(alias).humanize().s;
          alias = alias.toLowerCase();
          alias = S(alias).replaceAll(' ', '-').s;
          alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

          if (!v.alias) {
            v.alias = alias;
          }

          if (!v.id) {
            v.id = md5(v.alias + k);
          }
        } else {
          delete doc.tracks[k];
        }
      });

      if (!doc.query) {
        doc.query = keywords.join(' ');
      }

      let alias = null;

      if (doc.artist) {
        alias = S(`${doc.artist}-${doc.title}`).latinise().s;
      } else {
        alias = S(doc.title).latinise().s;
      }

      alias = S(alias).strip("'", '"', '!', '`', '*', '£', 'ù', '%', '$', '¨', '°', '(', ')', '§', '[', ']', '{', '}', ',').s;
      alias = S(alias).humanize().s;
      alias = alias.toLowerCase();
      alias = S(alias).replaceAll(' ', '-').s;
      alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

      if (!doc.alias) {
        doc.alias = alias;
      }

      doc.id = doc.alias;

      // $this.insert_release_in_parse([doc]);

      return doc;
    }
    return null;
  }
  insert_release_in_parse(releases) {
    releases.forEach((release) => {
      releases.forEach((release) => {

        // client.post('/insert', {release: release},function(){});

      });
    });
  }
  youtube(q, diggingYoutubeSearchCallback) {
    if (q && q.length && q.length > 0) {
      q = S(q).replaceAll('(', '').s;
      q = S(q).replaceAll(')', '').s;
      q = S(q).humanize('}', '').s;
      q = q.replace(/[^\w\s]/gi, '');

      const host = 'https://www.googleapis.com';
      const path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
      const url = host + path + q;
      diggingCurl(url, false, false, (err, res) => {
        if (res.html) {
          const allResults = [];
          const youtubeData = JSON.parse(res.html);

          if (youtubeData && youtubeData.items && youtubeData.items.length > 0 && youtubeData.items[0]) {
            youtubeData.items.forEach((item) => {
              if (item.id.kind === 'youtube#video') {
                const theTitle = item.snippet.title;

                // if(digging_contains_all_words(q, theTitle))
                // {
                allResults.push({title: theTitle, id: item.id.videoId});
                // }
              }
            });

            if (allResults.length > 0) {
              const options = {
                /* threshold: 0.4,*/
                keys: ['title', 'id']
              };

              const fuse = new Fuse(allResults, options);
              const fuseResult = fuse.search(q);

              if (fuseResult.length > 0) {
                const fuseUrlBestMatch = fuseResult[0];

                console.log(fuseUrlBestMatch);
                diggingYoutubeSearchCallback(null, fuseUrlBestMatch);
              } else {
                diggingYoutubeSearchCallback(null, null);
              }
            } else {
              diggingYoutubeSearchCallback(null, null);
            }
          } else {
            diggingYoutubeSearchCallback(null, null);
          }
        } else {
          diggingYoutubeSearchCallback(null, null);
        }
      });
    } else {
      diggingYoutubeSearchCallback(null, null);
    }
  }
}
const diggingHelpers = new DiggingHelpers();
export default diggingHelpers;
