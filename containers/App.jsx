import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import Header from '../components/Header';
import DigginUser from '../models/DiggingUser';
import {withRouter} from 'react-router'
import DiggingCssModel from '../models/DiggingCssModel';
import async from 'async';
// For Customization Options, edit  or use
// './src/material_ui_raw_theme_file.jsx' as a template.
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
const theme = createMuiTheme();
import DiggingLoadingOverlay from '../components/DiggingLoadingOverlay';
import DiggingLogin from '../components/DiggingLogin';
import DiggingNoExtensionActive from '../components/DiggingNoExtensionActive';
import DiggingModel from '../models/DiggingModel';
import diggingCurl from '../digging/diggingCurl';
import DiggingUser from '../models/DiggingUser';
import DiggingLikesModel from '../models/DiggingLikesModel';
import fire from '../models/fire';
import firebase from 'firebase';



@observer
class App extends Component {
  constructor() {
    super();
    this.state = {
      extentionIsActive: false
    };
  }
  componentWillMount(){

    const { history } = this.props;
    this.unsubscribeFromHistory = history.listen(this.handleLocationChange);
    this.handleLocationChange(history.location);


    diggingCurl('http://www.discogs.com', (error, result)=> {
      DiggingModel.startupLoaderProgress = 100;
      if (!error && result && result.html) {
        DiggingModel.extentionIsActive = true;
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            DiggingUser.loadUserData(()=>{
              DiggingModel.loggingCalled = true;
            });
          } else {
            DiggingModel.loggingCalled = true;
          }
        });
      } else {
        DiggingModel.extentionIsActive = false;
      }
    });
  }

  componentWillUnmount() {
    if (this.unsubscribeFromHistory) this.unsubscribeFromHistory();
  }

  handleLocationChange = (location) => {
    DiggingCssModel.resizeApp();
  };

  render() {

    let content = <DiggingNoExtensionActive />;
    if(DiggingModel.extentionIsActive === false){

    } else if(DiggingModel.extentionIsActive === true && !DiggingModel.loggingCalled){

    } else if (DiggingModel.extentionIsActive === true && DiggingModel.loggingCalled && DiggingUser.userId){
      content = <Header />;
    } else {
      content = <DiggingLogin />
    }

    return (
      <div>
        <DiggingLoadingOverlay />
          <MuiThemeProvider theme={theme}>
            <div>
              {content}
            </div>
          </MuiThemeProvider>
      </div>
    );
  }
}

App.propTypes = {

};

export default withRouter(App);
