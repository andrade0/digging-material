import md5 from 'md5';
import _ from 'underscore';
import async from 'async';
import S from 'string';
import jQuery from 'jquery';
import Fuse from 'fuse.js';
import user from './user';
import sourcesObjects from './digging/sources';
import diggingCurl from './digging/diggingCurl';
import diggingHelpers from './digging/diggingHelpers';
import tabsStore from './stores/tabsStore';


class digging2 {

  constructor() {
    this.diggingBuffer = {};
    this.activeTab = null;
    this.newsTabId = md5('news');
    this.getNews = this.getNews.bind(this);
    this.setActiveTab = this.setActiveTab.bind(this);
    this.setActiveCol = this.setActiveCol.bind(this);
    this.getActiveItemList = this.getActiveItemList.bind(this);
    this.showActiveCol = this.showActiveCol.bind(this);
    this.resizeTabs = this.resizeTabs.bind(this);
    this.removeTab = this.removeTab.bind(this);
    this.tabExists = this.tabExists.bind(this);
    this.youtube = this.youtube.bind(this);
    this.sources = null;
  }

  sendItem(item) {
    if (this.lists && this.lists[item.tabId] && this.lists[item.tabId][item.col]) {
      this.lists[item.tabId][item.col].count += 1;
    }

    const tabId = item.tabId;
    const col = item.col;

    if (!this.diggingBuffer[tabId]) {
      this.diggingBuffer[tabId] = {};
    }

    if (!this.diggingBuffer[tabId][col]) {
      this.diggingBuffer[tabId][col] = [];
    }

    if (this.lists && this.lists[item.tabId] && this.lists[item.tabId][item.col] && this.lists[item.tabId][item.col].items) {
      if (_.where(this.lists[tabId][col].items, { id: item.id }).length <= 0) {
        this.diggingBuffer[tabId][col].push(item);
      }
    } else {
      this.diggingBuffer[tabId][col].push(item);
    }
  }

  setActiveTab(id) {
    this.activeTab = id;
    this.showActiveCol();
  }

  setActiveCol(id, col) {
    tabsStore.tabs[id].activeCol = col;
    this.showActiveCol();
  }

  getActiveItemList() {
    if (this.activeTab && tabsStore.tabs[this.activeTab] && tabsStore.tabs[this.activeTab].activeCol) {
      return { id: this.activeTab, col: tabsStore.tabs[this.activeTab].activeCol };
    }

    return null;
  }

  showActiveCol() {
    const activeItemList = this.getActiveItemList();

    if (activeItemList) {
      jQuery('.tab').removeClass('active');
      jQuery(`.${activeItemList.id}`).addClass('active');
      jQuery(`.digging-content.tab.${activeItemList.id} .col`).removeClass('active');
      jQuery(`.digging-content.tab.${activeItemList.id} .col${activeItemList.col}`).addClass('active');
      jQuery(`.digging-content.tab.${activeItemList.id} .colcontent`).removeClass('active');
      jQuery(`.digging-content.tab.${activeItemList.id} .colcontent${activeItemList.col}`).addClass('active');
    }
  }

  resizeTabs() {
    const activeItemList = this.getActiveItemList();

    if (activeItemList) {
      jQuery('.tab').removeClass('active');
      jQuery(`.${activeItemList.id}`).addClass('active');

      const tabsCount = jQuery('.digging-tabs > div').length;

      if (tabsCount > 10) {
        jQuery(`.digging-tabs > div.tab:not(.${activeItemList.id})`).animate({
          maxWidth: '80px'
        }, 500);
      } else if (tabsCount > 8) {
        jQuery(`.digging-tabs > div.tab:not(.${activeItemList.id})`).animate({
          maxWidth: '100px'
        }, 500);
      } else if (tabsCount > 6) {
        jQuery(`.digging-tabs > div.tab:not(.${activeItemList.id})`).animate({
          maxWidth: '120px'
        }, 500);
      } else if (tabsCount > 4) {
        jQuery(`.digging-tabs > div.tab:not(.${activeItemList.id})`).animate({
          maxWidth: '160px'
        }, 500);
      } else {
        jQuery(`.digging-tabs > div.tab:not(.${activeItemList.id})`).animate({
          maxWidth: '320px'
        }, 500);
      }

      jQuery(`.digging-tabs > div.${activeItemList.id}`).animate({
        maxWidth: '320px'
      }, 500);
    }
  }

  removeTab(id) {
    this.diggingBuffer[id];
    tabsStore.remove(id);
    const arr = Object.keys(tabsStore.tabs);
    const len = arr.length;
    const lastId = arr[len - 1];
    this.setActiveTab(lastId);
  }

  tabExists(id) {
    if (tabsStore.tabs[id]) {
      return true;
    }

    return false;
  }

  scrape(source, type, query, cb) {
    if (type === 'news') {
      let action = 'getNews';
    } else if (type === 'getRelease') {
      let action = 'getRelease';
    } else {
      let action = `search_${type}`;
    }

    const releases = [];

    switch (source) {
      case 'all':
        async.each(sourcesObjects, (_source, callabck_each) => {
          let searchFuncName1 = action;
          let searchFuncName2 = `${action}_1`;
          const functions = [];
          if (_source[searchFuncName1]) {
            functions.push(searchFuncName1);
          }
          if (_source[searchFuncName2]) {
            functions.push(searchFuncName2);
          }

          if (functions.length === 1) {
            _source[action](query, (_releases) => {
              _releases.forEach((r) => {
                releases.push(r);
              });
              callabck_each(null);
            });
          } else if (functions.length === 2) {
            let searchFuncName1 = functions[0];
            let searchFuncName2 = functions[1];

            _source[searchFuncName1](query, null, (pages) => {
              if (pages.length > 0) {
                async.each(pages, (page, eachPagesCallback) => {
                  _source[searchFuncName2](query, page, (release) => {
                    if (release) {
                      releases.push(release);
                    }
                    eachPagesCallback(null);
                  });
                }, () => {
                  callabck_each(null);
                });
              } else {
                callabck_each(null);
              }
            });
          } else {
            callabck_each(null);
          }
        }, () => {
          cb(null, releases);
        });
        break;
      default:

        const sourceObject = _.findWhere(sourcesObjects, { value: source });

        if (sourceObject) {
          let searchFuncName1 = action;
          let searchFuncName2 = `${action}_1`;
          const functions = [];
          if (sourceObject[searchFuncName1]) {
            functions.push(searchFuncName1);
          }
          if (sourceObject[searchFuncName2]) {
            functions.push(searchFuncName2);
          }

          if (functions.length === 1) {
            sourceObject[action](query, (_releases) => {
              _releases.forEach((r) => {
                releases.push(r);
              });
              cb(null, releases);
            });
          } else if (functions.length === 2) {
            let searchFuncName1 = functions[0];
            let searchFuncName2 = functions[1];

            sourceObject[searchFuncName1](query, null, (pages) => {
              if (pages.length > 0) {
                async.eachSeries(pages, (page, eachPagesCallback) => {
                  sourceObject[searchFuncName2](query, page, (release) => {
                    if (release) {
                      releases.push(release);
                    }
                    eachPagesCallback(null);
                  });
                }, () => {
                  cb(null, releases);
                });
              } else {
                cb(null, releases);
              }
            });
          } else {
            cb(null, `Error: action "${action}" not set in Source "${source}" not found.`);
          }
        } else {
          cb(null, `Error: Source "${source}" not found.`);
        }
        break;
    }
  }

  youtube(q, diggingYoutubeSearchCallback) {
    if (q && q.length && q.length > 0) {
      q = S(q).replaceAll('(', '').s;
      q = S(q).replaceAll(')', '').s;
      q = S(q).humanize('}', '').s;
      q = q.replace(/[^\w\s]/gi, '');

      const host = 'https://www.googleapis.com';
      const path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
      const url = host + path + q;
      diggingCurl(url, (err, res) => {
        if (res.html) {
          const allResults = [];

          const youtubeData = res.html;

          if (youtubeData && youtubeData.items && youtubeData.items.length > 0 && youtubeData.items[0]) {
            youtubeData.items.forEach((item) => {
              if (item.id.kind === 'youtube#video') {
                const theTitle = item.snippet.title;

                allResults.push({ title: theTitle, id: item.id.videoId });
              }
            });

            if (allResults.length > 0) {
              const options = {
                /* threshold: 0.4,*/
                keys: ['title', 'id']
              };

              const fuse = new Fuse(allResults, options);
              const fuseResult = fuse.search(q);


              if (fuseResult.length > 0) {
                const fuseUrlBestMatch = fuseResult[0];

                diggingYoutubeSearchCallback(fuseUrlBestMatch);
              } else {
                diggingYoutubeSearchCallback(null);
              }
            } else {
              diggingYoutubeSearchCallback(null);
            }
          } else {
            diggingYoutubeSearchCallback(null);
          }
        } else {
          diggingYoutubeSearchCallback(null);
        }
      });
    } else {
      diggingYoutubeSearchCallback(null);
    }
  }

  suggestions(artist, title, callback) {
    jQuery.ajax({
      type: 'POST',
      url: `${loopbackUrl}/api/caches/suggestions`,
      data: { artist, title },
      success(data) {
        callback(data.releases);
      },
    });
  }

  getSources(callback) {
    callback(sourcesObjects);
  }

  search(query, type, href) {
    const sources = user.getSources('search');
    const viewMode = user.getViewMode('search');
    const tabId = md5(query + type);

    if (href && false) {
      this.scrape('discogs', 'getRelease', null, (release) => {
        console.log(release);
      });
    } else {
      switch (type) {
        case 'artist':
          const title = `search for artist "${query}"`;
          const cols = {
            1: { title: 'Productions', viewMode },
            2: { title: 'Music he plays', viewMode },
            3: { title: 'Related music', viewMode },
            4: { title: 'labels', viewMode: 'link' },
            5: { title: 'Fans', viewMode: 'link' },
          };
          break;
        case 'label':
          const title = `search for label "${query}"`;
          const cols = {
            1: { title: 'Tracks from label', viewMode },
            2: { title: 'Artist from label', viewMode: 'link' },
            3: { title: 'Artists playing label', viewMode: 'link' },
          };
          break;
        default:
          const title = `search for "${query}"`;
          const cols = { 1: { title: 'Tracks', viewMode: 'track' } };
          break;
      }

      tabsStore.add(tabId, title, cols, 'search');

      tabsStore.tabs[tabId].scraping = true;

      // this.setActiveTab(tabId);

      async.each(sources, (sourceName, callbackEachNews) => {
        this.scrape(sourceName, type, query, (error, releases) => {
          if (releases && Array.isArray(releases) && releases.length > 0) {
            this.sendReleases(tabId, query, type, viewMode, releases);
          }
          callbackEachNews(null);
        });
      }, () => {
        if (tabsStore.tabs[tabId]) {
          tabsStore.tabs[tabId].scraping = false;
        }
      });
    }
  }

  candidates(query, callback) {
    diggingHelpers.defineSearchType(query, (candidates) => {
      callback(candidates);
    });
  }

  loadItems() {
    const itemsToPush = [];

    const activeItemList = _digging.getActiveItemList();


    if (activeItemList) {
      if (_digging.diggingBuffer && _digging.diggingBuffer[activeItemList.id] && _digging.diggingBuffer[activeItemList.id][activeItemList.col]) {
        while (_digging.diggingBuffer[activeItemList.id][activeItemList.col].length > 0) {
          const item = _diggingHelpers.diggingBuffer[activeItemList.id][activeItemList.col].shift();
          if (item.tabId === activeItemList.id && item.col === activeItemList.col) {
            itemsToPush.push(item);
          }
        }

        if (itemsToPush.length > 0) {
          // console.log('NEW ITEM IN TAB '+activeItemList.id+' in COL '+activeItemList.col);

          if (this.lists[activeItemList.id][activeItemList.col]) {
            const currentItems = this.lists[activeItemList.id][activeItemList.col].items;

            const newArray = [...currentItems, ...itemsToPush];


            // tabsStore.tabs[activeItemList.id].cols.cols[activeItemList.col].items.items = newArray;
            this.lists[activeItemList.id][activeItemList.col].items = newArray;
          }
        }
      }
    }
  }

  getNews() {
    const tabId = md5('news');

    if (tabsStore.tabs[tabId]) {
      this.setActiveTab(tabId);
    } else {
      const $this = this;


      const sources = [];

      for (const index in user.sources.news) {
        if (user.sources.news[index] === true) {
          sources.push(index);
        }
      }

      const viewMode = user.getViewMode('news');

      const cols = {
        1: { title: 'House', viewMode },
        2: { title: 'Techno', viewMode },
        3: { title: 'Minimal', viewMode },
        4: { title: 'Electro', viewMode },
        5: { title: 'Disco', viewMode },
        6: { title: 'Rock', viewMode },
        7: { title: 'HipHop', viewMode },
        8: { title: 'Other', viewMode },
      };


      tabsStore.add(tabId, 'News from record shops', cols, 'news');
      tabsStore.tabs[tabId].scraping = true;

      async.each(sources, (sourceName, callbackEachNews) => {
        const sourceObject = sourcesObjects[sourceName];

        if (sourceObject && sourceObject.getNews) {
          sourceObject.getNews('', (releases) => {
            if (releases && releases.length > 0) {
              this.sendReleases(tabId, null, 'news', viewMode, releases);
            }
            callbackEachNews(null);
          });
        } else {
          callbackEachNews(null);
        }
      }, () => {
        tabsStore.tabs[tabId].scraping = false;
      });
    }
  }


  sendReleases(tabId, query, type, viewMode, releases) {
    let userLikes = user.get('likes');


    const $this = this;


    if (!userLikes) {
      userLikes = {};
    }

    if (this.tabExists(tabId)) {
      const $this = this;


      if (query) {
        let matchTerm = '.*';
        const terms = query.split(' ');
        for (let i = 0; i < terms.length; i++) {
          let final_term = terms[i];
          final_term = final_term.replace(',', '');
          final_term = final_term.replace(' ', '');
          final_term = final_term.replace('.', '');
          final_term = final_term.replace(':', '');
          final_term = final_term.replace(';', '');
          final_term = final_term.replace('"', '');
          final_term = final_term.replace("'", '');
          matchTerm += `(?=.*${final_term}.*)`;
        }
        matchTerm += '.*';
        const reg = new RegExp(matchTerm, 'i');
      }


      if (type === 'suggestions') {
        if (releases && releases.length && releases.length > 0) {
          releases.forEach((release) => {
            release.tabId = tabId;
            release.type = 'suggestions';
            release.col = 1;

            if (release && release.tracks && release.tracks.length && release.tracks.length > 0) {
              release.tracks.forEach((track) => {
                track.tabId = tabId;
                track.type = 'suggestions';
                track.col = 1;

                if (_.where(userLikes, { old_id: track.id }).length > 0) {
                  track.liked = true;
                }

                if (viewMode === 'track') {
                  $this.sendItem(track);
                }
              });

              if (viewMode === 'release') {
                $this.sendItem(release);
              }
            }
          });
        }
      } else if (type === 'playlist') {
        for (var index in releases) {
          var track = releases[index];
          track.tabId = tabId;
          track.type = 'playlist';
          track.col = 1;

          if (_.where(userLikes, { old_id: track.id }).length > 0) {
            track.liked = true;
          }

          $this.sendItem(track);
        }
      } else if (type === 'likes') {
        for (var index in releases) {
          var track = releases[index];
          track.tabId = tabId;
          track.type = 'likes';
          track.col = 1;

          if (_.where(userLikes, { old_id: track.id }).length > 0) {
            track.liked = true;
          }

          $this.sendItem(track);
        }
      } else if (releases && releases.length && releases.length > 0) {
        async.each(releases, (release, eachRealeasesCallback) => {
          if (type === 'news') {
            if (release && release.tracks && release.tracks.length && release.tracks.length > 0) {
              release.tabId = tabId;
              release.type = 'news';

              async.each(release.tracks, (track, eachtrackcb) => {
                if (track) {
                  track.tabId = tabId;
                  track.type = 'news';

                  if (_.where(userLikes, { old_id: track.id }).length > 0) {
                    track.liked = true;
                  }


                  if (track.genre && track.genre.length > 0) {
                    if (track.genre.match(new RegExp('minimal', 'i'))) {
                      track.col = 3;
                    } else if (track.genre.match(new RegExp('tech', 'i'))) {
                      track.col = 2;
                    } else if (track.genre.match(new RegExp('house', 'i'))) {
                      track.col = 1;
                    } else if (track.genre.match(new RegExp('electro', 'i'))) {
                      track.col = 4;
                    } else if (track.genre.match(new RegExp('disco', 'i'))) {
                      track.col = 5;
                    } else if (track.genre.match(new RegExp('rock', 'i'))) {
                      track.col = 6;
                    } else if (track.genre.match(new RegExp('hiphop', 'i')) || track.genre.match(new RegExp('hop', 'i'))) {
                      track.col = 7;
                    } else {
                      track.col = 8;
                    }

                    if (viewMode === 'track') {
                      $this.sendItem(track);
                    }
                  }
                }

                eachtrackcb(null);
              }, () => {

              });


              if (viewMode === 'release') {
                if (release.genre) {
                  if (release.genre.match(new RegExp('minimal', 'i'))) {
                    release.col = 3;
                  } else if (release.genre.match(new RegExp('tech', 'i'))) {
                    release.col = 2;
                  } else if (release.genre.match(new RegExp('house', 'i'))) {
                    release.col = 1;
                  } else if (release.genre.match(new RegExp('electro', 'i'))) {
                    release.col = 4;
                  } else if (release.genre.match(new RegExp('disco', 'i'))) {
                    release.col = 5;
                  } else if (release.genre.match(new RegExp('rock', 'i'))) {
                    release.col = 6;
                  } else if (release.genre.match(new RegExp('hiphop', 'i')) || release.genre.match(new RegExp('hop', 'i'))) {
                    release.col = 7;
                  } else {
                    release.col = 8;
                  }
                } else {
                  release.col = 8;
                }

                $this.sendItem(release);
              }
            }
          } else if (viewMode === 'track') {
            if (release && release.tracks && release.tracks.length && release.tracks.length > 0) {
              release.tabId = tabId;
              release.type = type;

              release.tracks.forEach((track) => {
                if (track) {
                  track.tabId = tabId;
                  track.type = type;

                  if (_.where(userLikes, { old_id: track.id }).length > 0) {
                    track.liked = true;
                  }

                  switch (type) {
                    case 'artist':

                      // one of the tracks is from artist
                      let diggingTrackMatchArtist = false;
                      if (release.tracks && release.tracks.length > 0) {
                        release.tracks.forEach((t) => {
                          if (t && t.artist && t.artist.match(reg)) {
                            diggingTrackMatchArtist = true;
                          }
                        });
                      }

                      if (track.artist && track.artist.match(reg)) {
                        track.col = 1;
                        $this.sendItem(track);

                        if (track.label) {
                          $this.sendItem({
                            id: md5(`labellink4${track.label}`),
                            type: 'label',
                            viewMode: 'link',
                            col: 4,
                            tabId,
                            name: track.label,
                            logo: track.logo,
                            source: track.source,
                            url: track.url,
                            title: release.title
                          });
                        }
                      } else if (release.playedBy && release.playedBy.trim().toLowerCase() === query.toLowerCase()) {
                        track.col = 2;
                        $this.sendItem(track);
                      } else if (diggingTrackMatchArtist) {
                        track.col = 3;
                        $this.sendItem(track);

                        if (track.playedBy) {
                          $this.sendItem({
                            id: md5(`artistlink5${track.artist}`),
                            type: 'artist',
                            viewMode: 'link',
                            col: 5,
                            tabId,
                            name: track.playedBy,
                            logo: track.logo,
                            source: track.source,
                            url: track.url,
                            title: release.title
                          });
                        }
                      }
                      break;
                    case 'label':

                      if (track.title.match(reg)) {
                        track.label = query;
                      }

                      if (track.label && track.label.match(reg)) {
                        track.col = 1;
                        $this.sendItem(track);

                        if (track.artist) {
                          $this.sendItem({
                            id: md5(`artistlink2${track.artist}`),
                            type: 'artist',
                            viewMode: 'link',
                            col: 2,
                            tabId: tabId,
                            name: track.artist,
                            logo: track.logo,
                            source: track.source,
                            url: track.url,
                            title: release.title
                          });
                        }

                        if (track.playedBy) {
                          $this.sendItem({
                            id: md5(`artistlink3${track.playedBy}`),
                            type: 'artist',
                            viewMode: 'link',
                            col: 3,
                            tabId: tabId,
                            name: track.playedBy,
                            logo: track.logo,
                            source: track.source,
                            url: track.url,
                            title: track.title
                          });
                        }
                      }
                      break;
                    case 'all':
                      track.col = 1;
                      $this.sendItem(track);
                      break;
                  }
                }
              });
            }
          } else // viewMode = release
          if (release && release.tracks && release.tracks.length && release.tracks.length > 0) {
            release.tabId = tabId;
            release.type = type;

            let diggingTrackMatchArtist = false;
            if (release.tracks) {
              release.tracks.forEach((t) => {
                if (_.where(userLikes, { old_id: track.id }).length > 0) {
                  track.liked = true;
                }

                if (t.artist && t.artist.match(reg)) {
                  diggingTrackMatchArtist = true;
                }
              });
            }

            switch (type) {
              case 'artist':

                if (((release.artist && release.artist.match(reg)) || diggingTrackMatchArtist) && release.kind === 'release') {
                  release.col = 1;
                  $this.sendItem(release);
                } else if (release.playedBy && release.playedBy.trim().toLowerCase() === query.toLowerCase()) {
                  release.col = 2;
                  $this.sendItem(release);
                } else {
                  if (release.playedBy) {
                    $this.sendItem({
                      id: md5(release.playedBy + tabId + 5),
                      type: 'artist',
                      viewMode: 'link',
                      col: 5,
                      tabId,
                      name: release.playedBy,
                      logo: release.logo,
                      source: release.source,
                      url: release.url,
                      title: release.title
                    });
                  }

                  release.col = 3;
                  $this.sendItem(release);
                }

                if (release.label) {
                  $this.sendItem({
                    id: md5(release.label + tabId + 4),
                    type: 'label',
                    viewMode: 'link',
                    col: 4,
                    tabId,
                    name: release.label,
                    logo: release.logo,
                    source: release.source,
                    url: release.url,
                    title: release.title
                  });
                }

                break;
              case 'label':

                if (release.title.match(reg)) {
                  release.label = query;
                }

                if (release.label && release.label.match(reg)) {
                  release.col = 1;

                  $this.sendItem(release);

                  if (release.artist) {
                    $this.sendItem({
                      id: md5(`artistlink2${release.artist}`),
                      type: 'artist',
                      viewMode: 'link',
                      col: 2,
                      tabId: tabId,
                      name: release.artist,
                      logo: release.logo,
                      source: release.source,
                      url: release.url,
                      title: release.title
                    });
                  }

                  if (release.playedBy) {
                    $this.sendItem({
                      id: md5(`artistlink3${release.playedBy}`),
                      type: 'artist',
                      viewMode: 'link',
                      col: 3,
                      tabId: tabId,
                      name: release.playedBy,
                      logo: release.logo,
                      source: release.source,
                      url: release.url,
                      title: release.title
                    });
                  }
                }
                break;
              case 'all':
                release.col = 1;
                $this.sendItem(release);
                break;
            }
          }
          eachRealeasesCallback(null);
        }, () => {

        });
      }
    }
  }


}

const _digging = new digging2();

setInterval(() => {
  _digging.loadItems();
}, 1000);

export default _digging;
