import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-dnd';
import HTML5Backend, { NativeTypes } from 'react-dnd-html5-backend';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Drawer from 'material-ui/Drawer';
import async from 'async';
import S from 'string';
import {HotKeys} from 'react-hotkeys';
import DigginUser from '../models/DiggingUser';
import cheerio from 'cheerio';
import { MenuList } from 'material-ui/Menu';
import Paper from 'material-ui/Paper';
import classNames from 'classnames';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import Digging from '../components/Digging';
import Likes from '../components/Likes';
import DiggingBlog from '../components/DiggingBlog';
import DiggingDebug from '../components/DiggingDebug';
import DiggingTimeline from '../components/DiggingTimeline';
import Playlists from '../components/Playlists';
import DiggingSettings from '../components/DiggingSettings';
import { Link, Switch, Route } from 'react-router-dom';
import DiggingModel from '../models/DiggingModel';
import diggingCurl from '../digging/diggingCurl';
import DiggingSearchBox from '../components/DiggingSearchBox';
import DiggingPlayer from '../components/DiggingPlayer';
import DiggingLikesLink from '../components/DiggingLikesLink';
import Playlist from '../components/Playlist';
import DiggingSuggestor from '../components/DiggingSuggestor';
import {Icon} from 'react-fa';
import DiggingCssModel from '../models/DiggingCssModel';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    zIndex: 1,
    overflow: 'hidden'
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
    color: '#ffffff'
  },
  hidden: {
    display: 'none'
  },
  appBar: {
    position: 'absolute',
    backgroundColor: '#7E7E7E',
    zIndex: theme.zIndex.navDrawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 0,
    marginRight: 36
  },
  menuItem: {
    height: 20,
    margin: 0,
    color: '#ffffff',
    '&:focus': {
      background: theme.palette.primary[500],
      '& $text, & $icon': {
        color: theme.palette.common.white
      }
    }
  },
  text: {
    color: '#ffffff'
  },
  textWhite: {
    color: '#ffffff'
  },
  icon: {
    color: '#ffffff'
  },
  menu: {
    width: 240,
    backgroundColor: '#222222',
    color: '#ffffff'
  },
  drawer: {

  },
  icon2: {
    color: '#ffffff',
    height: 18,
    marginRight: 0,
    marginBottom: -2
  },
  flex: {
    flex: 1
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300
  },
  hide: {
    display: 'none'
  },
  drawerPaper: {
    backgroundColor: '#222222',
    color: '#ffffff',
    position: 'relative',
    height: '100%',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    backgroundColor: '#222222',
    color: '#ffffff',
    width: 60,
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  drawerInner: {
    backgroundColor: '#222222',
    color: '#ffffff',
    // Make the items inside not wrap when transitioning:
    width: drawerWidth
  },
  drawerHeader: {
    backgroundColor: '#222222',
    color: '#ffffff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  settingsLink: {
    color: '#ffffff',
    fontSize: 12,
    margin: 0
  },
  drawerHeaderClosed: {
    backgroundColor: '#222222',
    color: '#ffffff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default
  }
});

@DragDropContext(HTML5Backend)
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      searchDialogueOpen: false,
      query: '',
      autoComplete: []
    };
    this.getAutoComplete = this.getAutoComplete.bind(this);
  }

  getAutoComplete() {

    this.state.query = S(this.state.query).replaceAll('(', '').s;
    this.state.query = S(this.state.query).replaceAll(')', '').s;
    this.state.query = S(this.state.query).humanize().s;
    this.state.query = this.state.query.replace(/[^\w\s]/gi, '');
    this.state.query = this.state.query.toLowerCase();

    const discogs_results = [];

    async.waterfall([
      (parallel_cb) => {
        const url = "https://www.discogs.com/search/?q=" + encodeURI(this.state.query) + "&type=all";
        diggingCurl(url, function (error, result) {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let count_artists = 0;
            let count_label = 0;
            let count_release = 0;
            $('#search_results .card h4 a.search_result_title').each(function (k, v) {

              const elt = $(v).first();
              let text = $(elt).text().trim().toLowerCase();
              text.replace(new RegExp('\(.*\)', 'i'), '');
              const href = $(elt).attr('href');
              const img = $(elt).parent().prev().find('img').attr('data-src');

              if (href.match('/artist/') && count_artists < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'artist'
                });
                count_artists = count_artists + 1;
              }
              else if (href.match('/label/') && count_label < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'label'
                });
                count_label = count_label + 1;
              }
              else if (href.match('/release/') && count_release < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'release'
                });
                count_release = count_release + 1;
              }

            });
            parallel_cb(null);
          }
        });
      },
      (parallel_cb) => {
        const url = "https://www.residentadvisor.net/search.aspx?searchstr=" + encodeURI(this.state.query) + "&section=events&titles=1";
        diggingCurl(url, function (error, result) {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let count_events = 0;
            $('main ul.content-list.search li section.content div.generic.events div ul.list li a').each(function (k, v) {

              const elt = $(v).first();
              let text = $(elt).text().trim().toLowerCase();
              const href = $(elt).attr('href');
              const img = null;

              if (count_events < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'event'
                });
                count_events = count_events + 1;
              }
            });
          }
          parallel_cb(null);
        });
      }
    ], () => {
      this.setState({autoComplete: discogs_results, searchDialogueOpen: true});
    });
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleChangeQuery = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleRequestClose = value => {
    this.setState({ searchDialogueOpen: false });
  };

  render() {
    const { classes } = this.props;
    let sideBarLink = <IconButton
      color="contrast"
      aria-label="open drawer"
      onClick={this.handleDrawerOpen}
      className={classes.menuButton}
    ><MenuIcon /></IconButton>;
    let sideBarLinkClass = classes.drawerHeaderClosed;
    if(this.state.open === true){
      sideBarLinkClass = classes.drawerHeader;
      sideBarLink = <IconButton className={classes.text} onClick={this.handleDrawerClose}>
        <ChevronLeftIcon />
      </IconButton>
    }

    let avatar = null;
    if(DigginUser.picture && DigginUser.picture.length > 0 && this.state.open){
      avatar = <img src={`${DigginUser.picture}`} style={{position: 'fixed', top: 10, left: 10, borderRadius: 50, cursor: 'pointer', width: 30,  display: 'block'}} />
    } else {
      avatar = null;
    }

    let username = null;
    if(this.state.open){
      username = <div className="username">{DigginUser.username}</div>;
    }

    const logo = <img src='/public/img/digging_logo.png' style={{cursor: 'pointer', width: 80, marginLeft: 'auto', marginRight: 'auto', display: 'block'}} />;

    const activeTab = DiggingModel.getActiveTab();
    let url = '/';
    if(activeTab) {
      let query = activeTab.query;
      if(!activeTab.query) {
        query = 'news';
      }
      url = `/${query}/${activeTab.type}/${activeTab.sources.join(',')}/${activeTab.viewMode}`;
    }

    return (
      <HotKeys keyMap={{

      }} handlers={{

      }}>
        <div className={classes.root}>
          <div className={classes.appFrame}>
            <Drawer
              type="permanent"
              style={{
                height: '100%',
                overflowY: scroll
              }}
              classes={{
                paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose)
              }}
              open={this.state.open}
            >
              <div className={classes.drawerInner} style={{height: DiggingCssModel.windowHeight}}>
                <div className={sideBarLinkClass}>
                  {sideBarLink}
                </div>
                <Paper className={classes.menu}>
                  {avatar}
                  {username}
                  <Link to="/">
                    {logo}
                  </Link>
                  <MenuList>
                    {this.state.open?<DiggingSearchBox />:null}
                    <ul className="sideLinks">
                      <li><Link to={url}><Icon name='home' /> Home</Link></li>
                      <li><DiggingLikesLink /></li>
                      <li><Link to="/blog"><Icon name='rss ' /> Blog</Link></li>
                      <li><Link to="/debug"><Icon name='rss ' /> Debug</Link></li>

                      <li><Link to="/news/news"><Icon name='newspaper-o ' /> News</Link></li>

                      <li><Link to="/suggest"><Icon name='lightbulb-o' /> Suggestions</Link></li>
                      <li><Link to="/settings"><Icon name='cog' /> Settings</Link></li>
                      <li><span onClick={()=>{
                        DigginUser.logout();
                      }}><Icon name='sign-out' /> Logout</span></li>
                    </ul>
                    <Playlists/>
                  </MenuList>
                </Paper>
              </div>
            </Drawer>
            <main className={classes.content} style={{
              height: DiggingCssModel.windowHeight,
              overflowY: 'scroll'
            }}>
              <Switch>
                <Route exact path='/' component={Digging}/>
                <Route exact path='/blog' component={DiggingBlog}/>
                <Route exact path='/debug' component={DiggingDebug}/>
                <Route exact path='/timeline' component={DiggingTimeline}/>
                <Route exact path='/settings' component={DiggingSettings}/>
                <Route exact path='/suggest' component={DiggingSuggestor}/>
                <Route exact path='/likes' component={Likes}/>
                <Route exact path='/playlist/:id' component={Playlist}/>
                <Route path='/:query/:type/:sources/:viewMode' component={Digging}/>
                <Route path='/:query/:type/:sources' component={Digging}/>
                <Route path='/:query/:type' component={Digging}/>
                <Route path='/:query' component={Digging}/>
              </Switch>
            </main>
          </div>
          <DiggingPlayer />
        </div>
      </HotKeys>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Header);
