import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import { LinearProgress } from 'material-ui/Progress';
import ReactPlayer from 'react-player';
import DiggingPlayerModel from '../models/DiggingPlayerModel';
import DiggingTrackActions from '../components/DiggingTrackActions';
import DiggingPlayerCandidates from '../components/DiggingPlayerCandidates';
import jQuery from 'jquery';
import {Icon} from 'react-fa';

const styles = theme => ({

});

@observer
class DiggingPlayer extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){

  }
  render(){
    const { classes } = this.props;
    const actions = (DiggingPlayerModel.track && DiggingPlayerModel.track.title)?<DiggingTrackActions suggestor={true} track={DiggingPlayerModel.track} />:null;
    const PlayPause = (DiggingPlayerModel.playing)?<Icon name='pause-circle' onClick={()=>{DiggingPlayerModel.pause();}} />:<Icon name='play-circle' onClick={()=>{DiggingPlayerModel.play(null);}} />;
    const cover = (DiggingPlayerModel.playingTrack && DiggingPlayerModel.playingTrack.cover)?<img src={DiggingPlayerModel.playingTrack.cover} />:null;
    const url = (DiggingPlayerModel.playingTrack && DiggingPlayerModel.playingTrack.url)?DiggingPlayerModel.playingTrack.url:null;

    let wrapperClass = 'DiggingPlayerWrapper';
    if(DiggingPlayerModel.track && DiggingPlayerModel.track.title){
      wrapperClass = 'DiggingPlayerWrapper playing'
    }

    return (
      <div className={wrapperClass}>
        <div className="ReactPlayerWrapper">
          <ReactPlayer
            ref={(player) => {
              return this.player = player;
            }}
            className='react-player'
            style={{
              width: 241,
              height: 43,
              position: 'fixed',
              zIndex: 9999999999999999999999999,
              left: 0,
              bottom: 0,
              visibility: 'hidden'
            }}
            width={241}
            height={43}
            url={url}
            playing={DiggingPlayerModel.playing}
            playbackRate={1}
            progressFrequency={500}
            volume={DiggingPlayerModel.volume}
            config={{
              youtube: {
                playerVars: { showinfo: 1 }
              }
            }}
            onReady={function () {

            }}
            onStart={function () {

            }}
            onPlay={function () {
              DiggingPlayerModel.playing = true;
            }}
            onPause={function () {
              DiggingPlayerModel.playing = false;
            }}
            onBuffer={function () {

            }}
            onEnded={() => {
              DiggingPlayerModel.next()
            }}
            onError={() => {
              DiggingPlayerModel.next()
            }}
            onProgress={(arg)=>{
              DiggingPlayerModel.progress = arg.played*100;
              DiggingPlayerModel.buffer = arg.loaded*100;
            }}
            onDuration={function (duration) {
              DiggingPlayerModel.duration = duration;
            }}
          />
        </div>
        <div className="DiggingCandidates">
          <DiggingPlayerCandidates />
        </div>
        <div className="DiggingInfoGroup">
          <div className="DiggingplayerCover">
            {cover}
          </div>
          <div className="DiggingplayerButtons">
            <div className="DiggingPlayerActions">
              {actions}
            </div>
            <div className="DiggingPlayerControls">
              {PlayPause}
              <Icon name='stop-circle' onClick={()=>{DiggingPlayerModel.stop()}} />
              <Icon name='fast-forward' onClick={()=>{DiggingPlayerModel.next()}} />
            </div>
          </div>

        </div>

        <div className="DiggingPlayerProgressBar">
          <div style={{
            height: '0px',
            paddingTop: '3px'
          }}>
            <LinearProgress
              onClick={(e)=>{
                const parentOffset = jQuery(e.target).parent().offset();
                const width = jQuery(e.target).width();
                const relX = e.pageX - parentOffset.left;
                const progress = ((relX*100)/width)/100;
                this.player.seekTo(parseFloat(progress));
              }}
              mode="buffer"
              className={classes.progressbar}
              value={DiggingPlayerModel.progress}
              valueBuffer={DiggingPlayerModel.buffer}
              classes={{
                'dashed': classes.dashed,
                'determinateBar1': classes.determinateBar1,
                'primaryColorBar': classes.primaryColorBar,
                'bufferBar2': classes.bufferBar2
              }}
            />
            <span style={{
              textAlign: 'center' ,
              position: 'absolute',
              bottom: 10,
              color: '#ffffff',
              left: '50%',
              zIndex: 99999999
            }}>

              </span>
          </div>
        </div>
      </div>
    );


  }
}

export default withStyles(styles)(DiggingPlayer);

