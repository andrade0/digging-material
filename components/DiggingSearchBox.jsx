import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import async from 'async';
import S from 'string';
import DigginUser from '../models/DiggingUser';
import cheerio from 'cheerio';
import { GridList, GridListTile, GridListTileBar } from 'material-ui/GridList';
import SearchIcon from 'material-ui-icons/Search';
import TextField from 'material-ui/TextField';
import Dialog, { DialogTitle } from 'material-ui/Dialog';
import diggingModel from '../models/DiggingModel';
import diggingCurl from '../digging/diggingCurl';
import sourcesObjects from '../digging/sources';
import CloseIcon from 'material-ui-icons/Close';
import {Icon} from 'react-fa';
import { withRouter } from 'react-router-dom';

const drawerWidth = 240;

const styles = theme => ({
  hidden: {
    display: 'none'
  },
  icon: {},
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: 0,
    marginBottom: 0,
    width: 220,
    color: '#ffffff'
  },
  hide: {
    display: 'none'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    background: theme.palette.background.paper
  },
  gridList: {
    width: 800,
    height: 450,
    padding: 10
  },
  label: {
    color: '#ffffff',
    fontSize: 12
  }
});

class SimpleDialog extends React.Component {

  constructor(props) {
    super(props);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  handleRequestClose = () => {
    this.props.closeModal();
  };

  render() {
    const { classes, ...other } = this.props;
    return (
      <Dialog onRequestClose={this.handleRequestClose} {...other}>
        <DialogTitle>Suggestins for: "{this.props.query}" <CloseIcon onClick={()=>{this.handleRequestClose()}} /></DialogTitle>
        <div>
          <div className={classes.container}>
            <GridList cols={3} cellHeight={180} className={classes.gridList}>
              <GridListTile key={Math.random()}>
                <img src={'/public/img/default-artist.png'} alt={this.props.query} />
                <GridListTileBar
                  title={this.props.query}
                  subtitle={<span>{this.props.type}</span>}
                  actionIcon={
                    <IconButton onClick={()=>{this.props.search(this.props.type, this.props.query)}}>
                      <SearchIcon color="rgba(255, 255, 255, 0.54)" />
                    </IconButton>
                  }
                />
              </GridListTile>
              {this.props.autoComplete.map(item => {
                let img = item.img;
                if(!item.img || (item.img && item.img.trim().length === 0)) {
                  img = '/public/img/default-artist.png';
                }
                return (<GridListTile key={Math.random()}>
                  <img src={img} alt={item.title} />
                  <GridListTileBar
                    title={item.title}
                    subtitle={<span>{S(item.type).capitalize().s}</span>}
                    actionIcon={
                      <IconButton onClick={()=>{this.props.search(item.type, item.title)}}>
                        <SearchIcon color="rgba(255, 255, 255, 0.54)" />
                      </IconButton>
                    }
                  />
                </GridListTile>);
              })}
            </GridList>
          </div>
        </div>
      </Dialog>
    );
  }
}


const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);

class DiggingSearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchDialogueOpen: false,
      query: '',
      type: '',
      autoComplete: []
    };
    this.getAutoComplete = this.getAutoComplete.bind(this);
    this.search = this.search.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  search(type, query){

    const sourceType = ( type==='news' )?'news':'search';

    let viewMode = 'track';
    if(sourceType === 'news') {
      if(DigginUser.newsPreferReleaseViewMode) {
        viewMode = 'release';
      } else {
        viewMode = 'track';
      }
    } else {
      if(DigginUser.searchPreferReleaseViewMode) {
        viewMode = 'release';
      } else {
        viewMode = 'track';
      }
    }

    let sources = DigginUser.sources.reduce((_sources, sourceItem) => {
      if(sourceItem.type === sourceType && sourceItem.value === true)
      {
        const sourceObject = sourcesObjects[sourceItem.name];
        if(sourceObject[type]) {
          _sources.push(sourceItem.name);
        }
      }
      return _sources;
    }, []);

    const title = S(sourceType).capitalize().s+' '+S(type).capitalize().s+' '+S(query).capitalize().s;

    if(!diggingModel.tabExists(title)) {
      diggingModel.addTab(title, type, query, viewMode, sources);
      this.props.history.push('/');
      //diggingModel.setActive(title);
    } else {
      this.props.history.push('/');
      //diggingModel.setActive(title);
    }

    this.setState({ searchDialogueOpen: false });
  }

  getAutoComplete() {

    diggingModel.loading = true;

    this.state.query = S(this.state.query).replaceAll('(', '').s;
    this.state.query = S(this.state.query).replaceAll(')', '').s;
    this.state.query = S(this.state.query).humanize().s;
    this.state.query = this.state.query.replace(/[^\w\s]/gi, '');
    this.state.query = this.state.query.toLowerCase();

    const discogs_results = [];

    async.waterfall([
      (parallel_cb) => {
        const url = "https://www.discogs.com/search/?q=" + encodeURI(this.state.query) + "&type=all";
        diggingCurl(url, function (error, result) {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let count_artists = 0;
            let count_label = 0;
            let count_release = 0;
            $('#search_results .card h4 a.search_result_title').each(function (k, v) {

              const elt = $(v).first();
              let text = $(elt).text().trim().toLowerCase();
              text.replace(new RegExp('\(.*\)', 'i'), '');
              const href = $(elt).attr('href');
              const img = $(elt).parent().prev().find('img').attr('data-src');

              if (href.match('/artist/') && count_artists < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'artist'
                });
                count_artists = count_artists + 1;
              }
              else if (href.match('/label/') && count_label < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'label'
                });
                count_label = count_label + 1;
              }
              else if (href.match('/release/') && count_release < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'release'
                });
                count_release = count_release + 1;
              }

            });
            parallel_cb(null);
          }
        });
      },
      (parallel_cb) => {
        const url = "https://www.residentadvisor.net/search.aspx?searchstr=" + encodeURI(this.state.query) + "&section=events&titles=1";
        diggingCurl(url, function (error, result) {
          if (result && result.html) {
            const $ = cheerio.load(result.html);
            let count_events = 0;
            $('main ul.content-list.search li section.content div.generic.events div ul.list li a').each(function (k, v) {

              const elt = $(v).first();
              let text = $(elt).text().trim().toLowerCase();
              const href = $(elt).attr('href');
              const img = null;

              if (count_events < 3) {
                discogs_results.push({
                  title: text,
                  href: 'https://www.discogs.com' + href,
                  img: img,
                  type: 'event'
                });
                count_events = count_events + 1;
              }
            });
          }
          parallel_cb(null);
        });
      }
    ], () => {
      diggingModel.loading = false;
      let defined_type = "all";
      if(discogs_results[0]){
        defined_type = discogs_results[0].type;
      }
      this.setState({autoComplete: discogs_results, type: defined_type, searchDialogueOpen: true});
    });
  }

  handleChangeQuery = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleRequestClose = value => {
    this.setState({ searchDialogueOpen: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <form onSubmit={(e)=>{e.preventDefault(); this.getAutoComplete();}}>
          <Icon className="searchIcon" name='search' />
          <TextField
            InputProps={{className: 'searchinput'}}
            label="Real time music search engine"
            placeholder="Artist, label, club ..."
            className={classes.textField}
            labelClassName={classes.label}
            value={this.state.query}
            onChange={this.handleChangeQuery('query')}
            margin="normal"
          />
          <button className={classes.hidden}></button>
        </form>
        <SimpleDialogWrapped
          search={this.search}
          query={this.state.query}
          type={this.state.type}
          autoComplete={this.state.autoComplete}
          open={this.state.searchDialogueOpen}
          closeModal={this.handleRequestClose}
        />
      </div>
    );
  }
}

DiggingSearchBox.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles, { withTheme: true })(DiggingSearchBox));

