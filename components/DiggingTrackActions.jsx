import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, componentWillReact } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import DiggingModel from '../models/DiggingModel';
import { DragSource } from 'react-dnd';
import {Icon} from 'react-fa';
import DiggingListenedModel from '../models/DiggingListenedModel';
import DiggingLikesModel from '../models/DiggingLikesModel';
import { withRouter } from 'react-router-dom';

const styles = theme => ({

});

@DragSource(props => props.track.type, {
  beginDrag(props) {
    return {
      track: props.track
    }
  }
}, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))

@observer
class DiggingTrackActions extends Component {



  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.track.ref = this.ref;
  }

  render(){

    const { classes, track, isDropped, isDragging, connectDragSource } = this.props;

    const opacity = (DiggingModel.droppedBoxNames.indexOf(this.props.track.name) > -1) ? 0.4 : 1;

    let likeButton = <Icon name='heart-o' style={{color: '#222222', cursor: 'pointer'}} onClick={(event)=>{this.props.track.like(this.props.track);}} />;
    if(this.props.track.liked) {
      likeButton = <Icon name='heart' style={{color: '#222222', cursor: 'pointer'}} onClick={(event)=>{this.props.track.dislike(this.props.track);}} />;
    }

    let playButton = <Icon name='play-circle' style={{color: '#222222', cursor: 'pointer'}} onClick={(event)=>{
      this.props.track.play();
    }} />
    if(this.props.track.playing) {
      playButton = <Icon name='pause-circle' style={{color: '#222222', cursor: 'pointer'}} onClick={(event)=>{this.props.track.pause();}} />;
    }

    if(this.props.suggestor){
      playButton = null;
    }

    let IpsfButton = null;
    if(this.props.track.ipfs){
      IpsfButton = <Icon name='download' style={{color: '#222222', cursor: 'pointer'}} onClick={(event)=>{
        const fileUrl = 'https://ipfs.io/ipfs/'+this.props.track.ipfs;
        window.open(fileUrl);
      }} />
    }

    if(this.props.track && this.props.track.colType && this.props.track.colType === 'news'){
      this.props.track.listened = DiggingListenedModel.tracks.reduce((_listened, t) => {
        if(t.artist === track.artist && t.title === track.title){
          _listened = true;
        }
        return _listened;
      }, false);
    }

    let bgColor = 'rgba(245, 245, 245, 0.19)';
    if(isDragging || this.props.track.playing) {
      bgColor = '#e2e2e2';
    } else if (this.props.track.listened) {
      bgColor = '#f4f3ff';
    }

    return connectDragSource(<span ref={(ref) => { this.ref = ref; }} className="trackActionsWrapper" style={{ ...{
      backgroundColor: bgColor
    }, opacity }}>
      {playButton}
      {likeButton}
      {IpsfButton}
      <CopyToClipboard text={this.props.track.artist+' '+this.props.track.title} onCopy={() => {}}>
        <span style={{color: '#1f6b71', cursor: 'pointer', display: 'inline-block', marginLeft: 5}}><span style={{cursor: 'pointer', color: '#3f51b5'}} onClick={()=>{
          DiggingModel.addTab('Search for artist "'+this.props.track.artist+'"', 'artist', this.props.track.artist, null, null);
          this.props.history.push('/');
        }}>{this.props.track.artist}</span> - <span style={{cursor: 'pointer'}} onClick={()=>{
          this.props.track.play();
        }}>{this.props.track.title}</span></span>
      </CopyToClipboard>
    </span>);

  }
}

export default withRouter(withStyles(styles)(DiggingTrackActions));


