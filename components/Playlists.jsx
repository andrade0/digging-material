import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import PlaylistLink from '../components/PlaylistLink';
import DiggingPlaylistsModel from '../models/DiggingPlaylistsModel';
import DiggingPlaylistEditorModel from '../models/DiggingPlaylistEditorModel';
import {Icon} from 'react-fa';
import Dialog, { DialogTitle } from 'material-ui/Dialog';
import CloseIcon from 'material-ui-icons/Close';
import fire from '../models/fire';
import firebase from 'firebase';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Radio from 'material-ui/Radio';


const styles = theme => ({
  text: {
    color: '#ffffff',
    fontSize: 10
  },
  diggingSidebarBackgroundColor: {
    backgroundColor: '#222222'
  },
  nested: {
    backgroundColor: '#222222',
    padding: 0,
    paddingLeft: 16
  },
  icon2: {
    color: '#ffffff',
    height: 18,
    marginRight: 0,
    marginBottom: -2
  },
  ListItem: {
    height: 12,
    margin: 0
  },
  textField: {
    color: '#000000'
  },
  hidden: {
    display: 'none'
  }
});

@observer
class SimpleDialog extends React.Component {

  constructor(props) {
    super(props);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.changeNewPlaylistName = this.changeNewPlaylistName.bind(this);
    this.handleChangeRadios = this.handleChangeRadios.bind(this);
  }

  changeNewPlaylistName(e) {
    if(DiggingPlaylistEditorModel && DiggingPlaylistEditorModel.playlist) {
      DiggingPlaylistEditorModel.playlist.title = e.target.value;
    } else {
      DiggingPlaylistEditorModel.title = e.target.value;
    }
  }

  handleRequestClose = () => {
    DiggingPlaylistEditorModel.open = false;
  };

  handleChangeRadios(e){

    if(DiggingPlaylistEditorModel && DiggingPlaylistEditorModel.playlist) {
      DiggingPlaylistEditorModel.playlist.visibility = e.target.value;
    } else {
      DiggingPlaylistEditorModel.visibility = e.target.value;
    }
  }

  render() {

    let dialogueTitle = 'Add new playlist';
    if(DiggingPlaylistEditorModel.playlist) {
      dialogueTitle = 'Rename playlist "'+DiggingPlaylistEditorModel.title+'"';
    }

    let visibility = DiggingPlaylistEditorModel.visibility;
    if(DiggingPlaylistEditorModel.playlist) {
      visibility = DiggingPlaylistEditorModel.playlist.visibility;
    }

    const { classes, ...other } = this.props;

    let textFieldValue = DiggingPlaylistEditorModel.title;

    let placeholder = "New playlist name";

    if(DiggingPlaylistEditorModel.playlist){
      textFieldValue = DiggingPlaylistEditorModel.playlist.title;
      placeholder = "Rename playlist "+DiggingPlaylistEditorModel.playlist.title;
    }

    return (
      <Dialog onRequestClose={this.handleRequestClose} {...other}>
        <DialogTitle>{dialogueTitle}</DialogTitle>
        <div style={{
          paddingRight: 30,
          paddingLeft: 30,
          paddingBottom: 50
        }}>
          <form onSubmit={(e)=>{
            e.preventDefault();

            if(DiggingPlaylistEditorModel && DiggingPlaylistEditorModel.playlist) {
              DiggingPlaylistEditorModel.playlist.rename(DiggingPlaylistEditorModel.playlist.title);
              DiggingPlaylistEditorModel.playlist = null;
            } else {
              DiggingPlaylistsModel.addPlaylist({title: DiggingPlaylistEditorModel.title});
            }
            DiggingPlaylistEditorModel.title = '';
            this.handleRequestClose();
          }}>
            <TextField
            onChange={this.changeNewPlaylistName}
            label={placeholder}
            className={classes.textField}
            value={textFieldValue}
            margin="normal"
          />
            <p>
              <Radio
                checked={visibility === 'public'}
                onChange={this.handleChangeRadios}
                value="public"
                name="Public"
                aria-label="A"
              />Public
              <Radio
                checked={visibility === 'private'}
                onChange={this.handleChangeRadios}
                value="private"
                name="Private"
                aria-label="B"
              />Private
            </p>

            <button style={{display: 'none'}}></button>
          </form>
        </div>
      </Dialog>
    );
  }
}

const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);


@observer
class Playlists extends Component {
  constructor(props) {
    super(props);
    this.openModal = this.openModal.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  handleRequestClose = value => {
    DiggingPlaylistEditorModel.open = false;
  };

  openModal = value => {
    DiggingPlaylistEditorModel.open = true;
  };

  render(){
    const { classes } = this.props;
    return (
      <div>
        <SimpleDialogWrapped
          open={DiggingPlaylistEditorModel.open}
          closeModal={this.handleRequestClose}
        />
        <div className="playlistSection">PLAYLISTS <Icon
          name='plus'
          onClick={this.openModal}
          style={{color: '#ffffff', cursor: 'pointer', marginLeft: 5}}
        /></div>
        <ul className="playlistsList">
          {DiggingPlaylistsModel.playlists.map((playlist, index)=>{
            return <PlaylistLink
              onDrop={item => {
                playlist.handleDrop(index, item)}
              }
              accepts={playlist.accepts}
              lastDroppedItem={playlist.lastDroppedItem}
              key={playlist.id}
              playlist={playlist} />
          })}
        </ul>
      </div>
    );

  }
}

export default withStyles(styles)(Playlists);
