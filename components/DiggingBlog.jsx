import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import DiggingBlogDataModel from '../models/DiggingBlogDataModel';
import DiggingCssModel from '../models/DiggingCssModel';


const styles = theme => ({

});

@observer
class DiggingBlog extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    const {classes} = this.props;
    return <div className="diggingBlog">
      <div className="diggingBlogWrapper">
        <div className="blogSectionwrapper">
          <div className="blogSectionTitle">Reviews</div>
          <div className="blogSectionContent">
            {DiggingBlogDataModel.reviews.map((content)=>{
              return <div key={content.title} style={{backgroundImage: 'url('+content.img+')'}} className="diggingBlogContentWrapper">
                <div className="diggingBlogContentContent">
                  <h1>{content.title}</h1>
                  <a className="gotoBlog" target="_blank" title={content.source} href={content.url}><img className="blogLogo" src={content.logo} /></a>
                  <span className="blogType">{content.type}</span>
                </div>
              </div>
            })}
          </div>
        </div>
      </div>
      <div className="diggingBlogWrapper">
        <div className="blogSectionwrapper">
          <div className="blogSectionTitle">Artists</div>
          <div className="blogSectionContent">
            {DiggingBlogDataModel.artists.map((content)=>{
              return <div key={content.title} style={{backgroundImage: 'url('+content.img+')'}} className="diggingBlogContentWrapper">
                <div className="diggingBlogContentContent">
                  <h1>{content.title}</h1>
                  <a className="gotoBlog" target="_blank" title={content.source} href={content.url}><img className="blogLogo" src={content.logo} /></a>
                  <span className="blogType">{content.type}</span>
                </div>
              </div>
            })}
          </div>
        </div>
      </div>
      <div className="diggingBlogWrapper">
        <div className="blogSectionwrapper">
          <div className="blogSectionTitle">Events</div>
          <div className="blogSectionContent">
            {DiggingBlogDataModel.events.map((content)=>{
              return <div key={content.title} style={{backgroundImage: 'url('+content.img+')'}} className="diggingBlogContentWrapper">
                <div className="diggingBlogContentContent">
                  <h1>{content.title}</h1>
                  <a className="gotoBlog" target="_blank" title={content.source} href={content.url}><img className="blogLogo" src={content.logo} /></a>
                  <span className="blogType">{content.type}</span>
                </div>
              </div>
            })}
          </div>
        </div>
      </div>
      <div className="diggingBlogWrapper">
        <div className="blogSectionwrapper">
          <div className="blogSectionTitle">News</div>
          <div className="blogSectionContent">
            {DiggingBlogDataModel.news.map((content)=>{
              return <div key={content.title} style={{backgroundImage: 'url('+content.img+')'}} className="diggingBlogContentWrapper">
                <div className="diggingBlogContentContent">
                  <h1>{content.title}</h1>
                  <a className="gotoBlog" target="_blank" title={content.source} href={content.url}><img className="blogLogo" src={content.logo} /></a>
                  <span className="blogType">{content.type}</span>
                </div>
              </div>
            })}
          </div>
        </div>
      </div>
    </div>
  }
}

export default withStyles(styles)(DiggingBlog);
