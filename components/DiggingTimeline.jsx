import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import fire from '../models/fire';
import firebase from 'firebase';


const styles = theme => ({

});

@observer
class DiggingTimeline extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('timeline');
    this.unsubscribe = null;
    this.onCollectionUpdate = this.onCollectionUpdate.bind(this);
    this.state = {
      items: []
    };
  }
  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  onCollectionUpdate(querySnapshot){
    const items = [];
    querySnapshot.forEach((doc) => {
      const { url, html } = doc.data();
      items.push(doc.data())
    });
    this.setState({
      items: items
    });
  }
  render() {
    const {classes} = this.props;
    return <div>{this.state.items.map((item)=>{
      return item.obj.tracks.getway+item.obj.tracks.urlPath
    })}</div>
  }
}

export default withStyles(styles)(DiggingTimeline);