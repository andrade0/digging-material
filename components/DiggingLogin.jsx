import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import DiggingUserModel from '../models/DiggingUserModel';
import DiggingModel from '../models/DiggingModel';
import DiggingUser from '../models/DiggingUser';
import fire from '../models/fire';
import firebase from 'firebase';


const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    width: '100%',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    backgroundColor: '#7E7E7E'
  },
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height:300
  },
  loginForm: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  menu: {
    width: 200
  },
  button: {
    marginTop: 20
  },
  divbutton: {
    width: 200,
    display: 'inline-block',
    textAlign: 'left'
  }
});




@observer
class DiggingLogin extends Component {
  constructor(props) {
    super(props);
    this.facebookLogin = this.facebookLogin.bind(this);
  }
  facebookLogin(){
    const provider = new firebase.auth.FacebookAuthProvider();
    const database = firebase.database();
    fire.auth().signInWithPopup(provider).then(function(response) {
      const facebookProfil = response.additionalUserInfo.profile;
      firebase.database().ref('users/' + response.user.uid).set(facebookProfil).then((response)=>{
        console.log(response);
      }).catch((error)=>{
        console.log(error);
      });
    }).catch(function(error) {

    });
  }
  render(){
    const { classes } = this.props;
    return <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item style={{visibility: 'hidden'}} xs>
          <Paper className={classes.paper}>xs</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <img onClick={()=>{
              DiggingUser.login(()=>{
                DiggingModel.loggingCalled = true;
              });
            }} src='/public/img/facekook_login.png' style={{width: 300}} />
          </Paper>
        </Grid>
        <Grid item style={{visibility: 'hidden'}} xs>
          <Paper className={classes.paper}>xs</Paper>
        </Grid>
      </Grid>
    </div>;
  }
}

export default withStyles(styles)(DiggingLogin);
