import React, { Component } from 'react';
import PropTypes from 'prop-types';
import jQuery from 'jquery';
import moment from 'moment';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import PlayIcon from 'material-ui-icons/PlayArrow';
import SaveIcon from 'material-ui-icons/Save';
import RemoveIcon from 'material-ui-icons/Remove';
import DiggingPlayerModel from '../models/DiggingPlayerModel';
import DiggingLikesModel from '../models/DiggingLikesModel';
import DiggingTrackActions from '../components/DiggingTrackActions';
import DiggingModel from '../models/DiggingModel';
import DiggingCssModel from '../models/DiggingCssModel';


import {
  CellMeasurer,
  CellMeasurerCache,
  createMasonryCellPositioner,
  Masonry
} from 'react-virtualized';


import {
  Grid,
  VirtualTableView,
  TableHeaderRow,
  TableColumnResizing,
  TableFilterRow
} from '@devexpress/dx-react-grid-material-ui';
import {
  SortingState,
  LocalSorting,
  FilteringState,
  LocalFiltering,
  IntegratedSorting
} from '@devexpress/dx-react-grid';


const styles = theme => ({
  root: {
    flexGrow: 1
  },
  card: {
    minWidth: 440
  },
  releaseTitle: {

  },
  releaseArtist: {

  },
  releaseLabel: {

  },
  releaseGenre: {

  },
  releasePlayedBy: {

  },
  releaselogo: {
    height: 25,
    width: 130
  },
  releaseTracks: {
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'scroll'
  },
  releaseTrack: {
    marginTop: 10,
    color: '#222222',
    marginLeft: 10
  },
  releaseCover: {
    height: 80,
    width: 80,
    marginRight: 10,
    marginBottom: 10
  },
  releaseWrapper: {
    display: 'flex',
    flexDirection: 'column'
  },
  releaseInfoWrapper: {
    display: 'flex'
  },
  releaseInfo: {
    display: 'flex',
    flexDirection: 'column'
  },
  nodatadiv: {
    display: 'block',
    height: '100%'
  }
});

const Cell = (row, columnName) => {
  if(columnName === 'actions') {
    return <DiggingTrackActions track={row} />
  } else if (columnName === 'cover') {
    return <img style={{ width: 50 }} src={row.cover} />
  } else if (columnName === 'name') {
    if(row.type === 'artist'){
      return <span style={{cursor: 'pointer', color: '#3f51b5'}} onClick={()=>{
        DiggingModel.addTab('Music for artist "'+row.name+'"', 'artist', row.name, null, null);
      }}>{row.name}</span>
    } else if (row.type === 'label') {
      return <span style={{cursor: 'pointer', color: '#3f51b5'}} onClick={()=>{
        DiggingModel.addTab('Music for label "'+row.name+'"', 'label', row.name, null, null);
      }}>{row.name}</span>
    }
  } else if (columnName === 'url') {
    return <a href={row.url} target="_blank"><img src={'/'+row.logo} /></a>
  } else if (columnName === 'label') {
    return <span style={{cursor: 'pointer', color: '#3f51b5'}} onClick={()=>{
      DiggingModel.addTab('Music for label "'+row.label+'"', 'label', row.label, null, null);
    }}>{row.label}</span>
  } else if(columnName === 'releaseDate'){
    return <span>{moment(row[columnName]).format("Do MMM YY")}</span>
  }
  else {
    return <span>{row[columnName]}</span>
  }

};

@observer
class DiggingCol extends Component {

  constructor(props, context) {
    super(props, context);

    this.cache = new CellMeasurerCache({
      defaultHeight: 280,
      defaultWidth: 450,
      fixedWidth: true
    });

    this.cellPositioner = createMasonryCellPositioner({
      cellMeasurerCache: this.cache,
      columnCount: 2,
      columnWidth: 450,
      spacer: 20
    });

    switch (props.col.viewMode) {
      case 'track':
        this.state = {
          sorting: [
            { columnName: 'title', direction: 'asc' },
            { columnName: 'artist', direction: 'asc' },
            { columnName: 'label', direction: 'asc' },
            { columnName: 'genre', direction: 'asc' },
            { columnName: 'releaseDate', direction: 'desc' }
          ]
        };
        break;
      case 'link':
        this.state = {
          sorting: [
            { columnName: 'name', direction: 'asc' }
          ]
        };
        break;
      default:
        break;
    }

    this.changeSorting = sorting => this.setState({ sorting });
    this.cellRenderer = this.cellRenderer.bind(this);
    this.resetMasonry = this.resetMasonry.bind(this);
  }

  cellRenderer ({ index, key, parent, style }) {
    const release = this.props.col.releases[index];
    const { classes } = this.props;
    style.padding = 10;
    return (
      <CellMeasurer
        cache={this.cache}
        index={index}
        key={key+Math.random()}
        parent={parent}
      >
        <div style={style}>
          <Card className={classes.card}>
            <CardContent className={classes.releaseWrapper}>
              <div className={classes.releaseInfoWrapper}>
                <img
                  src={release.cover}
                  className={classes.releaseCover}
                />
                <div className={classes.releaseInfo}>
                  <Typography onClick="" className={classes.releaseArtist} align="left">{release.artist}</Typography>
                  <Typography className={classes.releaseTitle} align="left">{release.title}</Typography>
                  <Typography className={classes.releaseLabel} align="left">{release.label}</Typography>
                  <Typography className={classes.releaseGenre} align="left">{release.genre}</Typography>
                  <Typography className={classes.releasePlayedBy} align="left">{release.PlayedBy}</Typography>
                  <img
                    src={'/'+release.logo}
                    className={classes.releaselogo}
                  />
                </div>
              </div>
              <div className={classes.releaseTracks}>
                {release.tracks.map((track) => {
                  return (<DiggingTrackActions key={track.id} track={track} />);
                })}
              </div>
            </CardContent>
          </Card>
        </div>
      </CellMeasurer>
    )
  }

  resetMasonry() {
    this.cache.clearAll();
    if(this.masonryRef) {
      this.masonryRef.recomputeCellPositions();
    }
  }

  componentDidUpdate(){
    DiggingModel.resize();
  }

  render() {


    let columns = [];

    const { classes } = this.props;

    switch (this.props.col.viewMode) {
      case 'track':
        columns = [
          { name: 'cover', title: 'Cover' },
          { name: 'actions', title: '' },
          { name: 'label', title: 'Label' },
          { name: 'url', title: 'Source' },
          { name: 'genre', title: 'Genre' },
          { name: 'releaseDate', title: 'Date' }
        ];
        if(this.props.col.tracks.length > 0) {
          const rows = this.props.col.tracks.reduce((c, v) => {
            c.push(v);
            return c;
          }, []);
          return (<div className='tableOfTracks'><Grid
            getCellValue={Cell}
            rows={rows}
            columns={columns}
            getRowId={(row)=>{return row.id+Math.random();}}
          >
            <SortingState
              sorting={this.state.sorting}
              onSortingChange={this.changeSorting}
            />
            <LocalSorting />
            <VirtualTableView height={DiggingCssModel.tablesHeight}  />
            <TableColumnResizing
              columnWidths={{
                cover: 100, actions: 500, label: 150, url: 100, genre: 150, releaseDate: 100
              }}
              onColumnWidthsChange={this.changeColumnWidths}
            />
            <TableHeaderRow allowSorting />
          </Grid></div>);
        } else {
          return <div className={classes.nodatadiv}></div>;
        }
        break;
      case 'link':
        columns = [
          { name: 'name', title: 'Name' }
        ];
        if(this.props.col.links.length > 0) {
          const rows = this.props.col.links.reduce((c, v) => {
            c.push(v);
            return c;
          }, []);
          return (<Grid
            rows={rows}
            getCellValue={Cell}
            columns={columns}
            getRowId={(row)=>{return row.name+Math.random();}}
          >
            <SortingState
              sorting={this.state.sorting}
              onSortingChange={this.changeSorting}
            />
            <LocalSorting />
            <VirtualTableView height={500} />
            <TableHeaderRow allowSorting />
          </Grid>);
        } else {
          return <div className={classes.nodatadiv}></div>;
        }
        break;
      case 'release':
        this.resetMasonry();
        return (<Masonry
          cellCount={this.props.col.releases.length}
          cellMeasurerCache={this.cache}
          cellPositioner={this.cellPositioner}
          cellRenderer={this.cellRenderer}
          height={600}
          width={1000}
          id={Math.random()}
          tabIndex={Math.random()}
          ref={this.setMasonryRef}
        />);
        break;
      default:
        break;
    }


  }
}

export default withStyles(styles)(DiggingCol);
