import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import DiggingCssModel from '../models/DiggingCssModel';
import TextField from 'material-ui/TextField';
import sources from '../digging/sources';
import diggingCurl from '../digging/diggingCurl';
import diggingHelpers from '../digging/diggingHelpers';


const styles = theme => ({

});

@observer
class DiggingDebug extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: 'https://www.discogs.com/Andrade-Design-Pattern/master/54654',
      source: 'discogs',
      release: {}
    };

    this.handleUrlChange = this.handleUrlChange.bind(this);
    this.handleSourceChange = this.handleSourceChange.bind(this);
    this.getDebug = this.getDebug.bind(this);
  }
  handleUrlChange(event){
    this.setState({
      url: event.target.value
    });
  };
  handleSourceChange(event){
    this.setState({
      source: event.target.value
    });
  };
  getDebug(){
    if(sources[this.state.source].parseDetail && this.state.url.length > 10){
      diggingCurl(this.state.url, (error, result)=>{
        if(result.html){
          const r = sources[this.state.source].parseDetail(result.html, this.state.url);
          const release = diggingHelpers.prepareRelease(sources[this.state.source], r);
          this.setState({
            release: release
          });
        }
      })
    }
  }
  render() {
    const {classes} = this.props;
    return <div className="diggingDebug" style={{color: '#000000'}}>
      <form onSubmit={(e)=>{e.preventDefault(); this.getDebug();}}>
        <TextField
          label="URL"
          style={{color: '#000000'}}
          placeholder="URL from source detail"
          value={this.state.url}
          onChange={(event)=>{this.handleUrlChange(event)}}
          margin="normal"
        />
        <TextField
          label="Source"
          style={{color: '#000000'}}
          placeholder="Source name"
          value={this.state.source}
          onChange={(event)=>{this.handleSourceChange(event)}}
          margin="normal"
        />
        <button>DEBUG</button>
        <div><pre>{JSON.stringify(this.state.release, null, 2) }</pre></div>
      </form>
    </div>
  }
}

export default withStyles(styles)(DiggingDebug);
