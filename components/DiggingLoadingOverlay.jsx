import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingModel from '../models/DiggingModel'
import { LinearProgress } from 'material-ui/Progress';
import { CircularProgress } from 'material-ui/Progress';
import purple from 'material-ui/colors/purple';


const styles = theme => ({
  open: {
    width: '100%',
    height: '100%',
    backgroundColor: '#000000',
    opacity: 0.5,
    position: 'fixed',
    zIndex: 999999,
    display: 'block'
  },
  close: {
    with: 0,
    height: 0,
    backgroundColor: '#000000',
    opacity: 0.0,
    display: 'none',
    position: 'fixed',
    zIndex: -9
  },
  root: {
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '25%',
    textAlign: 'center'
  },
  progress: {
    margin: `0 ${theme.spacing.unit * 2}px`
  }
});

@observer
class DiggingLoadingOverlay extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    const { classes } = this.props;

    let loading = false;
    if(DiggingModel.loading === true || DiggingModel.loading === false) {
      loading = DiggingModel.loading;
    }

    let _class = classes.close;
    if(loading) {
      _class = classes.open;
    }

    return (
      <div className={_class}>
        <div className={classes.root}>
          <CircularProgress className={classes.progress} />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DiggingLoadingOverlay);
