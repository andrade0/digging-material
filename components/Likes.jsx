import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import DiggingLikesModel from '../models/DiggingLikesModel';
import DiggingTrackActions from '../components/DiggingTrackActions';
import DiggingCssModel from '../models/DiggingCssModel';

import {
  Grid,
  VirtualTableView,
  TableHeaderRow,
  TableFilterRow,
  TableColumnResizing
} from '@devexpress/dx-react-grid-material-ui';
import {
  SortingState,
  LocalSorting,
  FilteringState,
  LocalFiltering
} from '@devexpress/dx-react-grid';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16
  })
});

const Cell = (row, columnName) => {

  if(columnName === 'actions') {
    return <DiggingTrackActions track={row} />
  } else if (columnName === 'cover') {
    return <img style={{width: 50}} src={row.cover}/>
  } else if (columnName === 'url') {
    return <a href={row.url} target="_blank"><img src={'/'+row.logo} /></a>
  } else if (columnName === 'label') {
    return <span style={{cursor: 'pointer', color: '#3f51b5'}} onClick={()=>{
      DiggingModel.addTab('Music for label "'+row.label+'"', 'label', row.label, null, null);
    }}>{row.label}</span>
  } else {
    return <span>{row[columnName]}</span>
  }

};

@observer
class Likes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sorting: [
        { columnName: 'title', direction: 'asc' },
        { columnName: 'artist', direction: 'asc' },
        { columnName: 'label', direction: 'asc' },
        { columnName: 'genre', direction: 'asc' }
      ]
    };
  }
  render(){

    const { classes } = this.props;
    const columns = [
      { name: 'cover', title: 'Cover' },
      { name: 'actions', title: '' },
      { name: 'label', title: 'Label' },
      { name: 'url', title: 'Source' },
      { name: 'genre', title: 'Genre' }
    ];
    if(DiggingLikesModel.tracks.length > 0) {

      const rows = DiggingLikesModel.tracks.reduce((c, v) => {
        c.push(v);
        return c;
      }, []);

      return (<div><Paper className={classes.root} elevation={4}>
        <Typography type="headline" component="h3">
          Your music
        </Typography>
      </Paper><Grid
        getCellValue={Cell}
        rows={rows}
        columns={columns}
        getRowId={(row)=>{return row.id+Math.random();}}
      >
        <SortingState
          sorting={this.state.sorting}
          onSortingChange={this.changeSorting}
        />
        <LocalSorting />
        <VirtualTableView height={DiggingCssModel.tablesHeight} />
        <TableColumnResizing
          columnWidths={{
            delete: 50, cover: 100, actions: 500, label: 150, url: 100, genre: 150
          }}
          onColumnWidthsChange={this.changeColumnWidths}
        />
        <TableHeaderRow allowSorting />
      </Grid></div>);
    } else {
      return <div>No data</div>;
    }
  }
}

export default withStyles(styles)(Likes);
