import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingSuggestModel from '../models/DiggingSuggestModel';
import DiggingTrackActions from '../components/DiggingTrackActions';
import YouTube from 'react-youtube';
import Paper from 'material-ui/Paper';
import DiggingCssModel from '../models/DiggingCssModel';
import Button from 'material-ui/Button';

const styles = theme => ({
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  title: {
    fontSize: 40
  },
  wrapper: {
    backgroundColor: theme.palette.background.default
  },
  button: {
    margin: theme.spacing.unit
  }
});


@observer
class DiggingSuggestor extends Component {
  constructor(props) {
    super(props);
    this.suggest = this.suggest.bind(this);
  }
  suggest(){
    DiggingSuggestModel.suggest();
  }
  componentWillMount(){
    DiggingSuggestModel.suggest();
  }
  render(){
    const { classes } = this.props;

    if(DiggingSuggestModel.youtubeId && DiggingSuggestModel.track) {
      return (<div className={classes.wrapper} style={{
        height: DiggingCssModel.windowHeight,
        textAlign: 'center'
      }}>
        <Paper className={classes.paper}>
          <div className={classes.title}>Music you may like</div>
        </Paper>
        <div style={{
          padding: 50
        }}>
          <YouTube
            videoId={DiggingSuggestModel.youtubeId}
            id={DiggingSuggestModel.youtubeId}
            className=""
            opts={{ height: '390', width: '640', playerVars: {  autoplay: 1 }}}
            onReady={()=>{}}
            onPlay={()=>{}}
            onPause={()=>{}}
            onEnd={()=>{
              DiggingSuggestModel.suggest();
            }}
            onError={()=>{
              DiggingSuggestModel.suggest();
            }}
            onStateChange={()=>{}}
            onPlaybackRateChange={()=>{}}
            onPlaybackQualityChange={()=>{}}
          />
          <div>
            <Button onClick={this.suggest} raised className={classes.button}>
              Next
            </Button>
          </div>
          <div><DiggingTrackActions suggestor={true} track={DiggingSuggestModel.track} /></div>
          <p style={{color: '#000000'}}>
            {DiggingSuggestModel.explanation}
          </p>
        </div>
      </div>)
    } else {
      return (<div style={{
        height: DiggingCssModel.windowHeight,
        textAlign: 'center'
      }}><Button onClick={this.suggest} raised className={classes.button}>
        Next
      </Button></div>)
    }

  }
}

export default withStyles(styles)(DiggingSuggestor);

//