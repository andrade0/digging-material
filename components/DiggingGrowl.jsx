import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingModel from '../models/DiggingModel';
import Snackbar from 'material-ui/Snackbar';

const styles = theme => ({
  snackbar: {
    margin: theme.spacing.unit
  }
});

@observer
class DiggingGrowl extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: true
    };

    this.handleRequestClose = this.handleRequestClose.bind(this);
  }
  handleRequestClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };
  render(){
    const { classes } = this.props;
    return <div>{DiggingModel.notifications.map((notification)=>{
      return <Snackbar
        key={Math.random()}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        open={this.state.open}
        autoHideDuration={1000}
        onRequestClose={this.handleRequestClose}
        SnackbarContentProps={{
          'aria-describedby': 'message-id'
        }}
        message={<span id="message-id">{notification}</span>}
      />
    })}
    </div>;
  }
}

export default withStyles(styles)(DiggingGrowl);

