import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import CloseIcon from 'material-ui-icons/Close';
import Tabs, { Tab } from 'material-ui/Tabs';
import DigginUser from '../models/DiggingUser';
import diggingModel from '../models/DiggingModel';
import DiggingTab from '../components/DiggingTab';
import S from 'string';
import sourcesObjects from '../digging/sources';
import DiggingSourcesLoadingStatus from '../components/DiggingSourcesLoadingStatus';

function TabContainer(props) {
  return <div>{props.children}</div>;
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#ffffff'
  },
  AppBarTabs: {
    backgroundColor: '#ffffff',
    marginTop: 10
  },
  CloseIcon: {
    height: 15,
    marginTop: 16,
    marginLeft: 0
  }
});

@observer
class Digging extends Component {
  constructor(props, context) {
    super(props, context);
    this.removeTab = this.removeTab.bind(this);
  }

  removeTab(index){
    diggingModel.removeTab(index);
  }

  componentDidMount() {

    let type = 'news';
    if(this.props.match.params.type && this.props.match.params.type.length > 0) {
      type = this.props.match.params.type;
    }

    const sourceType = ( type==='news' || type==='mynews' )?'news':'search';

    let query = null;
    if(this.props.match.params.query && this.props.match.params.query.length > 0) {
      query = this.props.match.params.query;
    }

    let title = '';
    switch (type) {
      case 'mynews':
        title = 'Yours news';
        break;
      case 'news':
        title = 'News from record stores';
        break;
      case 'artist':
        title = S(sourceType).capitalize().s+' '+S(type).capitalize().s+' '+S(query).capitalize().s;
        break;
      case 'label':
        title = S(sourceType).capitalize().s+' '+S(type).capitalize().s+' '+S(query).capitalize().s;
        break;
      default:
        title = query;
        break;
    }


    let sources = DigginUser.sources.reduce((_sources, sourceItem) => {
      if(sourceItem.type === sourceType && sourceItem.value === true)
      {
        const sourceObject = sourcesObjects[sourceItem.name];

        const testType = (type === 'news' || type === 'mynews')?'news':type;

        if(sourceObject[testType]) {
          _sources.push(sourceItem.name);
        }
      }
      return _sources;
    }, []);

    if(this.props.match.params.sources && this.props.match.params.sources.length > 0) {
      const sourcesFromStr = this.props.match.params.sources.split(',').map((str)=>{return str.trim();});
      sources = sourcesFromStr.reduce((current, sourceName) => {
        const sourceObject = sourcesObjects[sourceName];
        const testType = (type === 'news' || type === 'mynews')?'news':type;
        if(sourceObject && current.indexOf(sourceName)<0 && sourceObject[testType])
        {
          current.push(sourceName)
        }
        return current;
      }, []);
    }

    let viewMode = 'track';
    if(this.props.match.params.viewMode && this.props.match.params.viewMode.length > 0 && ( this.props.match.params.viewMode === 'track' || this.props.match.params.viewMode === 'release' )) {
      viewMode = this.props.match.params.viewMode;
    } else {
      if(sourceType === 'news') {
        if(DigginUser.newsPreferReleaseViewMode) {
          viewMode = 'release';
        } else {
          viewMode = 'track';
        }
      } else {
        if(DigginUser.searchPreferReleaseViewMode) {
          viewMode = 'release';
        } else {
          viewMode = 'track';
        }
      }
    }

    if(!diggingModel.tabExists(title)) {
      diggingModel.addTab(title, type, query, viewMode, sources);
    } else {
      diggingModel.setActive(title);
    }
  }
  handleChange = (event, value) => {
    diggingModel.activeTab = value;
  };
  render() {
    const { classes } = this.props;
    const value  = (diggingModel.activeTab && diggingModel.activeTab>=0)?diggingModel.activeTab:0;
    return (
    <div className={classes.root}>
      <AppBar className={classes.AppBarTabs} position="static" color="default">
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          scrollable
          scrollButtons="auto"
        >
          {diggingModel.tabs.map((tab, index) => {
            return (<Tab key={tab.title} className="AppBarTabsTitle" label={<span style={{
              whiteSpace: 'nowrap',
              maxWidth: 250,
              overflowX: 'hidden',
              display: 'inline-block',
              textOverflow: 'ellipsis'
            }}><CloseIcon className={classes.CloseIcon} onClick={()=>{this.removeTab(tab)}} />{tab.title}</span>} />);
          })}
        </Tabs>
      </AppBar>
      {diggingModel.tabs.map((tab, index) => {
        if(value === index)
        {
          return <TabContainer key={tab.title+index}>
            <DiggingSourcesLoadingStatus tab={tab} />
            <DiggingTab tab={tab} />
          </TabContainer>
        }
        else
        {
          return null;
        }
      })}
    </div>
    );
  }
}

export default withStyles(styles)(Digging);
