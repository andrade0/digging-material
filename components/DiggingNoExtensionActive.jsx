import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingModel from '../models/DiggingModel';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { LinearProgress } from 'material-ui/Progress';

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    width: '100%',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex'
  },
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height:300
  },
  loginForm: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  menu: {
    width: 200
  },
  button: {
    marginTop: 20
  },
  divbutton: {
    width: 200,
    display: 'inline-block',
    textAlign: 'left'
  }
});


@observer
class DiggingNoExtensionActive extends Component {
  constructor(props) {
    super(props);
  }
  render(){
    const { classes } = this.props;

    let message = 'Loading';
    if(DiggingModel.startupLoaderProgress >= 100) {
      message = <div>Actually, Digging music is working only on GOOGLE CHROME with the <a href='https://chrome.google.com/webstore/detail/digging-music/olmjkpkekphjjjkhplobhkmdbelehiek' target='_blank'>digging music chrome extension</a>. If you have it already installed, just disable and re enable it then refresh the page.</div>;
    }

    return <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item style={{visibility: 'hidden'}} xs>
          <Paper className={classes.paper}>xs</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
            <LinearProgress mode="query" value={DiggingModel.startupLoaderProgress} />
            {message}
          </Paper>
        </Grid>
        <Grid item style={{visibility: 'hidden'}} xs>
          <Paper className={classes.paper}>xs</Paper>
        </Grid>
      </Grid>
    </div>;
  }
}

export default withStyles(styles)(DiggingNoExtensionActive);
