import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingPlayerModel from '../models/DiggingPlayerModel';
import DiggingCssModel from '../models/DiggingCssModel';
import fire from '../models/fire';
import firebase from 'firebase';


const styles = theme => ({

});


@observer
class DiggingPlayerCandidates extends Component {
  constructor(props) {
    super(props);
  }
  render(){
    const { classes } = this.props;
    return <div className="playerCandidatesWrapper">
      <div className="playerCandidates">
        {DiggingPlayerModel.tracks.map((c)=>{
          let playingClass = '';
          if(
            DiggingPlayerModel.playingTrack
            && DiggingPlayerModel.playingTrack.url === c.url
          ){
            playingClass = 'playing';
          }
          if(c.img && !c.img.match(new RegExp('http', 'i')) && c.img.substr(0, 1) !== '/'){
            c.img = '/'+c.img;
          }

          if(c && c.msg){
            return <div className={playingClass} key={c.id}>
              {c.msg}
            </div>
          } else {
            return <div className={playingClass} key={c.id} onClick={()=>{
              DiggingPlayerModel.progress = 0;
              DiggingPlayerModel.playingTrack = c;
              DiggingPlayerModel.playing = true;
            }}><img src={c.img} style={{width: 30, height: 30}} /><span>{c.playerTitle}</span>
            </div>
          }


        })}
      </div>
    </div>;
  }
}

export default withStyles(styles)(DiggingPlayerCandidates);
