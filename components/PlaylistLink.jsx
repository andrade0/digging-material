import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import { Link, Switch, Route } from 'react-router-dom';
import { DropTarget } from 'react-dnd'
import Badge from 'material-ui/Badge';
import {Icon} from 'react-fa';
import DiggingPlaylistsModel from '../models/DiggingPlaylistsModel';
import DiggingPlaylistEditorModel from '../models/DiggingPlaylistEditorModel';

const style = {
  text: {
    color: '#ffffff'
  }
};


const styles = theme => ({
  nested: {
    color: '#ffffff',
    height: 20
  },
  text: {
    color: '#ffffff',
    fontSize: 12
  },
  icon2: {
    color: '#ffffff',
    height: 18,
    marginRight: 0,
    marginBottom: -2
  }
});

@DropTarget(props => props.accepts, {
  drop(props, monitor) {
    props.onDrop(monitor.getItem())
  }
}, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
@observer
class PlaylistLink extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    accepts: PropTypes.arrayOf(PropTypes.string).isRequired,
    lastDroppedItem: PropTypes.object,
    onDrop: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
  }

  render(){
    const {
      classes,
      accepts,
      isOver,
      canDrop,
      connectDropTarget,
      lastDroppedItem
    } = this.props;
    const isActive = isOver && canDrop;

    let backgroundColor = '#222';
    if (isActive) {
      backgroundColor = 'darkgreen';
    } else if (canDrop) {
      backgroundColor = 'darkkhaki';
    }

    return connectDropTarget(<li style={{ ...style, backgroundColor }}>
      <Icon name='pencil-square' onClick={()=>{
        DiggingPlaylistEditorModel.open = true;
        DiggingPlaylistEditorModel.playlist = this.props.playlist;
      }} />
      <Icon name='trash' onClick={()=>{
        this.props.playlist.remove();
      }} />
      <Link onClick={()=>{
        DiggingPlaylistsModel.setActivePlaylists(this.props.playlist);
      }} to={`/playlist/${this.props.playlist.id}`}>
        {this.props.playlist.title}
      </Link>
      <span className="diggingBadge">
        {this.props.playlist.tracks.length}
      </span>
    </li>);
  }
}

export default withStyles(styles)(PlaylistLink);
