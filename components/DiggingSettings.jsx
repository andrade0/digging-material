import React, { Component } from "react";
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import green from 'material-ui/colors/green';
import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import Button from 'material-ui/Button';
import DigginUser from '../models/DiggingUser';
import sourcesObjects from '../digging/sources';
import TextField from 'material-ui/TextField';
import AddIcon from 'material-ui-icons/Add';
import List, {
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import FolderIcon from 'material-ui-icons/Folder';
import DeleteIcon from 'material-ui-icons/Delete';
import Typography from 'material-ui/Typography';
import diggingCurl from '../digging/diggingCurl';
import fire from '../models/fire';
import firebase from 'firebase';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  checked: {
    color: green[500]
  },
  title: {
    fontSize: 40
  },
  miniTitle: {
    fontSize: 20
  },
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4
  },
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: 'none'
  }
});


@observer
class DiggingSettings extends Component {
  constructor(){
    super();

    this.state = {
      open: false,
      newYoutubeChannelUrl: ''
    };

    this.handleNewYoutubeChannel = this.handleNewYoutubeChannel.bind(this);
    this.addYoutubeChannelName = this.addYoutubeChannelName.bind(this);
    this.removeYoutubeChannelName = this.removeYoutubeChannelName.bind(this);

  }
  addYoutubeChannelName(){
    const channelExists = DigginUser.youtubeChannels.reduce((exists, channel) => {
      if(channel.url === this.state.newYoutubeChannelUrl) {
        exists = channel;
      }
      return exists;
    }, false);

    if(channelExists) {
      alert('You already have this channel in your list: '+channelExists.name+'( '+channelExists.url+' )');
    } else {
      if(this.state.newYoutubeChannelUrl.match(new RegExp('https://www.youtube.com/channel/', 'i'))) {
        const channelId = this.state.newYoutubeChannelUrl.replace('https://www.youtube.com/channel/', '');
        const url = 'https://www.googleapis.com/youtube/v3/channels?key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&id='+channelId+'&part=snippet';
        diggingCurl(url, (error, result) => {
          const channelName = result.html.items["0"].snippet.title;
          DigginUser.addYoutubeChannel(this.state.newYoutubeChannelUrl, channelName);
          this.setState({
            newYoutubeChannelUrl: ''
          });
        });
      } else {
        alert('Youtube channel should start by : https://www.youtube.com/channel/')
      }
    }
  }
  removeYoutubeChannelName(key){
    DigginUser.removeYoutubeChannel(key);
  }
  handleNewYoutubeChannel(event){
    this.setState({
      newYoutubeChannelUrl: event.target.value
    });
  }
  handleChange = name => event => {
    const expl = name.split('_');
    if(expl && expl.length === 2) {
      const type = expl[0];
      const sourceName = expl[1];
      DigginUser.updateSourceStatus(sourceName, type, event.target.checked);
    } else {
      DigginUser[name] = event.target.checked;
    }
    this.setState({
      open: true
    });
  };
  handleRequestClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={this.state.open}
          autoHideDuration={2000}
          onRequestClose={this.handleRequestClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">Setting saved</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleRequestClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Paper className={classes.paper}>
          <div className={classes.title}>Settings</div>
        </Paper>
        <Grid style={{
          padding: 10
        }} container spacing={24}>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>
              <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={DigginUser.newsPreferPreview}
                    onChange={this.handleChange('newsPreferPreview')}
                    value="newsPreferPreview"
                  />
                }
                label="News prefere preview"
              />
            </FormGroup>
            </Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>
              <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={DigginUser.searchPreferPreview}
                    onChange={this.handleChange('searchPreferPreview')}
                    value="searchPreferPreview"
                  />
                }
                label="Search prefere preview"
              />
            </FormGroup>
            </Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>
              <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={DigginUser.newsPreferReleaseViewMode}
                    onChange={this.handleChange('newsPreferReleaseViewMode')}
                    value="newsPreferReleaseViewMode"
                  />
                }
                label="News prefer release view mode"
              />
            </FormGroup>
            </Paper>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Paper className={classes.paper}>
              <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={DigginUser.searchPreferReleaseViewMode}
                    onChange={this.handleChange('searchPreferReleaseViewMode')}
                    value="searchPreferReleaseViewMode"
                  />
                }
                label="Search prefer release view mode"
              />
            </FormGroup>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <div className={classes.miniTitle}>News</div>
              {Object.values(sourcesObjects).map((source)=>{
                const varname = `news_${source.value}`;
                const label = `${source.name}`;
                return (<FormGroup key={varname} row>
                  <img src={source.logo} />
                  <FormControlLabel
                    control={ <Checkbox
                    checked={DigginUser.getSourceStatus(source.value, 'news')}
                    onChange={this.handleChange(varname)}
                    value={`varname`}
                />
                    }
                    label={label}
                  />
                </FormGroup>);
              })}
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}>
              <div className={classes.miniTitle}>Search</div>
              {Object.values(sourcesObjects).map((source)=>{
                const varname = `search_${source.value}`;
                const label = `${source.name}`;
                return (<FormGroup key={varname} row>
                  <img src={source.logo} />
                  <FormControlLabel
                    control={ <Checkbox
                      checked={DigginUser.getSourceStatus(source.value, 'search')}
                      onChange={this.handleChange(varname)}
                      value={`varname`}
                    />
                    }
                    label={label}
                  />
                </FormGroup>);
              })}
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper className={classes.paper}>
              <div className={classes.miniTitle}>Add new Youtube Channels</div>
              <TextField
                id="helperText"
                label="channel URL"
                value={this.state.newYoutubeChannelUrl}
                className={classes.textField}
                helperText="Add you favorites youtube channels"
                margin="normal"
                onChange={this.handleNewYoutubeChannel}
              />
              <Button onClick={this.addYoutubeChannelName} fab color="primary" aria-label="add" className={classes.button}>
                <AddIcon />
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12}>
            <Paper className={classes.paper}>
              <div className={classes.miniTitle}>Youtube Channels</div>
              <List dense={false}>
                {DigginUser.youtubeChannels.map((youtubeChannel) => {
                  return (<ListItem key={youtubeChannel.key} button>
                    <ListItemAvatar>
                      <Avatar>
                        <FolderIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={youtubeChannel.name}
                      secondary={false ? 'Secondary text' : null}
                    />
                    <ListItemSecondaryAction>
                      <IconButton onClick={()=>{this.removeYoutubeChannelName(youtubeChannel.key);}} aria-label="Delete">
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>);
                })}
              </List>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

DiggingSettings.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withStyles(styles)(DiggingSettings);

