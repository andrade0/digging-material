import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import PlayIcon from 'material-ui-icons/PlayArrow';
import PauseIcon from 'material-ui-icons/Pause';
import StopIcon from 'material-ui-icons/Stop';
import NextIcon from 'material-ui-icons/SkipNext';
import PrevIcon from 'material-ui-icons/SkipPrevious';
import PlaylistAddIcon from 'material-ui-icons/PlaylistAdd';
import AddIcon from 'material-ui-icons/Hearing';
import { LinearProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import DraftsIcon from 'material-ui-icons/Drafts';
import { Link } from 'react-router-dom';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Menu, { MenuItem } from 'material-ui/Menu';
import { ListItemIcon, ListItemText } from 'material-ui/List';
import DiggingModel from '../models/DiggingModel';

const styles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  menuItem: {
    margin: 0,
    marginTop: 20,
    height: 12,
    '&:focus': {
      background: theme.palette.primary[500],
      '& $text, & $icon': {
        color: theme.palette.common.white
      }
    }
  },
  text: {
    color: '#ffffff',
    fontSize: 12,
    margin: 0
  },
  icon: {
    color: '#ffffff'
  },
  link: {
    marginLeft: 0
  },
  icon2: {
    height: 18,
    marginRight: 0,
    marginBottom: -2
  }
});

@observer
class DiggingHomeLink extends Component {
  constructor(props) {
    super(props);
  }
  render(){
    const { classes } = this.props;

    const activeTab = DiggingModel.getActiveTab();
    let url = '/';
    if(activeTab) {
      let query = activeTab.query;
      if(!activeTab.query) {
        query = 'news';
      }
      url = `/${query}/${activeTab.type}/${activeTab.sources.join(',')}/${activeTab.viewMode}`;
    }

    return (
      <MenuItem className={classes.menuItem}>
        <Link className={classes.link} to={url}>
          <ListItemIcon className={classes.icon}>
            <DraftsIcon className={classes.icon2} />
          </ListItemIcon>
        </Link>
        <ListItemText classes={{ text: classes.text }} inset primary="Home" />
      </MenuItem>
    );
  }
}

export default withStyles(styles)(DiggingHomeLink);
