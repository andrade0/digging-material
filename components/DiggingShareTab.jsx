import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import DiggingModel from '../models/DiggingModel';
import ShareIcon from 'material-ui-icons/Share';
import fire from '../models/fire';
import firebase from 'firebase';

const styles = theme => ({

});

@observer
class DiggingShareTab extends Component {
  constructor(props) {
    super(props);
    this.share = this.share.bind(this);
  }
  share(){
    if(this.props.tabIndex>=0){
      const tabContent = DiggingModel.tabs[this.props.tabIndex];
      const sharesListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/shares');
      const newShareRef = sharesListRef.push();
      const newShareRefKey = newShareRef.key;
      newShareRef.set(tabContent).then(()=>{
        console.log(tabContent);
      });
    }
  }
  render(){
    const { classes } = this.props;
    return (<ShareIcon onClick={this.share} />)
  }
}

export default withStyles(styles)(DiggingShareTab);
