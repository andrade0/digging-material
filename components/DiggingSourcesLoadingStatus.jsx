import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';
import { observer } from 'mobx-react';
import DiggingCssModel from '../models/DiggingCssModel';


const styles = theme => ({
  root: {
    flex: 1,
    marginBottom: 0,
    position: 'fixed',
    left: 240,
    backgroundColor: '#222222',
    opacity: 0.8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  paper: {
    textAlign: 'left',
    color: theme.palette.text.secondary
  },
  progress: {

  },
  card: {
    maxWidth: 120,
    padding: 0
  },
  media: {
    height: 20
  },
  sourceName: {
    textTransform: 'uppercase',
    fontSize: '10px',
    padding: '2px',
    backgroundColor: '#FFFFFF',
    textAlign: 'center'
  },
  cardContent: {
    margin: '0px'
  }
});

@observer
class DiggingSourcesLoadingStatus extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { classes } = this.props;

    const items = this.props.tab.sourcesProgress;

    return (<div style={{ bottom: DiggingCssModel.loadingBottomPosition }} className={classes.root}>
      {items.map((item) => {
        let content = <div key={item.source.value+Math.random()}></div>
        if(Math.floor(item.progress) < 100){

          content = <div style={{
            backgroundColor: item.source.color,
            textAlign: 'center',
            margin: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 4
          }}
          key={item.source.value+Math.random()}
          >
            <CircularProgress
              mode="determinate"
              value={Math.floor(item.progress)}
            />
            <img style={{marginTop: 6}} src={'/'+item.source.logo} />
          </div>
        }
        return (content);
    })}
    </div>);
  }
}

export default withStyles(styles)(DiggingSourcesLoadingStatus);
