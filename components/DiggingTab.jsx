import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import Tabs, { Tab } from 'material-ui/Tabs';
import DiggingCol from '../components/DiggingCol'
import DiggingTabModel from '../models/DiggingTabModel'
import AppBar from 'material-ui/AppBar';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';


function TabContainer(props) {
  return <div>{props.children}</div>;
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

/*
*
* display: flex;
 flex-direction: column;
 justify-content: flex-end;
*
* */

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#ffffff',
    paddingtop: 0,
    backgroundColor: theme.palette.background.paper
  },
  colname: {
    backgroundColor: theme.palette.background.paper,
    height: 30,
    marginRight: 10
  },
  blanc: {
    paddingTop: 10,
    backgroundColor: '#F7F7F7',
    display: 'flex',
    flexDirection: 'column'
  },
  icon2: {
    color: '#ffffff',
    height: 18,
    marginRight: 0,
    marginBottom: -2
  }
});

@observer
class DiggingTab extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange = (event, value) => {
    this.props.tab.activeColumn = value;
  };
  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar className={classes.blanc} position="static" color="default">
          <Tabs
            className="DiggingTabsClass"
            value={this.props.tab.activeColumn}
            onChange={this.handleChange}
            indicatorColor="none"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            {this.props.tab.columns.map((column, index) => {
              switch (column.viewMode) {
                case 'track':
                  return (<Tab className={classes.colname} key={this.props.tab.title+column.title+index} label={<span style={{
                    whiteSpace: 'nowrap',
                    maxWidth: 250,
                    overflowX: 'hidden',
                    display: 'inline-block',
                    textOverflow: 'ellipsis'
                  }}>{column.title} {'['+column.tracks.length+']'}</span>} />);
                  break;
                case 'link':
                  return (<Tab className={classes.colname} key={this.props.tab.title+column.title+index} label={<span style={{
                    whiteSpace: 'nowrap',
                    maxWidth: 250,
                    overflowX: 'hidden',
                    display: 'inline-block',
                    textOverflow: 'ellipsis'
                  }}>{column.title} {'['+column.links.length+']'}</span>} />);
                  break;
                default:
                  return (<Tab className={classes.colname} key={this.props.tab.title+column.title+index} label={<span style={{
                    whiteSpace: 'nowrap',
                    maxWidth: 250,
                    overflowX: 'hidden',
                    display: 'inline-block',
                    textOverflow: 'ellipsis'
                  }}>{column.title} {'['+column.releases.length+']'}</span>} />);
                  break;
              }
            })}
          </Tabs>
        </AppBar>
        {this.props.tab.columns.map((column, index) => {
          if(this.props.tab.activeColumn === index)
          {
            return <TabContainer key={column.title+index}><DiggingCol col={column} /></TabContainer>
          }
          else
          {
            return null;
          }
        })}
      </div>
    );
  }
}

export default withStyles(styles)(DiggingTab);
