import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import {withStyles} from 'material-ui/styles';
import {Icon} from 'react-fa';
import {Link} from 'react-router-dom';
import Badge from 'material-ui/Badge';
import DiggingLikesModel from '../models/DiggingLikesModel';

const styles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
    backgroundColor: '#222222',
    color: '#ffffff'
  },
  menuItem: {
    height: 24,
    margin: 0,
    color: '#ffffff',
    backgroundColor: '#222222',
    '&:focus': {
      background: theme.palette.primary[500],
      '& $text, & $icon': {
        color: theme.palette.common.white
      }
    }
  },
  text: {
    color: '#ffffff',
    fontSize: 12,
    margin: 0
  },
  icon: {
    color: '#ffffff',
    backgroundColor: '#222222'
  },
  icon2: {
    color: '#ffffff',
    height: 18,
    marginRight: 0,
    marginBottom: -2
  },
  badge: {}
});

@observer
class DiggingLikesLink extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {classes} = this.props;

    return (<Link to="/likes">
        <Icon name='heart' /> Likes
      <span className="diggingBadge">
        {DiggingLikesModel.tracks.length}
      </span>
    </Link>);

  }
}

export default withStyles(styles)(DiggingLikesLink);
