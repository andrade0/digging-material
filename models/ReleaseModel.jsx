import { observable, action } from 'mobx';
import md5 from 'md5';

import TrackModel from './TrackModel';

export default class ReleaseModel {
  id;
  title;
  artist;
  genre;
  label;
  cover;
  kind;
  playedBy;
  source;
  alias;
  query;
  year;
  releaseDate;
  logo
  tracks = [];
  constructor(release) {
    for(const index in release) {
      this[index] = release[index];
    }
    this.id = md5(release.artist + release.title + Math.random());
    if(release.tracks && release.tracks.length > 0) {
      this.tracks = release.tracks.map((_track) => {
        return new TrackModel(_track);
      });
    }
  }
  @action
  addTrack(track) {
    this.tracks.push(new TrackModel(track));
  }
}
