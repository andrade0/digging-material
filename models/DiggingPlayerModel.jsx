import { observable, action, intercept } from 'mobx';
import diggingCurl from '../digging/diggingCurl';
import Fuse from 'fuse.js';
import S from 'string';
import firebase from 'firebase';
import fire from '../models/fire';
import async from 'async';
import DiggingModel from '../models/DiggingModel';
import DiggingUser from '../models/DiggingUser';
import DiggingPlaylistsModel from '../models/DiggingPlaylistsModel';
import DiggingLikesModel from '../models/DiggingLikesModel';
import TrackModel from '../models/TrackModel';
import FuzzySet from 'fuzzyset.js';

class DiggingPlayerModel {
  @observable track = {};
  @observable volume = 1;
  @observable playing = false;
  @observable duration = 0;
  @observable progress = 0;
  @observable buffer = 0;
  @observable tracks = [];
  @observable playingTrack = null;
  constructor() {
    intercept(this, "track", change => {
      this.progress = 0;
      if(change.newValue && change.newValue.title) {

        let type = 'search';
        if(DiggingModel.tabs[DiggingModel.activeTab]){
          type = DiggingModel.tabs[DiggingModel.activeTab].type;
        }

        let tracks = [];

        if(change.newValue.ipfs){
          tracks.push({ title: 'mp3 from digging music blockchain', img: change.newValue.cover, url: 'https://ipfs.io/ipfs/'+change.newValue.ipfs });
        }

        let playerModePreference = null;

        this.tracks = [];
        this.playing = false;
        this.progress = 0;
        this.playingTrack = null;

        if(type === 'news') {
          if(DiggingUser.newsPreferPreview){
            playerModePreference = 'preview';
          } else {
            playerModePreference = 'youtube';
          }
        } else {
          if(DiggingUser.searchPreferPreview){
            playerModePreference = 'preview';
          } else {
            playerModePreference = 'youtube';
          }
        }

        if(playerModePreference === 'youtube'){
          async.waterfall([
            (cb)=>{
              this.youtubeCandidates(change.newValue.artist+' '+change.newValue.title, (_candidates)=> {
                tracks = [...tracks, ..._candidates];
                cb();
              });
            },
            (cb)=> {
              if(change.newValue.sample){
                tracks.push({ title: change.newValue.artist+' - '+change.newValue.title+' ['+change.newValue.source+' snippet]', img: change.newValue.logo, url: change.newValue.sample });
              }
              cb();
            }
          ], ()=>{
            if(tracks.length > 0){
              this.tracks = tracks.map((t)=>{
                const newobj = {...change.newValue, url: t.url, img: t.img, playerTitle: t.title};
                return new TrackModel(newobj);
              }).reverse();
              const len = tracks.length;
              this.playingTrack = this.tracks[len-1];
              this.playing = true;
            } else {
              this.tracks = [{msg: 'No playable source found for this track'}];
            }
          });
        } else if(playerModePreference === 'preview'){
          async.waterfall([
            (cb)=>{
              if(change.newValue.sample){
                tracks.push({ title: change.newValue.artist+' - '+change.newValue.title+' ['+change.newValue.source+' snippet]', img: change.newValue.logo, url: change.newValue.sample });
              }
              cb();
            },
            (cb)=>{
              this.youtubeCandidates(change.newValue.artist+' '+change.newValue.title, (_candidates)=>{
                tracks = [...tracks, ..._candidates];
                cb();
              });
            }
          ], ()=>{
            if(tracks.length > 0){
              this.tracks = tracks.map((t)=>{
                const newobj = {...change.newValue, url: t.url, img: t.img, playerTitle: t.title};
                return new TrackModel(newobj);
              }).reverse();
              const len = tracks.length;
              this.playingTrack = this.tracks[len-1];
              this.playing = true;
            } else {
              this.tracks = [{msg: 'No playable source found for this track'}];
            }
          });
        }
      }
      return change;
    });
  }
  @action
  play(track) {
    if(track === null) {

      if(this.track && this.track.play){
        if(this.track.playing){
          this.track.pause();
        } else {
          this.track.playing = true;
          this.playing = true;
        }
      } else {
        const activeTab = DiggingModel.activeTab;
        const tab = DiggingModel.tabs[activeTab];
        const activeCol = tab.activeColumn;
        const col = tab.columns[activeCol];
        if(tab.viewMode === 'track') {
          col.tracks[0].play();
        } else if (tab.viewMode === 'release'){
          col.releases[0].tracks[0].play();
        } else {
          alert('unknown viewMode: '+tab.viewMode);
        }
      }
    } else {
      if(this.track && this.track.play){
        if(track.artist && this.track.artist && this.track.title && track.title && track.title === this.track.title && track.artist === this.track.artist){
          if(this.track.playing){
            this.track.pause();
          } else {
            this.track.playing = true;
            this.playing = true;
          }
        } else {
          this.track.stop();
          this.track = track;
        }
      } else {
        this.track = track;
      }
    }
  }
  @action
  next() {
    if(this.track === null || ( this.track && !this.track.play)){
      this.play();
    } else {
      if(this.track.playlistId){
        const tracks = DiggingPlaylistsModel.playlists.reduce((c, v, i, a)=>{
          if(v.id === this.track.playlistId){
            c = v.tracks;
          }
          return c;
        }, []);
        if(tracks) {
          const nextTrack = tracks.reduce((_nextTrack, _track, index, allTracks) => {
            if(_track.id === this.track.id) {
              const nextIndex = index+1;
              if(allTracks[nextIndex]) {
                _nextTrack = allTracks[nextIndex];
              }
            }
            return _nextTrack;
          }, null);
          if(nextTrack) {
            nextTrack.play();
          } else {
            alert('there is no next track in this col');
          }
        } else {
          alert('there is no track in this playlist : '+this.track.playlistId);
        }
      } else if(this.track.inLikesTab) {
        const nextTrack = DiggingLikesModel.tracks.reduce((_nextTrack, _track, index, allTracks) => {
          if(_track.id === this.track.id) {
            const nextIndex = index+1;
            if(allTracks[nextIndex]) {
              _nextTrack = allTracks[nextIndex];
            }
          }
          return _nextTrack;
        }, null);
        if(nextTrack) {
          nextTrack.play();
        } else {
          alert('there is no next track in this col');
        }
      } else {
        const activeTab = DiggingModel.activeTab;
        const tab = DiggingModel.tabs[activeTab];
        const activeCol = tab.activeColumn;
        const col = tab.columns[activeCol];
        if(tab.viewMode === 'track') {
          const nextTrack = col.tracks.reduce((_nextTrack, _track, index, allTracks) => {
            if(_track.id === this.track.id) {
              const nextIndex = index+1;
              if(allTracks[nextIndex]) {
                _nextTrack = allTracks[nextIndex];
              }
            }
            return _nextTrack;
          }, null);
          if(nextTrack) {
            nextTrack.play();
          } else {
            alert('there is no next track in this col');
          }
        } else if (tab.viewMode === 'release'){
          const currentRelease = col.releases.reduce((_currentRelease, _release, index, allReleases) => {
            const currentTrackInThisRelease = _release.tracks.reduce((_currentTrackInThisRelease, _track) => {
              if(_track.id === this.track.id){
                _currentTrackInThisRelease = true;
              }
              return _currentTrackInThisRelease;
            }, false);
            if(currentTrackInThisRelease) {
              _currentRelease = _release;
            }
            return _currentRelease;
          }, null);
          if(currentRelease) {
            const nextTrack = currentRelease.tracks.reduce((_nextTrack, _track, index, allTracks) => {
              if(_track.id === this.track.id) {
                const nextIndex = index+1;
                if(allTracks[nextIndex]) {
                  _nextTrack = allTracks[nextIndex];
                }
              }
              return _nextTrack;
            }, null);
            if(nextTrack) {
              nextTrack.play();
            } else {
              const nextRelease = col.releases.reduce((_nextRelease, _release, index, allReleases) => {
                const currentTrackInThisRelease = _release.tracks.reduce((_currentTrackInThisRelease, _track) => {
                  if(_track.id === this.track.id){
                    _currentTrackInThisRelease = true;
                  }
                  return _currentTrackInThisRelease;
                }, false);
                if(currentTrackInThisRelease) {
                  const nextIndex = index+1;
                  if(allReleases[nextIndex]) {
                    _nextRelease = allReleases[nextIndex];
                  }
                }
                return _nextRelease;
              }, null);
              if(nextRelease) {
                nextRelease.tracks[0].play();
              } else {
                alert('there is no next release in this col');
              }
            }
          } else {
            alert('current track not found');
          }
        } else {
          alert('unknown viewMode: '+tab.viewMode);
        }
      }
    }
  }
  youtubeCandidates(q, diggingYoutubeSearchCallback) {
    const fuzzyTester = FuzzySet();
    fuzzyTester.add(q);
    let candidates = [];
    if (q && q.length && q.length > 0) {
      q = S(q).replaceAll(' ', '  ').s;
      q = S(q).replaceAll('(', '').s;
      q = S(q).replaceAll(')', '').s;
      q = S(q).humanize('}', '').s;
      q = q.replace(/[^\w\s]/gi, '');

      const host = 'https://www.googleapis.com';
      const path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
      const url = host + path + q;
      diggingCurl(url, (err, res) => {
        if (res.html) {
          const youtubeData = res.html;
          let firstItem = null;
          if (youtubeData && youtubeData.items && youtubeData.items.length > 0 && youtubeData.items[0]) {
            youtubeData.items.forEach((item) => {
              if (item
                && item.id
                && item.id.kind === 'youtube#video'
                && item.id.videoId
                && item.snippet
                && item.snippet.title) {

                if(firstItem === null){
                  firstItem = { img: '/public/img/sources/youtube.png', title: item.snippet.title, url: 'https://www.youtube.com/watch?v='+item.id.videoId };
                }

                let cleanTitle = item.snippet.title;
                cleanTitle = S(cleanTitle).replaceAll('(', '').s;
                cleanTitle = S(cleanTitle).replaceAll(')', '').s;
                cleanTitle = S(cleanTitle).humanize().s;
                //cleanTitle = cleanTitle.replace(/ *\([^)]*\) */g, "");
                cleanTitle = cleanTitle.replace(/ *\[[^)]*\] */g, "");
                let fuzzyResult = fuzzyTester.get(cleanTitle);
                if(fuzzyResult && fuzzyResult[0] && fuzzyResult[0][0] && fuzzyResult[0][0] > 0.6){
                  // item.snippet.thumbnails.default.url
                  candidates.push({ img: '/public/img/sources/youtube.png', title: item.snippet.title, url: 'https://www.youtube.com/watch?v='+item.id.videoId });
                } else if (fuzzyResult && fuzzyResult[0] && fuzzyResult[0][0]){
                  console.log(item.snippet.title+': '+fuzzyResult[0][0]);
                } else {
                  console.log('Not matching : '+item.snippet.title+' for '+q+'"');
                }
              }
            });

            if(candidates.length === 0 && firstItem){
              let cleanTitle = firstItem.title;
              cleanTitle = S(cleanTitle).replaceAll('(', '').s;
              cleanTitle = S(cleanTitle).replaceAll(')', '').s;
              cleanTitle = S(cleanTitle).humanize().s;
              cleanTitle = cleanTitle.replace(/ *\[[^)]*\] */g, "");
              let fuzzyResult = fuzzyTester.get(cleanTitle);
              if(fuzzyResult && fuzzyResult[0] && fuzzyResult[0][0] && fuzzyResult[0][0] > 0.4){
                candidates.push(firstItem);
              }
            }

            diggingYoutubeSearchCallback(candidates);
          } else {
            diggingYoutubeSearchCallback([]);
          }
        } else {
          diggingYoutubeSearchCallback([]);
        }
      });
    } else {
      diggingYoutubeSearchCallback([]);
    }
  }
  youtube(q, diggingYoutubeSearchCallback) {
    if (q && q.length && q.length > 0) {
      q = S(q).replaceAll('(', '').s;
      q = S(q).replaceAll(')', '').s;
      q = S(q).humanize('}', '').s;
      q = q.replace(/[^\w\s]/gi, '');

      const host = 'https://www.googleapis.com';
      const path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
      const url = host + path + q;
      diggingCurl(url, (err, res) => {
        if (res.html) {
          const allResults = [];

          const youtubeData = res.html;

          if (youtubeData && youtubeData.items && youtubeData.items.length > 0 && youtubeData.items[0]) {
            youtubeData.items.forEach((item) => {
              if (item
                && item.id
                && item.id.kind === 'youtube#video'
                && item.id.videoId
                && item.snippet
                && item.snippet.title) {
                const theTitle = item.snippet.title;
                allResults.push({ title: theTitle, id: item.id.videoId });
              }
            });

            if (allResults.length > 0) {
              const options = {
                /* threshold: 0.4,*/
                keys: ['title', 'id']
              };

              const fuse = new Fuse(allResults, options);
              const fuseResult = fuse.search(q);

              if (fuseResult.length > 0) {
                const fuseUrlBestMatch = fuseResult[0];
                diggingYoutubeSearchCallback(fuseUrlBestMatch);
              } else {
                diggingYoutubeSearchCallback(null);
              }
            } else {
              diggingYoutubeSearchCallback(null);
            }
          } else {
            diggingYoutubeSearchCallback(null);
          }
        } else {
          diggingYoutubeSearchCallback(null);
        }
      });
    } else {
      diggingYoutubeSearchCallback(null);
    }
  }
  trackIsPlaying(track){
    if(this.track && this.track.id && this.track.id === track.id && this.playing) {
      return true;
    } else {
      return false;
    }
  }
  @action
  pause(){
    if(this.track && this.track.pause){
      this.track.pause();
    }
    this.playing = false;
  }
  @action
  stop(){
    if(this.track && this.track.stop){
      this.track.stop();
    }
    this.track = {};
    this.playing = false;
  }
  @action
  initPlayer() {
    this.playing = false;
    this.track = {};
  }
}

const DiggingPlayerModelInstance = new DiggingPlayerModel();
export default DiggingPlayerModelInstance;
