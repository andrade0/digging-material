import { observable, action } from 'mobx';
import md5 from 'md5';
import async from 'async';
import cheerio from 'cheerio';
import moment from 'moment';
import S from 'string';
import DiggingCloudCurl from '../digging/diggingCloudCurl';

class DiggingBlogDataModel {
  @observable news = [];
  @observable artists = [];
  @observable events = [];
  @observable reviews = [];
  today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
  constructor() {
    async.waterfall([
      (cb)=>{
        DiggingCloudCurl('https://www.residentadvisor.net/?ddk='+this.today, (error, result)=>{
          if(result && result.html){
            const $ = cheerio.load(result.html);
            async.eachSeries($('a'), (elt, cbEach)=>{
              if($(elt).attr('href') && $(elt).attr('href').length > 0){
                if($(elt).attr('href').match(/\/news\.aspx[?][i][d][=][0-9]*$/) && this.news.length <= 3) {
                  const url = 'https://www.residentadvisor.net'+$(elt).attr('href');
                  const exist = this.news.reduce((ret, el)=>{
                    if(el.url === url){
                      ret = true;
                    }
                    return ret;
                  }, false);
                  if(!exist){
                    DiggingCloudCurl(url, (e, r)=>{
                      if(r && r.html){
                        const $$ = cheerio.load(r.html);
                        const title = $$('#sectionHead > h1').text();
                        let text = $$('#news-item > div').text();
                        let img = 'https://www.residentadvisor.net'+$$('#news-item > figure > img').attr('src');
                        text = S(text).humanize().s;
                        this.news.push({
                          title,
                          text,
                          url,
                          img,
                          logo: '/public/img/sources/ra2.png',
                          source: 'Resident Advisor',
                          type: 'news'
                        });
                        cbEach();
                      } else {
                        cbEach();
                      }
                    });
                  } else {
                    cbEach();
                  }
                }
                else if($(elt).attr('href').match(/^\/reviews\/[0-9]*$/) && this.reviews.length <= 3){
                  const url = 'https://www.residentadvisor.net'+$(elt).attr('href');
                  const exist = this.reviews.reduce((ret, el)=>{
                    if(el.url === url){
                      ret = true;
                    }
                    return ret;
                  }, false);
                  if(!exist){
                    DiggingCloudCurl(url, (e, r)=>{
                      if(r && r.html){
                        const $$ = cheerio.load(r.html);
                        const title = $$('#sectionHead > h1').text();
                        let text = $$('#review-item .reviewContent').text();
                        let img = 'https://www.residentadvisor.net'+$$('#review-item img.mobile-off').attr('src');
                        text = S(text).humanize().s;
                        this.reviews.push({
                          title,
                          text,
                          url,
                          img,
                          logo: '/public/img/sources/ra2.png',
                          source: 'Resident Advisor',
                          type: 'review'
                        });
                        cbEach();
                      } else {
                        cbEach();
                      }
                    });
                  } else {
                    cbEach();
                  }
                }
                else if($(elt).attr('href').match(/^\/events\/[0-9]*$/) && this.events.length <= 3){
                  const url = 'https://www.residentadvisor.net'+$(elt).attr('href');
                  const exist = this.events.reduce((ret, el)=>{
                    if(el.url === url){
                      ret = true;
                    }
                    return ret;
                  }, false);
                  if(!exist){
                    DiggingCloudCurl(url, (e, r)=>{
                      if(r && r.html){
                        const $$ = cheerio.load(r.html);
                        const title = $$('#sectionHead > h1').text();
                        let text = $$('div#event-item div.left').text();
                        let img = 'https://www.residentadvisor.net'+$$('.flyer > a > img').attr('src');
                        text = S(text).humanize().s;
                        this.events.push({
                          title,
                          text,
                          url,
                          img,
                          logo: '/public/img/sources/ra2.png',
                          source: 'Resident Advisor',
                          type: 'event'
                        });
                        cbEach();
                      } else {
                        cbEach();
                      }
                    });
                  } else {
                    cbEach();
                  }
                }
                else {
                  cbEach();
                }
              } else {
                cbEach();
              }
            }, ()=>{
              cb();
            });
          } else {
            cb();
          }
        });
      },
      (cb)=>{
        DiggingCloudCurl('https://pitchfork.com?ddk='+this.today, (error, result)=>{
          if(result && result.html){
            const $ = cheerio.load(result.html);
            async.eachSeries($('a'), (elt, cbEach)=>{
              if($(elt).attr('href') && $(elt).attr('href').length > 0){
                if($(elt).attr('href').match(/\/news\/.+/) && this.news.length <= 6) {
                  const url = 'https://pitchfork.com'+$(elt).attr('href');
                  const exist = this.news.reduce((ret, el)=>{
                    if(el.url === url){
                      ret = true;
                    }
                    return ret;
                  }, false);
                  if(!exist){
                    DiggingCloudCurl(url, (e, r)=>{
                      if(r && r.html){
                        const $$ = cheerio.load(r.html);
                        const title = $$('.news-detail__article-row > div > header > h1').text();
                        let text = $$('.contents').text();
                        let img = $$('.news-visual-header__img').attr('src');
                        text = S(text).humanize().s;
                        this.news.push({
                          title,
                          text,
                          url,
                          logo: '/public/img/sources/pitchfork.png',
                          img,
                          source: 'Pitchfork',
                          type: 'news'
                        });
                        cbEach();
                      } else {
                        cbEach();
                      }
                    });
                  } else {
                    cbEach();
                  }
                }
                else if($(elt).attr('href').match(/^\/features\/rising\/.+\/$/) && this.artists.length <= 6){
                  const url = 'https://pitchfork.com'+$(elt).attr('href');
                  const exist = this.artists.reduce((ret, el)=>{
                    if(el.url === url){
                      ret = true;
                    }
                    return ret;
                  }, false);
                  if(!exist){
                    DiggingCloudCurl(url, (e, r)=>{
                      if(r.html){
                        const $$ = cheerio.load(r.html);
                        const title = $$('header > div.container-fluid > div > div > div.fts-header__title-container > h1').text();
                        let text = $$('#feature-detail-container article div.text-block div.contents').text();
                        let img = $$('header > div.fts-header--visual.caption-container > img').attr('src');
                        text = S(text).humanize().s;
                        this.artists.push({
                          title,
                          text,
                          url,
                          img,
                          logo: '/public/img/sources/pitchfork.png',
                          source: 'Pitchfork',
                          type: 'Artist'
                        });
                        cbEach();
                      } else {
                        cbEach();
                      }
                    });
                  } else {
                    cbEach();
                  }
                }
                else {
                  cbEach();
                }
              } else {
                cbEach();
              }
            }, ()=>{
              cb();
            });
          } else {
            cb();
          }
        });
      }
    ], ()=>{
      /*console.log(this.news);
      console.log(this.events);
      console.log(this.reviews);
      console.log(this.artists);*/
    });
  }

}

const _DiggingBlogDataModel = new DiggingBlogDataModel();
export default _DiggingBlogDataModel;
