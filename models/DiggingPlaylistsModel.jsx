import { observable, action } from 'mobx';
import md5 from 'md5';
import _ from 'underscore';
import firebase from 'firebase';
import fire from '../models/fire';
import TrackModel from './TrackModel';
import DiggingPlaylistModel from './DiggingPlaylistModel';

class DiggingPlaylistsModel {
  @observable playlists = [];
  @observable activePlaylist = null;
  constructor() {

  }
  @action
  addPlaylist(playlist) {
    const exists = _.where(this.playlists, {title: playlist.title});
    if(exists.length > 0 && playlist.id !== exists[0].id) {
      alert('You already a playlist with that name');
    } else {
      const trackForFirebase = {};
      for(const index in playlist) {
        if(playlist[index]){
          trackForFirebase[index] = playlist[index];
        }
      }
      const playlistsListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists');
      const newPlaylistRef = playlistsListRef.push();
      trackForFirebase.key = newPlaylistRef.key;
      playlist.key = trackForFirebase.key;
      if(trackForFirebase.ref){
        delete trackForFirebase.ref;
      }
      newPlaylistRef.set(trackForFirebase).then(()=>{
        this.playlists.push(new DiggingPlaylistModel(trackForFirebase));
      });
    }
  }
  @action
  addPlaylists(playlists) {
    const newPlaylists = playlists.map((playlist)=>{
      return new DiggingPlaylistModel(playlist);
    });

    this.playlists = newPlaylists;
  }
  @action
  setActivePlaylists(playlist) {
    this.activePlaylist = playlist;
  }
  @action
  removePlaylist(playlist) {
    if(playlist.key){
      firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists/'+playlist.key).remove();
      this.playlists = this.playlists.reduce((current, _playlist) => {
        if (playlist.title !== _playlist.title) {
          current.push(_playlist);
        }
        return current;
      }, []);
    } else {
      alert('track in liked but as no key');
    }
  }
}

const _DiggingPlaylistsModel = new DiggingPlaylistsModel();
export default _DiggingPlaylistsModel;