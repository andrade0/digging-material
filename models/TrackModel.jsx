import { observable, action } from 'mobx';
import md5 from 'md5';
import moment from 'moment';
import DiggingLikesModel from './DiggingLikesModel';
import DiggingListenedModel from './DiggingListenedModel';
import DiggingPlayerModel from './DiggingPlayerModel';

export default class TrackModel {
  id;
  artist;
  @observable key;
  title;
  genre;
  cover;
  logo;
  sample;
  source;
  type = 'track';
  name;
  listened;
  @observable liked;
  @observable playing = false;
  @observable ref = null;

  constructor(track) {
    
    for(const index in track) {
      if(track[index] && index !== 'listened'){
        this[index] = track[index];
      }
    }

    if(!this.liked && DiggingLikesModel.tracks && DiggingLikesModel.tracks.length > 0){
      this.liked = DiggingLikesModel.tracks.reduce((_liked, t) => {
        if(t.artist === track.artist && t.title === track.title){
          _liked = true;
        }
        return _liked;
      }, false);
    }

    this.name = this.title;
    this.id = md5(track.artist + track.title + Math.random());

    this.playing = this.isPlaying();
  }
  @action
  play() {
    DiggingPlayerModel.play(this);
    if(this.colType && this.colType === 'news'){
      this.listened = moment().format();
      DiggingListenedModel.addTrack(this);
    }
    this.playing = true;
  }
  @action
  pause() {
    DiggingPlayerModel.playing = false;
    this.playing = false;
  }
  @action
  stop() {
    DiggingPlayerModel.playing = false;
    this.playing = false;
  }
  @action
  isPlaying(){
    DiggingPlayerModel.trackIsPlaying(this);
  }
  @action
  like() {
    const exists = this.isInLikes();
    if(!exists){
      DiggingLikesModel.addTrack(this, ()=>{
        this.liked = true;
      });
    } else {
      alert('track exists already in likes');
    }
  }
  @action
  dislike() {
    DiggingLikesModel.removeTrack(this, ()=>{
      this.liked = false;
    });
  }
  @action
  isInLikes() {
    const response = DiggingLikesModel.tracks.reduce((_exists, _track) => {
      if (
        this.artist
        && this.title
        && _track.artist
        && _track.title
        && this.artist === _track.artist
        && this.title === _track.title
      ) {
        if (!this.key) {
          this.key = _track.key;
        }
        _exists = true;
      }
      return _exists;
    }, false);
    this.isInlikes = response;
    return response;
  }
}
