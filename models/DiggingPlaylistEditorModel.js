import { observable, action, computed } from 'mobx';
import md5 from 'md5';

class DiggingPlaylistEditorModel {
  @observable open = false;
  @observable playlist = null;
  @observable title = '';
  @observable visibility = 'public';
  constructor() {

  }
}

const _DiggingPlaylistEditorModel = new DiggingPlaylistEditorModel();
export default _DiggingPlaylistEditorModel;

