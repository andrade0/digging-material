import { observable, action } from 'mobx';
import md5 from 'md5';
import _ from 'underscore';
import fire from '../models/fire';
import firebase from 'firebase';
import TrackModel from './TrackModel';
import DiggingPlaylistsModel from './DiggingPlaylistsModel';
import DiggingModel from './DiggingModel';

export default class DiggingPlaylistModel {
  id;
  @observable title;
  @observable tracks = [];
  @observable weight;
  accepts = [];
  @observable key;
  lastDroppedItem = null;
  constructor(playlist) {
    this.id = md5(playlist.title);
    this.title = playlist.title;
    if(playlist.key) {
      this.key = playlist.key;
    }
    this.accepts = ['track'];
    this.lastDroppedItem = null;
    if(!playlist.weight) {
      playlist.weight = 0;
    }
    this.weight = playlist.weight;

    if(playlist.tracks && Object.values(playlist.tracks).length > 0) {
      this.addTracks(Object.values(playlist.tracks));
    }
  }
  @action
  addTrack(track) {
    track.playlistId = this.id;
    const exists = _.where(this.tracks, {title: track.track.title, artist: track.track.artist});
    if(exists.length <= 0){
      const trackForFirebase = {};
      for(const index in track.track) {
        if(track.track[index]){
          trackForFirebase[index] = track.track[index];
        }
      }
      const playlistTracksListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists/'+this.key+'/tracks');
      const newTrackRef = playlistTracksListRef.push();
      trackForFirebase.key = newTrackRef.key;
      track.track.key = trackForFirebase.key;
      if(trackForFirebase.ref){
        delete trackForFirebase.ref;
      }
      newTrackRef.set(trackForFirebase).then(()=>{
        this.tracks.push(new TrackModel(trackForFirebase));
      });
    } else {
      alert('Track already in playlist');
    }
  }
  @action
  addTracks(tracks) {
    this.tracks = [...this.tracks, ...tracks.map((track)=>{
      track.playlistId = this.id;
      return new TrackModel(track);
    })];
  }
  @action
  removeTrack(track) {
    if(track.key && this.key){
      firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists/'+this.key+'/tracks/'+track.key).remove();
      this.tracks = this.tracks.reduce((current, _track) => {
        if (track.id !== _track.id) {
          current.push(_track);
        }
        return current;
      }, []);
      track.key = null;
    } else {
      alert('Track don\'t have key in playlists or track');
    }
  }
  @action
  rename(title){
    const exists = _.where(DiggingPlaylistsModel.playlists, {title: this.title});
    if(exists.length > 0 && this.key !== exists[0].key) {
      alert('You already a playlist with that name');
      return false;
    } else {
      firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists/'+this.key).set({title: title});
      this.title = title;
      return true;
    }
  }
  @action
  changeWeight(weight) {
    firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/playlists/'+this.key).set({weight: weight});
    this.weight = weight;
    return true;
  }
  @action
  remove() {
    DiggingPlaylistsModel.removePlaylist(this);
  }
  handleDrop(index, item) {
    this.addTrack(item);
  }
}
