import { observable, action } from 'mobx';

export default class DiggingScraping {
  @observable name;
  @observable progress = 0;
  @observable state = 0;
  constructor(name) {
    this.name = name;
  }
  @action
  updateProgress(progress) {
    this.progress = progress;
  }
}
