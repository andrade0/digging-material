import { observable, action } from 'mobx';
import md5 from 'md5';
import DiggingLikesModel from './DiggingLikesModel';
import DiggingPlaylistsModel from './DiggingPlaylistsModel';
import DiggingPlayerModel from './DiggingPlayerModel';
import TrackModel from './TrackModel';
import sourcesObjects from '../digging/sources';
import DiggingModel from './DiggingModel';


class DiggingSuggestModel {
  @observable track = null;
  @observable youtubeId = null;
  @observable explanation = null;

  constructor(track) {

  }

  @action
  suggest(){

    DiggingModel.loading = true;

    DiggingPlayerModel.playing = false;
    DiggingPlayerModel.playingTrack = null;
    DiggingPlayerModel.tracks = [];
    DiggingPlayerModel.track = null;

    let explanation = 'This track was suggested to you ';

    let artists = DiggingLikesModel.tracks.reduce((c, t, i, a)=>{
      if(t.artist && c.indexOf(t.artist) < 0){
        c.push(t.artist);
      }
      return c;
    }, []);

    const artistsPlaylists = DiggingPlaylistsModel.playlists.reduce((c, p, i, a)=>{
      let artistsFromPlaylist = [];
      if(p && p.tracks){
        artistsFromPlaylist = p.tracks.reduce((cc, t, ii, aa)=>{
          if(t.artist && cc.indexOf(t.artist) < 0){
            cc.push(t.artist);
          }
          return cc;
        }, []);
      }
      c = [...c, ...artistsFromPlaylist];
      return c;
    }, []);

    artists = [...artists, ...artistsPlaylists];

    let labels = DiggingLikesModel.tracks.reduce((c, t, i, a)=>{
      if(t.label && c.indexOf(t.label) < 0){
        c.push({label: t.label, track: t});
      }
      return c;
    }, []);

    const labelsPlaylists = DiggingPlaylistsModel.playlists.reduce((c, p, i, a)=>{
      let labelsFromPlaylist = [];
      if(p && p.tracks){
        labelsFromPlaylist = p.tracks.reduce((cc, t, ii, aa)=>{
          if(t.label && cc.indexOf(t.label) < 0){
            cc.push({label: t.label, track: t});
          }
          return cc;
        }, []);
      }
      c = [...c, ...labelsFromPlaylist];
      return c;
    }, []);

    labels = [...labels, ...labelsPlaylists];


    const myArray = [];
    myArray.push('artist');
    myArray.push('label');
    const type = myArray[Math.floor(Math.random() * myArray.length)];

    let suggestionType = null;
    if(type === 'artist') {
      explanation += 'because you like the artist "';
      const artist = artists[Math.floor(Math.random() * artists.length)];
      explanation += artist+'" ';
      suggestionType = {type: type, subject: artist};
    } else if (type === 'label') {
      explanation += 'because you like the label "';
      const labelObject = labels[Math.floor(Math.random() * labels.length)];
      explanation += +labelObject.label+'" who released the track "'+labelObject.track.artist+' - '+labelObject.track.title+'" that you like ';
      suggestionType = {type: type, subject: labelObject.label};
    }

    const discogs = sourcesObjects['discogs'];

    if(suggestionType.type === 'label') {
      discogs.label(suggestionType.subject, null, (pages)=>{
        const page = pages[Math.floor(Math.random() * pages.length)];
        if(page && page.url){
          discogs.label1(suggestionType.subject, page, (release)=>{
            if(release && release.tracks && release.tracks.length > 0) {
              this.track = new TrackModel(release.tracks[Math.floor(Math.random() * release.tracks.length)]);
              DiggingPlayerModel.youtube(this.track.artist+' '+this.track.title, (youtubeResult)=>{
                if(youtubeResult && youtubeResult.id) {
                  this.youtubeId = youtubeResult.id;
                  DiggingModel.loading = false;
                  explanation += 'and this track is from the same label';
                  this.explanation = explanation;
                } else {
                  this.suggest();
                }
              });
            }
          });
        } else {
          this.suggest();
        }
      });
    } else if(suggestionType.type === 'artist') {
      discogs.artist(suggestionType.subject, null, (pages)=>{
        const page = pages[Math.floor(Math.random() * pages.length)];
        if(page && page.url){
          discogs.artist1(suggestionType.subject, page, (release)=>{
            if(release && release.tracks && release.tracks.length > 0) {
              this.track = new TrackModel(release.tracks[Math.floor(Math.random() * release.tracks.length)]);
              DiggingPlayerModel.youtube(this.track.artist+' '+this.track.title, (youtubeResult)=>{
                if(youtubeResult && youtubeResult.id) {
                  this.youtubeId = youtubeResult.id;
                  DiggingModel.loading = false;
                  explanation += 'and this track is from this artist as well';
                  this.explanation = explanation;
                } else {
                  this.suggest();
                }
              });
            }
          });
        } else {
          this.suggest();
        }
      });
    }
  }
}

const _DiggingSuggestModel = new DiggingSuggestModel();
export default _DiggingSuggestModel;