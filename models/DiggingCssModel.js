import { observable, action, computed } from 'mobx';
import md5 from 'md5';
import jQuery from 'jquery';
import fire from '../models/fire';
import firebase from 'firebase';
import TrackModel from './TrackModel';
import DiggingPlayerModel from './DiggingPlayerModel';

class DiggingCssModel {
  @observable tablesHeight = 0;
  @observable loadingBottomPosition = 0;
  @observable windowHeight = 0;
  @observable windowWith = 0;
  @observable sidebarHeight = 0;
  constructor() {
    jQuery( window ).resize(() => {
      this.resizeApp();
    });
    this.resizeApp();
  }

  resizeApp(){
    this.windowHeight = jQuery( window ).height();
    this.windowWith = jQuery( window ).width();


    if(DiggingPlayerModel && DiggingPlayerModel.track && DiggingPlayerModel.track.title){
      this.loadingBottomPosition = 40;
    } else {
      this.loadingBottomPosition = 0;
    }

    this.sidebarHeight = this.windowHeight;

    const menu = window.location.href.split('/').reduce((r, c, i, a)=>{
      if(i>2){
        r.push(c);
      }
      return r;
    }, [])[0];

    if(menu === 'likes' || menu === 'playlist' || menu === 'suggest' || menu === 'settings'){
      this.tablesHeight = this.windowHeight-55;
    } else {
      this.tablesHeight = this.windowHeight-105;
    }

  }

}

const _DiggingCssModel = new DiggingCssModel();
export default _DiggingCssModel;

