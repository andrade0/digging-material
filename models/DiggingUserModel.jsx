import { observable, action, computed, intercept } from 'mobx';
import jQuery from 'jquery';
import async from 'async';
import md5 from 'md5';
import moment from 'moment';
import _ from 'underscore';
import DiggingLikesModel from './DiggingLikesModel';
import DiggingListenedModel from './DiggingListenedModel';
import DiggingPlaylistsModel from './DiggingPlaylistsModel';
import sourcesObjects from '../digging/sources';
import config from '../config';
const Client = require('node-rest-client').Client;
const client = new Client();
import fire from '../models/fire';
import firebase from 'firebase';

export default class DiggingUserModel {
  @observable userId;
  @observable username;
  @observable email;
  @observable picture;
  @observable accessToken;
  @observable likes = null;
  @observable playlists = [];
  @observable sources = [];
  @observable newsPreferPreview;
  @observable searchPreferPreview;
  @observable newsPreferReleaseViewMode;
  @observable searchPreferReleaseViewMode;
  @observable youtubeChannels = [];
  constructor(){
    intercept(this, "newsPreferPreview", change => {
      firebase.database().ref('users/' + this.userId +'/newsPreferPreview').set(change.newValue);
      return change;
    });

    intercept(this, "searchPreferPreview", change => {
      firebase.database().ref('users/' + this.userId +'/searchPreferPreview').set(change.newValue);
      return change;
    });

    intercept(this, "newsPreferReleaseViewMode", change => {
      firebase.database().ref('users/' + this.userId +'/newsPreferReleaseViewMode').set(change.newValue);
      return change;
    });

    intercept(this, "searchPreferReleaseViewMode", change => {
      firebase.database().ref('users/' + this.userId +'/searchPreferReleaseViewMode').set(change.newValue);
      return change;
    });
  }
  getSourceStatus(sourceName, type) {
    const sourceExistsInUser = _.where(this.sources, {name: sourceName, type: type});
    if(sourceExistsInUser.length > 0) {
      return sourceExistsInUser[0].value;
    } else {
      return null;
    }
  }
  updateSourceStatus(sourceName, type, value) {
    this.sources = this.sources.map((source) => {
      if(source.name === sourceName && source.type === type)
      {
        source.value = value;
        firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/sources/'+source.key+'').set({name: source.name, value: source.value, type: source.type});
      }
      return source;
    });
  }
  @action
  addLike(track) {
    this.likes.push(new DiggingLikeModel(track.id, track.artist, track.title, track.sample, track.genre));
  }
  @action
  removeLike(track) {
    this.likes.reduce((current, like) => {
      if (like.id !== track.id) {
        current.push(like);
      }
      return current;
    }, []);
  }
  @action
  addYoutubeChannel(youtubeChannelUrl, name) {
    const youtubeChannelListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/youtubeChannels');
    const newYoutubeChannelRef = youtubeChannelListRef.push();
    const newYoutubeChannelRefKey = newYoutubeChannelRef.key;
    newYoutubeChannelRef.set({url: youtubeChannelUrl, name: name}).then(()=>{
      this.youtubeChannels.push({key: newYoutubeChannelRefKey, url: youtubeChannelUrl, name: name});
    });
  }
  @action
  removeYoutubeChannel(youtubeChannelUrlKey) {
    this.youtubeChannels = this.youtubeChannels.reduce((current, _youtubeChannel) => {
      if (_youtubeChannel.key !== youtubeChannelUrlKey) {
        current.push(_youtubeChannel);
      }
      return current;
    }, []);
    firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/youtubeChannels/'+youtubeChannelUrlKey).remove();
  }
  @action
  addPlaylist(title) {
    this.playlists.push(new DiggingPlaylistModel(title));
  }
  @action
  removePlaylist(playlist) {
    this.playlists.reduce((current, _playlist) => {
      if (_playlist.id !== playlist.id) {
        current.push(_playlist);
      }
      return current;
    }, []);
  }
  loadUserData(loginCb){
    this.username =firebase.auth().currentUser.displayName;
    this.picture = firebase.auth().currentUser.photoURL;
    this.email = firebase.auth().currentUser.email;
    this.userId = firebase.auth().currentUser.uid;

    async.parallel([
      (settingCb)=>{
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/newsPreferPreview').once('value').then((snapshot) => {
          if(snapshot.val()){
            this[snapshot.key] = snapshot.val();
          } else {
            this[snapshot.key] = false;
          }
          settingCb(null);
        });
      },
      (settingCb)=>{
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/searchPreferPreview').once('value').then((snapshot) => {
          if(snapshot.val()){
            this[snapshot.key] = snapshot.val();
          } else {
            this[snapshot.key] = false;
          }
          settingCb(null);
        });
      },
      (settingCb)=>{
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/newsPreferReleaseViewMode').once('value').then((snapshot) => {
          if(snapshot.val()){
            this[snapshot.key] = snapshot.val();
          } else {
            this[snapshot.key] = false;
          }
          settingCb(null);
        });
      },
      (settingCb)=>{
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/searchPreferReleaseViewMode').once('value').then((snapshot) => {
          if(snapshot.val()){
            this[snapshot.key] = snapshot.val();
          } else {
            this[snapshot.key] = false;
          }
          settingCb(null);
        });
      },
      (getSourceCb) => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/sources').once('value').then((snapshot) => {
          let userSources = [];
          if(snapshot.val()){
            userSources = snapshot.val();
          }
          async.each(['news', 'search'], (type, cbEachType) => {
            async.each(sourcesObjects, (sourcesObject, cbEachSource) => {
              const sourceExistsInUserKey = Object.keys(userSources).reduce((key, k, i, a)=>{
                const s = userSources[k];
                if(s.name === sourcesObject.value && s.type === type){
                  key = k;
                }
                return key;
              }, null);
              if(sourceExistsInUserKey === null)
              {
                const sourceListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/sources');
                const newSourceRef = sourceListRef.push();
                const newSourceRefKey = newSourceRef.key;
                newSourceRef.set({name: sourcesObject.value, value: true, type: type}).then(()=>{
                  this.sources.push({key: newSourceRefKey, name: sourcesObject.value, value: true, type: type});
                  cbEachSource(null);
                });
              }
              else
              {
                this.sources.push({ key: sourceExistsInUserKey, name: userSources[sourceExistsInUserKey].name, value: userSources[sourceExistsInUserKey].value, type: type });
                cbEachSource(null);
              }
            }, () => {
              cbEachType(null);
            });
          },  () => {
            getSourceCb(null);
          });

        });
      },
      (getYoutubeChannelsCB) => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/youtubeChannels').once('value').then((snapshot) => {
          snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            childData.key = childKey;
            this.youtubeChannels.push(childData);
          });
          getYoutubeChannelsCB(null);
        });
      },
      (getLikesCB) => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/likes').once('value').then((snapshot) => {
          const initialTracks = [];
          snapshot.forEach(function(childSnapshot) {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            childData.key = childKey;
            childData.isInlikes = true;
            initialTracks.push(childData);
          });
          DiggingLikesModel.addTracks(initialTracks);
          getLikesCB(null);
        });
      },
      (getListenedCB) => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/listened').once('value').then((snapshot) => {
          let initialTracks = [];
          snapshot.forEach(function(childSnapshot) {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            childData.key = childKey;
            initialTracks.push(childData);
          });

          const oneMonthAgo = moment().subtract(30, 'days');

          initialTracks = initialTracks.reduce((list, t)=>{
            if(t.listened){
              const m = moment(t.listened);
              if (m < oneMonthAgo){ //expired
                firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/listened/'+t.key).remove();
              } else {
                list.push(t)
              }
            }
            return list;
          }, []);

          DiggingListenedModel.addTracks(initialTracks);
          getListenedCB(null);
        });
      },
      (getPlaylistsCB) => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/playlists').once('value').then((snapshot) => {
          const initialPlaylists = [];
          snapshot.forEach(function(childSnapshot) {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            childData.key = childKey;
            initialPlaylists.push(childData);
          });
          DiggingPlaylistsModel.addPlaylists(initialPlaylists);
          getPlaylistsCB(null);
        });
      }
    ], () => {
      loginCb();
    });
  }

  @action
  login(loginCb) {
    const provider = new firebase.auth.FacebookAuthProvider();
    fire.auth().signInWithPopup(provider).then((response) => {
      if(response){
        const facebookProfil = response.additionalUserInfo.profile;
        for(const index in facebookProfil) {
          firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/' + index).set(facebookProfil[index])
        }
        this.loadUserData(loginCb);
      }
    }).catch((error)=>{
      console.log(error);
    });
  }
  @action
  logout(){
    fire.auth().signOut().then(()=> {
      this.userId = null;
    }, function(error) {
      // An error happened.
    });

  }
  test(){
    const arrayRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/testarray');
    const newItem = arrayRef.push();
    const obj = {
      key: newItem.key,
      name: 'fdfsd'
    };
    arrayRef.set(obj).then((res)=>{
      console.log(res)
    }).catch((error)=>{
      console.log(error)
    });
  }
}
