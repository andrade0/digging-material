import { observable, action, intercept } from 'mobx';
import jQuery from 'jquery';
import DiggingCssModel from '../models/DiggingCssModel';
import DigginUser from '../models/DiggingUser';
import sourcesObjects from '../digging/sources';


import DiggingTabModel from './DiggingTabModel';

class DiggingModel {
  @observable title;
  @observable tabs = [];
  @observable activeTab = 0;
  @observable loading = false;
  @observable droppedBoxNames = []; // to remove ?
  @observable notifications = [];
  @observable loggingCalled = false;
  @observable extentionIsActive = false;
  @observable startupLoaderProgress = 0;

  resize(){
    const documentWith = jQuery( document ).width();
    const tableWidth = documentWith-240;
    //jQuery('.tableOfTracks table').css('width', tableWidth+'px');
  }

  constructor() {
    intercept(this, "activeTab", change => {
      const activeTabObject = this.tabs.reduce((_activeTabObject, _tab, index) => {
        if(index === change.newValue) {
          _activeTabObject = _tab;
        }
        return _activeTabObject;
      }, null);

      if(activeTabObject){
        let query = activeTabObject.query;
        if(!activeTabObject.query) {
          query = 'news';
        }
        const url = `/${query}/${activeTabObject.type}/${activeTabObject.sources.join(',')}/${activeTabObject.viewMode}`;
        window.history.pushState(activeTabObject.title, activeTabObject.title, url);
      }
      return change;
    });
  }

  @action
  addTab(title, type, query, viewMode, sources) {

    if(viewMode === null){
      viewMode = 'track';
      if(DigginUser.searchPreferReleaseViewMode) {
        viewMode = 'release';
      } else {
        viewMode = 'track';
      }
    }

    if(sources === null){
      const sourceType = 'search';
      sources = DigginUser.sources.reduce((_sources, sourceItem) => {
        if(sourceItem.type === sourceType && sourceItem.value === true)
        {
          const sourceObject = sourcesObjects[sourceItem.name];
          const testType = type;
          if(sourceObject[testType]) {
            _sources.push(sourceItem.name);
          }
        }
        return _sources;
      }, []);
    }

    const tab = new DiggingTabModel(title, type, query, viewMode, sources);
    this.tabs.push(tab);
    this.activeTab = this.tabs.reduce((activeTab, tab, index) => {
      if(tab.title === title) {
        activeTab = index;
      }
      return activeTab;
    }, 0);
    DiggingCssModel.resizeApp();
    return tab;
  }
  @action
  removeTab(_tab) {
    this.tabs = this.tabs.reduce((current, tab) => {
      if (tab.id !== _tab.id) {
        current.push(tab);
      }
      return current;
    }, []);
    this.activeTab = this.tabs.reduce((activeTab, tab, index) => {
      return index+1;
    }, 0);
  }
  tabExists(title) {
    return this.tabs.reduce((exists, tab) => {
      if(tab.title === title) {
        exists = true;
      }
      return exists;
    }, false);
  }
  setActive(title) {
    this.activeTab = this.tabs.reduce((activeTab, tab, index) => {
      if(tab.title === title) {
        activeTab = index;
      }
      return activeTab;
    }, 0);
  }
  getActiveTab() {
    return this.tabs.reduce((activeTab, tab, index) => {
      if(index === this.activeTab) {
        activeTab = tab;
      }
      return activeTab;
    }, null);
  }
}

const diggingModel = new DiggingModel();

export default diggingModel;
