import { observable, action, computed } from 'mobx';
import md5 from 'md5';
import fire from '../models/fire';
import firebase from 'firebase';
import TrackModel from './TrackModel';

class DiggingTimelineModel {
  @observable items = [];
  constructor() {
  }
  @action
  addItem(type, obj) {
    const TimelineRef = firebase.database().ref('timeline');
    const newTimeLineEvent = TimelineRef.push();
    const objForTimeline = {key: newTimeLineEvent.key, object: trackForFirebase, actionType: 'Like', user: firebase.auth().currentUser};
    newTimeLineEvent.set(objForTimeline);
  }
}

const _DiggingTimelineModel = new DiggingTimelineModel();
export default _DiggingTimelineModel;

