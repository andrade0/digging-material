import { observable, action, computed } from 'mobx';
import md5 from 'md5';
import fire from '../models/fire';
import firebase from 'firebase';
import TrackModel from './TrackModel';
import DiggingPlayerModel from './DiggingPlayerModel';

class DiggingListenedModel {
  @observable tracks = [];
  constructor() {
  }
  @action
  addTrack(track) {
    const exists = this.isInLikes(track);
    if(!exists){
      const trackForFirebase = {};
      for(const index in track) {
        if(track[index] && index !== 'key'){
          trackForFirebase[index] = track[index];
        }
      }
      const listenedslListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/listened');
      const newTrackRef = listenedslListRef.push();
      trackForFirebase.key = newTrackRef.key;
      track.key = newTrackRef.key;
      if(trackForFirebase.ref){
        delete trackForFirebase.ref;
      }
      delete trackForFirebase.ref;
      newTrackRef.set(trackForFirebase).then(()=>{
        this.tracks = [...this.tracks, new TrackModel(trackForFirebase)];
      });
    }
  }
  @action
  removeTrack(track) {
    if(track.key){
      firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/listened/'+track.key).remove();
      this.tracks = this.tracks.reduce((_tracks, _track)=>{
        if(track.artist && track.title && _track.artist && _track.title && track.artist === _track.artist && track.title === _track.title) {
        } else {
          _tracks.push(_track);
        }
        return _tracks;
      }, []);
      track.key = null;
    } else {
      alert('track in listened but as no key');
    }
  }
  @action
  addTracks(tracks) {
    tracks = tracks.map((track)=>{
      return new TrackModel(track);
    });
    this.tracks = tracks;
  }
  isInLikes(track){
    return this.tracks.reduce((_exists, _track)=>{
      if(
        track.artist
        && track.title
        && _track.artist
        && _track.title
        && track.artist === _track.artist
        && track.title === _track.title
      ) {
        _exists = true;
      }
      return _exists;
    }, false);
  }
}

const _DiggingListenedModel = new DiggingListenedModel();
export default _DiggingListenedModel;

