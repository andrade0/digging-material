import { observable, action, computed } from 'mobx';
import md5 from 'md5';
import fire from '../models/fire';
import firebase from 'firebase';
import TrackModel from './TrackModel';
import DiggingPlayerModel from './DiggingPlayerModel';

class DiggingLikesModel {
  @observable tracks = [];
  constructor() {
  }
  @action
  addTrack(track, cbAddTrack) {
    track.inLikesTab = true;
    const exists = this.isInLikes(track);
    if(!exists){
      const trackForFirebase = {};
      for(const index in track) {
        if(track[index] && index !== 'key'){
          trackForFirebase[index] = track[index];
        }
      }
      delete trackForFirebase.ref;
      const likeslListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/likes');
      const newTrackRef = likeslListRef.push();
      trackForFirebase.key = newTrackRef.key;
      track.key = newTrackRef.key;
      newTrackRef.set(trackForFirebase).then(()=>{
        this.tracks = [...this.tracks, new TrackModel(trackForFirebase)];
      });
      cbAddTrack();
    } else {
      alert('track exists already in likes');
    }
  }
  @action
  removeTrack(track, cbAddTrack) {
    if(track.key){
      firebase.database().ref('users/' + firebase.auth().currentUser.uid +'/likes/'+track.key).remove();
      this.tracks = this.tracks.reduce((_tracks, _track)=>{
        if(track.artist && track.title && _track.artist && _track.title && track.artist === _track.artist && track.title === _track.title) {
        } else {
          _tracks.push(_track);
        }
        return _tracks;
      }, []);
      track.key = null;
      cbAddTrack();
    } else {
      alert('track in liked but as no key');
    }
  }
  @action
  addTracks(tracks) {
    tracks = tracks.map((track)=>{
      track.inLikesTab = true;
      const t = new TrackModel(track);
      t.liked = true;
      return t;
    });
    this.tracks = tracks;
  }
  isInLikes(track){
    return this.tracks.reduce((_exists, _track)=>{
      if(
        track.artist
        && track.title
        && _track.artist
        && _track.title
        && track.artist === _track.artist
        && track.title === _track.title
      ) {
        _exists = true;
      }
      return _exists;
    }, false);
  }
}

const _DiggingLikesModel = new DiggingLikesModel();
export default _DiggingLikesModel;

