import firebase from 'firebase'
require('firebase/firestore');

const config = {
  apiKey: "AIzaSyC5ANvw3CCC-feGY0OxP48-eqIYC6nZWD4",
  authDomain: "digging-903.firebaseapp.com",
  databaseURL: "https://digging-903.firebaseio.com",
  projectId: "digging-903",
  storageBucket: "digging-903.appspot.com",
  messagingSenderId: "701504995855"
};


const fire = firebase.initializeApp(config);

export default fire;