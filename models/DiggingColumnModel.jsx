import { observable, action } from 'mobx';
import md5 from 'md5';
import _ from 'underscore';

import ReleaseModel from './ReleaseModel';
import TrackModel from './TrackModel';


export default class DiggingColumnModel {
  id;
  @observable title;
  matchString;
  @observable releases = [];
  allTracks = [];
  viewMode = 'tracks';
  // @observable lazyTracks = [];
  @observable tracks = [];
  @observable links = [];
  constructor(title, matchString, viewMode) {
    this.id = md5(title);
    this.title = title;
    this.matchString = matchString;
    this.viewMode = viewMode;
  }
  @action
  addRelease(release) {
    if(release && release.tracks && release.tracks.length > 0) {
      this.releases.push(new ReleaseModel(release));
    }
  }
  @action
  addReleases(releases) {
    const newReleases = [...this.releases, ...releases].reduce((allReleases, release) => {
      if(release && release.tracks && release.tracks.length > 0) {
        const exists = _.where(allReleases, {artist: release.artist, title: release.title, label: release.label });
        if(exists.length === 0) {
          allReleases.push(new ReleaseModel(release));
        }
      }
      return allReleases;
    }, []);
    this.releases = newReleases;
  }
  @action
  addTrack(track) {

    if(track && (track.artist || track.title) && listened === false){
      this.tracks.push(new TrackModel(track));
    }
  }
  @action
  addTracks(tracks) {
    const newTracks = [...this.tracks, ...tracks].reduce((allTracks, track) => {
      if(track && (track.artist || track.title)){
        const exists = _.where(allTracks, {artist: track.artist, title: track.title, label: track.label });
        if(exists.length === 0) {
          allTracks.push(new TrackModel(track));
        }
      }
      return allTracks;
    }, []);
    this.tracks = newTracks;
  }
  @action
  addLink(Link) {
    this.links.push(new TrackModel(Link));
  }
  @action
  addLinks(links) {
    const newLinks = [...this.links, ...links].reduce((allLinks, link) => {
    const exists = _.where(allLinks, {name: link.name});
    if(exists.length) {
      allLinks = allLinks.map((l) => {
        if(l.name === exists[0].name) {
          if(l.tracks) {
            l.tracks = [...l.tracks, ...exists[0].tracks];
          } else if (l.releases) {
            l.releases = [...l.releases, ...exists[0].releases];
          }
        }
        return l;
      }, []);
    } else {
      allLinks.push(link);
    }
    return allLinks;
    }, []);
    this.links = newLinks;
  }
  @action
  injectTracks() {
     const tracks = [...this.tracks, ...this.allTracks];
     if (tracks.length > 0) {
      this.tracks = tracks;
     }
  }
}
