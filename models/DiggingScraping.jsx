import { observable, action } from 'mobx';
import jQuery from 'jquery';
import sourcesObjects from '../digging/sources';
import DiggingScrapingSource from './DiggingScrapingSource';

export default class DiggingScraping {
  @observable sources = [];
  @observable progress = 0;
  @observable state = 0;
  @action
  addSource(sourceName) {
    const s = new DiggingScrapingSource(sourceName);
    this.sources.push(s);
    return s;
  }
  @action
  removeSource(sourceName) {
    this.sources.reduce((current, sn) => {
      if (sn !== sourceName) {
        current.push(sn);
      }
      return current;
    }, []);
  }
}
