import { observable, action } from 'mobx';
import md5 from 'md5';
import _ from 'underscore';
import async from 'async';
import sourcesObjects from '../digging/sources';

import DiggingColumnModel from './DiggingColumnModel';
import DiggingUser from './DiggingUser';
import ReleaseModel from './ReleaseModel';
import DiggingPlaylistsModel from './DiggingPlaylistsModel';
import DiggingLikesModel from './DiggingLikesModel';

export default class DiggingTabModel {
  id;
  title;
  sources = [];
  query;
  type;
  viewMode;
  @observable progress = 0;
  @observable sourcesProgress = [];
  @observable columns = [];
  @observable activeColumn = 0;
  constructor(title, type, query, viewMode, sources) {
    this.id = md5(title);
    this.type = type;
    this.title = title;
    this.query = query;
    this.viewMode = viewMode;
    this.sources = sources;
    this.sourcesProgress = this.sources.map((s) => {
      return {
        source: sourcesObjects[s],
        progress: 0
      }
    });
    const injectMethod = (this.viewMode === 'release')?'addReleases':'addTracks';
    let YoutubeColumn = null;
    let HouseColumn = null;
    let TechnoColumn = null;
    let MinimalColumn = null;
    let ElectroColumn = null;
    let DiscoColumn = null;
    let RockColumn = null;
    let HipHopColumn = null;
    let OtherColumn = null;
    switch (this.type) {
      case 'news':
        if(sources.indexOf('youtube')>=0)
        {
          YoutubeColumn = new DiggingColumnModel('Youtube', '', 'track');
          this.columns.push(YoutubeColumn);
        }
        HouseColumn = new DiggingColumnModel('House', 'house', this.viewMode);
        this.columns.push(HouseColumn);
        TechnoColumn = new DiggingColumnModel('Techno', 'tech', this.viewMode);
        this.columns.push(TechnoColumn);
        MinimalColumn = new DiggingColumnModel('Minimal', 'minimal', this.viewMode);
        this.columns.push(MinimalColumn);
        ElectroColumn = new DiggingColumnModel('Electro', 'electro', this.viewMode);
        this.columns.push(ElectroColumn);
        DiscoColumn = new DiggingColumnModel('Disco', 'disco', this.viewMode);
        this.columns.push(DiscoColumn);
        RockColumn = new DiggingColumnModel('Rock', 'rock', this.viewMode);
        this.columns.push(RockColumn);
        HipHopColumn = new DiggingColumnModel('HipHop', 'hop', this.viewMode);
        this.columns.push(HipHopColumn);
        OtherColumn = new DiggingColumnModel('Other', 'other', this.viewMode);
        this.columns.push(OtherColumn);
        async.eachSeries(sources, (sourceName, callbackEachSources) => {
          const sourceObject = sourcesObjects[sourceName];
          if (sourceObject && sourceObject.news) {

            let newsArg = '';
            if(sourceName === 'youtube') {
              newsArg = DiggingUser.youtubeChannels;
            }

            sourceObject.news(newsArg, (releases) => {

              releases = releases.map((_r)=>{
                _r.colType = 'news';

                if(_r.tracks && _r.tracks.length > 0){
                  _r.tracks = _r.tracks.map((_t)=>{
                    _t.colType = 'news';
                    return _t;
                  });
                }

                return _r;
              });

              if(YoutubeColumn && sourceName === 'youtube' && sources.indexOf('youtube')>=0) {
                YoutubeColumn.addTracks(releases.reduce((current, release) => {
                  if (release.tracks && release.tracks.length > 0) {
                    current = [...current, ...release.tracks];
                  }
                  return current;
                }, []));
                let progress = this.progress + (100 / this.sources.length);
                if(progress > 90) {
                  progress = 100;
                }
                this.progress = progress;
                this.sourcesProgress = this.sourcesProgress.map((item) => {
                  if(item.source.value === sourceName) {
                    item.progress = 100;
                  }
                  return item;
                });
                callbackEachSources(null);
              } else {
                async.eachSeries(this.columns, (columnModel, eachColumnCallback) => {
                  if(columnModel.matchString === 'other') {
                    columnModel[injectMethod](releases.reduce((current, release) => {
                      if (release.tracks && release.tracks.length > 0 && release.genre) {
                        const inject = this.columns.reduce((response, col) => {
                          if(col.matchString !== 'other' && release.genre.match(new RegExp(col.matchString, 'i')))
                          {
                            response = false;
                          }
                          return response;
                        }, true);

                        if(inject)
                        {
                          if(injectMethod === 'addReleases') {
                            current.push(release);
                          } else {
                            current = [...current, ...release.tracks];
                          }
                        }
                      }
                      return current;
                    }, []));
                  } else {
                    columnModel[injectMethod](releases.reduce((current, release) => {
                      if (release.tracks && release.tracks.length > 0 && release.genre && release.genre.match(new RegExp(columnModel.matchString, 'i'))) {
                        if(injectMethod === 'addReleases') {
                          current.push(release);
                        } else {
                          current = [...current, ...release.tracks];
                        }
                      }
                      return current;
                    }, []));
                  }
                  eachColumnCallback(null);
                }, () => {
                  //console.log('all cols for source '+sourceName+' as been processeded');
                  let progress = this.progress + (100 / this.sources.length);
                  if(progress > 90) {
                    progress = 100;
                  }
                  this.progress = progress;
                  this.sourcesProgress = this.sourcesProgress.map((item) => {
                    if(item.source.value === sourceName) {
                      item.progress = 100;
                    }
                    return item;
                  });
                  callbackEachSources(null);
                });
              }
            });
          } else {
            let progress = this.progress + (100 / this.sources.length);
            if(progress > 90) {
              progress = 100;
            }
            this.progress = progress;
            this.sourcesProgress = this.sourcesProgress.map((item) => {
              if(item.source.value === sourceName) {
                item.progress = 100;
              }
              return item;
            });
            callbackEachSources(null);
          }
        }, () => {
          //console.log('All releases from all sources as been injected');
        });
        break;
      case 'mynews':

        if(sources.indexOf('youtube')>=0)
        {
          YoutubeColumn = new DiggingColumnModel('Youtube', '', 'track');
          this.columns.push(YoutubeColumn);
        }
        HouseColumn = new DiggingColumnModel('House', 'house', this.viewMode);
        this.columns.push(HouseColumn);
        TechnoColumn = new DiggingColumnModel('Techno', 'tech', this.viewMode);
        this.columns.push(TechnoColumn);
        MinimalColumn = new DiggingColumnModel('Minimal', 'minimal', this.viewMode);
        this.columns.push(MinimalColumn);
        ElectroColumn = new DiggingColumnModel('Electro', 'electro', this.viewMode);
        this.columns.push(ElectroColumn);
        DiscoColumn = new DiggingColumnModel('Disco', 'disco', this.viewMode);
        this.columns.push(DiscoColumn);
        RockColumn = new DiggingColumnModel('Rock', 'rock', this.viewMode);
        this.columns.push(RockColumn);
        HipHopColumn = new DiggingColumnModel('HipHop', 'hop', this.viewMode);
        this.columns.push(HipHopColumn);
        OtherColumn = new DiggingColumnModel('Other', 'other', this.viewMode);
        this.columns.push(OtherColumn);
        async.eachSeries(sources, (sourceName, callbackEachSources) => {
          const sourceObject = sourcesObjects[sourceName];
          if (sourceObject && sourceObject.news) {

            let newsArg = '';
            if(sourceName === 'youtube') {
              newsArg = DiggingUser.youtubeChannels;
            }

            sourceObject.news(newsArg, (releases) => {

              let artists = DiggingLikesModel.tracks.reduce((c, t, i, a)=>{
                if(t.artist && c.indexOf(t.artist) < 0){
                  c.push(t.artist);
                }
                return c;
              }, []);

              const artistsPlaylists = DiggingPlaylistsModel.playlists.reduce((c, p, i, a)=>{
                let artistsFromPlaylist = [];
                if(p && p.tracks){
                  artistsFromPlaylist = p.tracks.reduce((cc, t, ii, aa)=>{
                    if(t.artist && cc.indexOf(t.artist) < 0){
                      cc.push(t.artist);
                    }
                    return cc;
                  }, []);
                }
                c = [...c, ...artistsFromPlaylist];
                return c;
              }, []);

              artists = [...artists, ...artistsPlaylists];

              let labels = DiggingLikesModel.tracks.reduce((c, t, i, a)=>{
                if(t.label && c.indexOf(t.label) < 0){
                  c.push(t.label);
                }
                return c;
              }, []);

              const labelsPlaylists = DiggingPlaylistsModel.playlists.reduce((c, p, i, a)=>{
                let labelsFromPlaylist = [];
                if(p && p.tracks){
                  labelsFromPlaylist = p.tracks.reduce((cc, t, ii, aa)=>{
                    if(t.label && cc.indexOf(t.label) < 0){
                      cc.push(t.label);
                    }
                    return cc;
                  }, []);
                }
                c = [...c, ...labelsFromPlaylist];
                return c;
              }, []);

              labels = [...labels, ...labelsPlaylists];

              releases = releases.map((_r)=>{
                _r.colType = 'news';

                if(_r.tracks && _r.tracks.length > 0){
                  _r.tracks = _r.tracks.map((_t)=>{
                    _t.colType = 'news';
                    return _t;
                  });
                }

                return _r;
              });

              releases = releases.reduce((list, _r)=>{

                let send = false;

                if(_r.artist){
                  if(artists.indexOf(_r.artist)>=0)
                  {
                    send = true;
                  }
                }

                if(_r.label){
                  if(labels.indexOf(_r.label)>=0)
                  {
                    send = true;
                  }
                }

                if(_r.tracks && _r.tracks.length > 0)
                {
                  _r.tracks.forEach((_t)=>{
                    if(_t.artist){
                      if(artists.indexOf(_t.artist)>=0)
                      {
                        send = true;
                      }
                    }

                  });
                }

                if(send) {
                  list.push(_r);
                }

                return list;
              }, []);

              if(YoutubeColumn && sourceName === 'youtube' && sources.indexOf('youtube')>=0) {
                YoutubeColumn.addTracks(releases.reduce((current, release) => {
                  if (release.tracks && release.tracks.length > 0) {
                    current = [...current, ...release.tracks];
                  }
                  return current;
                }, []));
                let progress = this.progress + (100 / this.sources.length);
                if(progress > 90) {
                  progress = 100;
                }
                this.progress = progress;
                this.sourcesProgress = this.sourcesProgress.map((item) => {
                  if(item.source.value === sourceName) {
                    item.progress = 100;
                  }
                  return item;
                });
                callbackEachSources(null);
              } else {
                async.eachSeries(this.columns, (columnModel, eachColumnCallback) => {
                  if(columnModel.matchString === 'other') {
                    columnModel[injectMethod](releases.reduce((current, release) => {
                      if (release.tracks && release.tracks.length > 0 && release.genre) {
                        const inject = this.columns.reduce((response, col) => {
                          if(col.matchString !== 'other' && release.genre.match(new RegExp(col.matchString, 'i')))
                          {
                            response = false;
                          }
                          return response;
                        }, true);

                        if(inject)
                        {
                          if(injectMethod === 'addReleases') {
                            current.push(release);
                          } else {
                            current = [...current, ...release.tracks];
                          }
                        }
                      }
                      return current;
                    }, []));
                  } else {
                    columnModel[injectMethod](releases.reduce((current, release) => {
                      if (release.tracks && release.tracks.length > 0 && release.genre && release.genre.match(new RegExp(columnModel.matchString, 'i'))) {
                        if(injectMethod === 'addReleases') {
                          current.push(release);
                        } else {
                          current = [...current, ...release.tracks];
                        }
                      }
                      return current;
                    }, []));
                  }
                  eachColumnCallback(null);
                }, () => {
                  console.log('all cols for source '+sourceName+' as been processeded');
                  let progress = this.progress + (100 / this.sources.length);
                  if(progress > 90) {
                    progress = 100;
                  }
                  this.progress = progress;
                  this.sourcesProgress = this.sourcesProgress.map((item) => {
                    if(item.source.value === sourceName) {
                      item.progress = 100;
                    }
                    return item;
                  });
                  callbackEachSources(null);
                });
              }
            });
          } else {
            let progress = this.progress + (100 / this.sources.length);
            if(progress > 90) {
              progress = 100;
            }
            this.progress = progress;
            this.sourcesProgress = this.sourcesProgress.map((item) => {
              if(item.source.value === sourceName) {
                item.progress = 100;
              }
              return item;
            });
            callbackEachSources(null);
          }
        }, () => {
          //console.log('All releases from all sources as been injected');
        });
        break;
      case 'artist':
        const ProductionsColumn = new DiggingColumnModel('Productions', '', this.viewMode);
        this.columns.push(ProductionsColumn);
        const MusicHePlaysColumn = new DiggingColumnModel('Music he plays', '', this.viewMode);
        this.columns.push(MusicHePlaysColumn);
        const ReleatedColumn = new DiggingColumnModel('Related', '', 'track');
        this.columns.push(ReleatedColumn);
        const LabelsColumn = new DiggingColumnModel('Labels', '', 'link');
        this.columns.push(LabelsColumn);
        const FansColumn = new DiggingColumnModel('Fans', '', 'link');
        this.columns.push(FansColumn);
        async.each(sources, (sourceName, callbackEachArtistSource) => {
          const sourceObject = sourcesObjects[sourceName];
          let searchFuncName1 = this.type;
          let searchFuncName2 = `${this.type}1`;
          const functions = [];
          if (sourceObject[searchFuncName1]) {
            functions.push(searchFuncName1);
          }
          if (sourceObject[searchFuncName2]) {
            functions.push(searchFuncName2);
          }
          if (functions.length === 1) {
            sourceObject[functions[0]](query, (releases) => {
              if(injectMethod === 'addReleases') {
                ProductionsColumn.addReleases(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0 && release.artist && release.artist.match(new RegExp(this.query, 'i'))) {
                    current.push(release);
                  }
                  return current;
                }, []));
                MusicHePlaysColumn.addReleases(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0 && release.playedBy && release.playedBy.match(new RegExp(this.query, 'i'))) {
                    if(release.artist && release.artist.length > 0) {
                      if(!release.artist.match(new RegExp(this.query, 'i'))){
                        current.push(release);
                      }
                    } else {
                      current.push(release);
                    }
                  }
                  return current;
                }, []));
                ReleatedColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.playedBy && !track.playedBy.match(new RegExp(this.query, 'i')) && track.artist && !track.artist.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
                LabelsColumn.addLinks(releases.reduce((labelsLinks, release) => {
                  if(release && release.artist && release.artist.match(new RegExp(this.query, 'i')) && release.label && release.label.length > 0) {
                    if(_.where(labelsLinks, {name: release.label}).length === 0) {
                      labelsLinks.push({id: Math.random(), name: release.label, type: 'label', release: [release]});
                    } else {
                      labelsLinks = labelsLinks.map((currentLink) => {
                        if(currentLink.name === release.label) {
                          currentLink.release.push(release);
                        }
                        return currentLink;
                      });
                    }
                  }
                  return labelsLinks;
                }, []));
                FansColumn.addLinks(releases.reduce((artistsLink, release) => {
                  if(release && release.tracks && release.tracks.length && release.playedBy && release.playedBy.length > 0)
                  {
                    if(release.playedBy && release.playedBy.length > 0)
                    {
                      if(_.where(artistsLink, {name: release.playedBy}).length === 0) {
                        artistsLink = [...artistsLink, {id: Math.random(), name: release.playedBy, type: 'artist', release: [release]}];
                      } else {
                        artistsLink = artistsLink.map((currentLink) => {
                          if(currentLink.name === release.playedBy) {
                            currentLink.release.push(release);
                          }
                          return currentLink;
                        });
                      }
                    }
                  }
                  return artistsLink;
                }, []));
              } else {
                ProductionsColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
                MusicHePlaysColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.playedBy && track.playedBy.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
                ReleatedColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.playedBy && !track.playedBy.match(new RegExp(this.query, 'i')) && track.artist && !track.artist.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
                LabelsColumn.addLinks(releases.reduce((_allTracks, release) => {
                  if(release && release.tracks && release.tracks.length)
                  {
                    _allTracks = [..._allTracks, ...release.tracks];
                  }
                  return _allTracks;
                }, []).reduce((currentLinks, track) => {
                  if (track && track.label && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                    if(_.where(currentLinks, {name: track.label}).length === 0) {
                      currentLinks.push({name: track.label, type: 'label', tracks: [track]});
                    } else {
                      currentLinks = currentLinks.map((currentLink) => {
                        if(currentLink.name === track.label) {
                          currentLink.tracks.push(track);
                        }
                        return currentLink;
                      });
                    }
                  }
                  return currentLinks;
                }, []));
                FansColumn.addLinks(releases.reduce((_allTracks, release) => {
                  if(release && release.tracks && release.tracks.length)
                  {
                    _allTracks = [..._allTracks, ...release.tracks];
                  }
                  return _allTracks;
                }, []).reduce((currentLinks, track) => {
                  if (track && track.playedBy && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                    if(_.where(currentLinks, {name: track.playedBy}).length === 0) {
                      currentLinks.push({name: track.playedBy, type: 'artist', tracks: [track]});
                    } else {
                      currentLinks = currentLinks.map((currentLink) => {
                        if(currentLink.name === track.playedBy) {
                          currentLink.tracks.push(track);
                        }
                        return currentLink;
                      });
                    }
                  }
                  return currentLinks;
                }, []));
              }
              this.sourcesProgress = this.sourcesProgress.map((item) => {
                if(item.source.value === sourceName) {
                  item.progress = 100;
                }
                return item;
              });
              callbackEachArtistSource(null);
            });
          } else if (functions.length === 2) {
            let searchFuncName1 = functions[0];
            let searchFuncName2 = functions[1];
            sourceObject[searchFuncName1](query, null, (pages) => {
              if (pages.length > 0) {
                async.mapSeries(pages, (page, eachPagesCallback) => {
                  sourceObject[searchFuncName2](query, page, (release) => {
                    this.sourcesProgress = this.sourcesProgress.map((item) => {
                      if(item.source.value === sourceName) {
                        item.progress += 100/pages.length;
                      }
                      return item;
                    });
                    if(release) {
                      eachPagesCallback(null, release);
                    } else {
                      eachPagesCallback(null, null);
                    }

                  });
                }, (error, allReleases) => {

                  const releases = allReleases.reduce((_releases, _release) => {
                    if(_release) {
                      _releases.push(_release);
                    }
                    return _releases;
                  }, []);

                  if(injectMethod === 'addReleases') {
                    ProductionsColumn.addReleases(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0 && release.artist && release.artist.match(new RegExp(this.query, 'i'))) {
                        if(release.kind === 'release') {
                          current.push(release);
                        }
                      }
                      return current;
                    }, []));
                    MusicHePlaysColumn.addReleases(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0 && release.playedBy && release.playedBy.match(new RegExp(this.query, 'i'))) {
                        if(release.artist && release.artist.length > 0) {
                          if(release.kind !== 'playlist' && !release.artist.match(new RegExp(this.query, 'i'))){
                            current.push(release);
                          } else if(release.kind === 'playlist'){
                            current.push(release);
                          }
                        } else {
                          current.push(release);
                        }
                      }
                      return current;
                    }, []));
                    ReleatedColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.playedBy && !track.playedBy.match(new RegExp(this.query, 'i')) && track.artist && !track.artist.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                    LabelsColumn.addLinks(releases.reduce((labelsLinks, release) => {
                      if(release && release.artist && release.artist.match(new RegExp(this.query, 'i')) && release.label && release.label.length > 0) {
                        if(_.where(labelsLinks, {name: release.label}).length === 0) {
                          labelsLinks.push({id: Math.random(), name: release.label, type: 'label', release: [release]});
                        } else {
                          labelsLinks = labelsLinks.map((currentLink) => {
                            if(currentLink.name === release.label) {
                              currentLink.release.push(release);
                            }
                            return currentLink;
                          });
                        }
                      }
                      return labelsLinks;
                    }, []));
                    FansColumn.addLinks(releases.reduce((artistsLink, release) => {
                      if(release && release.tracks && release.tracks.length && release.playedBy && release.playedBy.length > 0)
                      {
                        if(release.playedBy && release.playedBy.length > 0)
                        {
                          if(_.where(artistsLink, {name: release.playedBy}).length === 0) {
                            artistsLink = [...artistsLink, {id: Math.random(), name: release.playedBy, type: 'artist', release: [release]}];
                          } else {
                            artistsLink = artistsLink.map((currentLink) => {
                              if(currentLink.name === release.playedBy) {
                                currentLink.release.push(release);
                              }
                              return currentLink;
                            });
                          }
                        }
                      }
                      return artistsLink;
                    }, []));
                  } else {
                    ProductionsColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                    MusicHePlaysColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.playedBy && track.playedBy.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                    ReleatedColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.playedBy && !track.playedBy.match(new RegExp(this.query, 'i')) && track.artist && !track.artist.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                    LabelsColumn.addLinks(releases.reduce((_allTracks, release) => {
                      if(release && release.tracks && release.tracks.length)
                      {
                        _allTracks = [..._allTracks, ...release.tracks];
                      }
                      return _allTracks;
                    }, []).reduce((currentLinks, track) => {
                      if (track && track.label && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                        if(_.where(currentLinks, {name: track.label}).length === 0) {
                          currentLinks.push({name: track.label, type: 'label', tracks: [track]});
                        } else {
                          currentLinks = currentLinks.map((currentLink) => {
                            if(currentLink.name === track.label) {
                              currentLink.tracks.push(track);
                            }
                            return currentLink;
                          });
                        }
                      }
                      return currentLinks;
                    }, []));
                    FansColumn.addLinks(releases.reduce((_allTracks, release) => {
                      if(release && release.tracks && release.tracks.length)
                      {
                        _allTracks = [..._allTracks, ...release.tracks];
                      }
                      return _allTracks;
                    }, []).reduce((currentLinks, track) => {
                      if (track && track.playedBy && track.artist && track.artist.match(new RegExp(this.query, 'i'))) {
                        if(_.where(currentLinks, {name: track.playedBy}).length === 0) {
                          currentLinks.push({name: track.playedBy, type: 'artist', tracks: [track]});
                        } else {
                          currentLinks = currentLinks.map((currentLink) => {
                            if(currentLink.name === track.playedBy) {
                              currentLink.tracks.push(track);
                            }
                            return currentLink;
                          });
                        }
                      }
                      return currentLinks;
                    }, []));
                  }

                  this.sourcesProgress = this.sourcesProgress.map((item) => {
                    if(item.source.value === sourceName) {
                      item.progress = 100;
                    }
                    return item;
                  });
                  callbackEachArtistSource(null);
                });
              } else {
                this.sourcesProgress = this.sourcesProgress.map((item) => {
                  if(item.source.value === sourceName) {
                    item.progress = 100;
                  }
                  return item;
                });
                callbackEachArtistSource(null);
              }
            });
          } else {
            this.sourcesProgress = this.sourcesProgress.map((item) => {
              if(item.source.value === sourceName) {
                item.progress = 100;
              }
              return item;
            });
            callbackEachArtistSource(null);
          }
        }, () => {
          //console.log('all sources as searched for query : '+query);
        });
        break;
      case 'label':
        const TracksFromLabelColumn = new DiggingColumnModel('Tracks from '+this.query, '', this.viewMode);
        this.columns.push(TracksFromLabelColumn);
        const ArtistsFromLabelColumn = new DiggingColumnModel('Artists from '+this.query, '', 'link');
        this.columns.push(ArtistsFromLabelColumn);
        const ArtistsPlayingLabelColumn = new DiggingColumnModel('Artists playing music from '+this.query, '', 'link');
        this.columns.push(ArtistsPlayingLabelColumn);
        async.eachSeries(sources, (sourceName, callbackEachArtistSource) => {
          const sourceObject = sourcesObjects[sourceName];
          let searchFuncName1 = this.type;
          let searchFuncName2 = `${this.type}1`;
          const functions = [];
          if (sourceObject[searchFuncName1]) {
            functions.push(searchFuncName1);
          }
          if (sourceObject[searchFuncName2]) {
            functions.push(searchFuncName2);
          }
          if (functions.length === 1) {
            sourceObject[functions[0]](query, (releases) => {
              if(injectMethod === 'addReleases') {
                TracksFromLabelColumn.addReleases(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0 && release.label && release.label.match(new RegExp(this.query, 'i'))) {
                    current.push(release);
                  }
                  return current;
                }, []));
                ArtistsFromLabelColumn.addLinks(releases.reduce((artistsLink, release) => {
                  if(_.where(artistsLink, {name: release.artist}).length === 0) {
                    artistsLink = [...artistsLink, {id: Math.random(), name: release.artist, type: 'artist', release: [release]}];
                  } else {
                    artistsLink = artistsLink.map((currentLink) => {
                      if(currentLink.name === release.artist) {
                        currentLink.release.push(release);
                      }
                      return currentLink;
                    });
                  }
                  return artistsLink;
                }, []));

                ArtistsPlayingLabelColumn.addLinks(releases.reduce((artistsLink, release) => {
                  if(_.where(artistsLink, {name: release.playedBy}).length === 0) {
                    artistsLink = [...artistsLink, {id: Math.random(), name: release.playedBy, type: 'artist', release: [release]}];
                  } else {
                    artistsLink = artistsLink.map((currentLink) => {
                      if(currentLink.name === release.playedBy) {
                        currentLink.release.push(release);
                      }
                      return currentLink;
                    });
                  }
                  return artistsLink;
                }, []));
              } else {
                TracksFromLabelColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.label && track.label.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
                ArtistsFromLabelColumn.addLinks(releases.reduce((_allTracks, release) => {
                  if(release && release.tracks && release.tracks.length)
                  {
                    _allTracks = [..._allTracks, ...release.tracks];
                  }
                  return _allTracks;
                }, []).reduce((currentLinks, track) => {
                  if (track && track.label && track.artist && track.label.match(new RegExp(this.query, 'i'))) {
                    if(_.where(currentLinks, {name: track.artist}).length === 0) {
                      currentLinks.push({name: track.artist, type: 'artist', tracks: [track]});
                    } else {
                      currentLinks = currentLinks.map((currentLink) => {
                        if(currentLink.name === track.artist) {
                          currentLink.tracks.push(track);
                        }
                        return currentLink;
                      });
                    }
                  }
                  return currentLinks;
                }, []));
                ArtistsPlayingLabelColumn.addLinks(releases.reduce((_allTracks, release) => {
                  if(release && release.tracks && release.tracks.length)
                  {
                    _allTracks = [..._allTracks, ...release.tracks];
                  }
                  return _allTracks;
                }, []).reduce((currentLinks, track) => {
                  if (track && track.playedBy && track.artist && track.label && track.label.match(new RegExp(this.query, 'i'))) {
                    if(_.where(currentLinks, {name: track.playedBy}).length === 0) {
                      currentLinks.push({name: track.playedBy, type: 'artist', tracks: [track]});
                    } else {
                      currentLinks = currentLinks.map((currentLink) => {
                        if(currentLink.name === track.playedBy) {
                          currentLink.tracks.push(track);
                        }
                        return currentLink;
                      });
                    }
                  }
                  return currentLinks;
                }, []));
              }
              this.sourcesProgress = this.sourcesProgress.map((item) => {
                if(item.source.value === sourceName) {
                  item.progress = 100;
                }
                return item;
              });
              callbackEachArtistSource(null);
            });
          } else if (functions.length === 2) {
            let searchFuncName1 = functions[0];
            let searchFuncName2 = functions[1];
            sourceObject[searchFuncName1](query, null, (pages) => {
              if (pages.length > 0) {
                async.mapSeries(pages, (page, eachPagesCallback) => {
                  sourceObject[searchFuncName2](query, page, (release) => {
                    this.sourcesProgress = this.sourcesProgress.map((item) => {
                      if(item.source.value === sourceName) {
                        item.progress += 100/pages.length;
                      }
                      return item;
                    });
                    eachPagesCallback(null, release);
                  });
                }, (error, allReleases) => {

                  const releases = allReleases.reduce((_releases, _release) => {
                    if(_release) {
                      _releases.push(_release);
                    }
                    return _releases;
                  }, []);

                  if(injectMethod === 'addReleases') {
                    TracksFromLabelColumn.addReleases(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0 && release.label && release.label.match(new RegExp(this.query, 'i'))) {
                        current.push(release);
                      }
                      return current;
                    }, []));
                    ArtistsFromLabelColumn.addLinks(releases.reduce((artistsLink, release) => {
                      if(_.where(artistsLink, {name: release.artist}).length === 0) {
                        artistsLink = [...artistsLink, {id: Math.random(), name: release.artist, type: 'artist', release: [release]}];
                      } else {
                        artistsLink = artistsLink.map((currentLink) => {
                          if(currentLink.name === release.artist) {
                            currentLink.release.push(release);
                          }
                          return currentLink;
                        });
                      }
                      return artistsLink;
                    }, []));

                    ArtistsPlayingLabelColumn.addLinks(releases.reduce((artistsLink, release) => {
                      if(_.where(artistsLink, {name: release.playedBy}).length === 0) {
                        artistsLink = [...artistsLink, {id: Math.random(), name: release.playedBy, type: 'artist', release: [release]}];
                      } else {
                        artistsLink = artistsLink.map((currentLink) => {
                          if(currentLink.name === release.playedBy) {
                            currentLink.release.push(release);
                          }
                          return currentLink;
                        });
                      }
                      return artistsLink;
                    }, []));
                  } else {
                    TracksFromLabelColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.label && track.label.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                    ArtistsFromLabelColumn.addLinks(releases.reduce((_allTracks, release) => {
                      if(release && release.tracks && release.tracks.length)
                      {
                        _allTracks = [..._allTracks, ...release.tracks];
                      }
                      return _allTracks;
                    }, []).reduce((currentLinks, track) => {
                      if (track && track.label && track.artist && track.label.match(new RegExp(this.query, 'i'))) {
                        if(_.where(currentLinks, {name: track.artist}).length === 0) {
                          currentLinks.push({name: track.artist, type: 'artist', tracks: [track]});
                        } else {
                          currentLinks = currentLinks.map((currentLink) => {
                            if(currentLink.name === track.artist) {
                              currentLink.tracks.push(track);
                            }
                            return currentLink;
                          });
                        }
                      }
                      return currentLinks;
                    }, []));
                    ArtistsPlayingLabelColumn.addLinks(releases.reduce((_allTracks, release) => {
                      if(release && release.tracks && release.tracks.length)
                      {
                        _allTracks = [..._allTracks, ...release.tracks];
                      }
                      return _allTracks;
                    }, []).reduce((currentLinks, track) => {
                      if (track && track.playedBy && track.artist && track.label && track.label.match(new RegExp(this.query, 'i'))) {
                        if(_.where(currentLinks, {name: track.playedBy}).length === 0) {
                          currentLinks.push({name: track.playedBy, type: 'artist', tracks: [track]});
                        } else {
                          currentLinks = currentLinks.map((currentLink) => {
                            if(currentLink.name === track.playedBy) {
                              currentLink.tracks.push(track);
                            }
                            return currentLink;
                          });
                        }
                      }
                      return currentLinks;
                    }, []));
                  }

                  this.sourcesProgress = this.sourcesProgress.map((item) => {
                    if(item.source.value === sourceName) {
                      item.progress = 100;
                    }
                    return item;
                  });
                  callbackEachArtistSource(null);
                });
              } else {
                this.sourcesProgress = this.sourcesProgress.map((item) => {
                  if(item.source.value === sourceName) {
                    item.progress = 100;
                  }
                  return item;
                });
                callbackEachArtistSource(null);
              }
            });
          } else {
            this.sourcesProgress = this.sourcesProgress.map((item) => {
              if(item.source.value === sourceName) {
                item.progress = 100;
              }
              return item;
            });
            callbackEachArtistSource(null);
          }
        }, () => {
          //console.log('all sources as searched for query : '+query);
        });
        break;
      case 'release':
        const discogs = sourcesObjects['discogs'];
        discogs.all(this.query, null, (pages)=>{
          if(pages && pages[0] && pages[0].url){
            discogs.all1(this.query, pages[0], (_release)=>{
              if(_release) {
                const release = new ReleaseModel(_release);
                const ReleaseColumn = new DiggingColumnModel('Tracks for '+this.query, '', 'release');
                this.columns.push(ReleaseColumn);
                ReleaseColumn.addRelease(release);
              }
            })
          }
        });
        break;
      default:
        const TracksColumn = new DiggingColumnModel('Tracks for '+this.query, '', this.viewMode);
        this.columns.push(TracksColumn);
        async.eachSeries(sources, (sourceName, callbackEachArtistSource) => {
          const sourceObject = sourcesObjects[sourceName];
          let searchFuncName1 = this.type;
          let searchFuncName2 = `${this.type}1`;
          const functions = [];
          if (sourceObject[searchFuncName1]) {
            functions.push(searchFuncName1);
          }
          if (sourceObject[searchFuncName2]) {
            functions.push(searchFuncName2);
          }
          if (functions.length === 1) {
            sourceObject[functions[0]](query, (releases) => {
              if(!releases){
                releases = [];
              }
              if(injectMethod === 'addReleases') {
                TracksColumn.addReleases(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    current.push(release);
                  }
                  return current;
                }, []));
              } else {
                TracksColumn.addTracks(releases.reduce((current, release) => {
                  if(release && release.tracks && release.tracks.length > 0) {
                    const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                      if (track && track.query && track.query.match(new RegExp(this.query, 'i'))) {
                        currentTracks.push(track);
                      }
                      return currentTracks;
                    }, []);
                    if(tracksToAdd && tracksToAdd.length > 0) {
                      current = [...current, ...tracksToAdd];
                    }
                  }
                  return current;
                }, []));
              }
              this.sourcesProgress = this.sourcesProgress.map((item) => {
                if(item.source.value === sourceName) {
                  item.progress = 100;
                }
                return item;
              });
              callbackEachArtistSource(null);
            });
          } else if (functions.length === 2) {
            let searchFuncName1 = functions[0];
            let searchFuncName2 = functions[1];
            sourceObject[searchFuncName1](query, null, (pages) => {
              if (pages.length > 0) {
                async.mapSeries(pages, (page, eachPagesCallback) => {
                  sourceObject[searchFuncName2](query, page, (release) => {
                    this.sourcesProgress = this.sourcesProgress.map((item) => {
                      if(item.source.value === sourceName) {
                        item.progress += 100/pages.length;
                      }
                      return item;
                    });
                    eachPagesCallback(null, release);
                  });
                }, (error, allReleases) => {

                  const releases = allReleases.reduce((_releases, _release) => {
                    if(_release) {
                      _releases.push(_release);
                    }
                    return _releases;
                  }, []);

                  if(injectMethod === 'addReleases') {
                    TracksColumn.addReleases(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        current.push(release);
                      }
                      return current;
                    }, []));
                  } else {
                    TracksColumn.addTracks(releases.reduce((current, release) => {
                      if(release && release.tracks && release.tracks.length > 0) {
                        const tracksToAdd = release.tracks.reduce((currentTracks, track) => {
                          if (track && track.query && track.query.match(new RegExp(this.query, 'i'))) {
                            currentTracks.push(track);
                          }
                          return currentTracks;
                        }, []);
                        if(tracksToAdd && tracksToAdd.length > 0) {
                          current = [...current, ...tracksToAdd];
                        }
                      }
                      return current;
                    }, []));
                  }

                  this.sourcesProgress = this.sourcesProgress.map((item) => {
                    if(item.source.value === sourceName) {
                      item.progress = 100;
                    }
                    return item;
                  });
                  callbackEachArtistSource(null);
                });
              } else {
                this.sourcesProgress = this.sourcesProgress.map((item) => {
                  if(item.source.value === sourceName) {
                    item.progress = 100;
                  }
                  return item;
                });
                callbackEachArtistSource(null);
              }
            });
          } else {
            this.sourcesProgress = this.sourcesProgress.map((item) => {
              if(item.source.value === sourceName) {
                item.progress = 100;
              }
              return item;
            });
            callbackEachArtistSource(null);
          }
        }, () => {
          //console.log('all sources as searched for query : '+query);
        });
        break;
    }
  }
  @action
  addColumn(title) {
    this.columns.push(new DiggingColumnModel(title));
  }
  @action
  removeColumn(id) {
    this.columns.reduce((current, column) => {
      if (column.id !== id) {
        current.push(column);
      }
      return current;
    }, []);
  }
  @action
  setActiveColumn(index) {
    this.activeColumn = index;
  }
}
